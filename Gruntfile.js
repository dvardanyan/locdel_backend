module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        concat: {
            dist_js: {
                src: [
                    'admin/js/*.js',
                    'site/js/*.js'
                ],
                dest: 'build/locdel.js'
            },
            dist_css: {
                src: [
                    'admin/css/*.css',
                    'site/css/*.css'

                ],
                dest: '/www/locdel/css/admin.css'
            },
            develop_js: {
                src: [
                    'admin/js/*.js',
                    'site/js/*.js'
                ],
                dest: 'public/js/locdel.js'
            },
            develop_css: {
                src: [
                    'admin/css/*.css'
                ],
                dest: 'public/css/admin.css'
            }
        },
        uglify: {
            options: {
                mangle: true
            },
            dist: {
                files: {
                    '/www/locdel/js/locdel.min.js': ['/www/locdel/js/locdel.js']
                }
            },
            develop: {
                files: {
                    'public/js/locdel.min.js': ['public/js/locdel.js']
                }
            }
        },
        babel: {
            options: {
                sourceMap: true,
                presets: ['es2015']
            },
            dist: {
                files: {
                    '/www/locdel/js/locdel.js': ['build/locdel.js']
                }
            },
            develop: {
                files: {
                    'public/js/locdel.js': ['public/js/locdel.js']
                }
            }
        },
        copy: {
            main: {
                files: [
                    {
                        expand: true,
                        cwd: './node_modules/angular',
                        src: 'angular.min.js',
                        dest: '/www/locdel/js/'
                    },
                    {
                        expand: true,
                        cwd: './node_modules/bootstrap/dist/css',
                        src: 'bootstrap.min.css',
                        dest: '/www/locdel/css/'
                    },
                    {
                        expand: true,
                        cwd: './node_modules/angular-ui-bootstrap',
                        src: 'ui-bootstrap.min.js',
                        dest: '/www/locdel/js/'
                    },
                    {
                        expand: true,
                        cwd: './node_modules/angular-ui-bootstrap',
                        src: 'ui-bootstrap-tpls.min.js',
                        dest: '/www/locdel/js/'
                    },
                    {
                        expand: true,
                        cwd: './node_modules/angular-data-table/release',
                        src: 'dataTable.min.js',
                        dest: '/www/locdel/js/'
                    },
                    {
                        expand: true,
                        cwd: './node_modules/moment/min',
                        src: 'moment.min.js',
                        dest: '/www/locdel/js/'
                    },
                    {
                        expand: true,
                        cwd: './node_modules/ob-daterangepicker/dist/scripts/min/',
                        src: 'ob-daterangepicker.js',
                        dest: '/www/locdel/js/'
                    },
                    {
                        expand: true,
                        cwd: './node_modules/ajaxchimp/',
                        src: 'jquery.ajaxchimp.min.js',
                        dest: '/www/locdel/js/'
                    },
                    {
                        expand: true,
                        cwd: './node_modules/ob-daterangepicker/dist/styles/',
                        src: 'ob-daterangepicker.css',
                        dest: '/www/locdel/css/'
                    },
                    {
                        expand: true,
                        cwd: './node_modules/angular-bootstrap-lightbox/dist',
                        src: 'angular-bootstrap-lightbox.min.js',
                        dest: '/www/locdel/js/'
                    },
                    {
                        expand: true,
                        cwd: './node_modules/angular-bootstrap-lightbox/dist',
                        src: 'angular-bootstrap-lightbox.min.css',
                        dest: '/www/locdel/css/'
                    },
                    {
                        expand: true,
                        cwd: './node_modules/jsonformatter/dist',
                        src: 'json-formatter.min.js',
                        dest: '/www/locdel/js/'
                    },
                    {
                        expand: true,
                        cwd: './node_modules/jsonformatter/dist',
                        src: 'json-formatter.css',
                        dest: '/www/locdel/css/'
                    },
                    {
                        expand: true,
                        cwd: 'public/text',
                        src: '*',
                        dest: '/www/locdel/text/'
                    },
                    {
                        expand: true,
                        cwd: './assets/images/',
                        src: ['**/*.{png,jpg,svg}'],
                        dest: '/www/locdel/images/'
                    },
                    {
                        expand: true,
                        cwd: './assets/css/',
                        src: ['**/*.css'],
                        dest: '/www/locdel/css/'
                    },
                    {
                        expand: true,
                        cwd: './assets/templates/',
                        src: ['**/*.html'],
                        dest: '/www/locdel/templates/'
                    },
                    {
                        expand: true,
                        cwd: './admin/',
                        src: ['**/*.html'],
                        dest: '/www/locdel/templates/'
                    },
                    {
                        expand: true,
                        cwd: './assets/fonts/',
                        src: ['**/*'],
                        dest: '/www/locdel/fonts/'
                    },
                    {
                        expand: true,
                        cwd: './site/js/',
                        src: ['**/*'],
                        dest: '/www/locdel/js/'
                    },
                    {
                        expand: true,
                        cwd: './site/css/',
                        src: ['**/*'],
                        dest: '/www/locdel/css/'
                    },
                    {
                        expand: true,
                        cwd: './node_modules/angular-data-table/release',
                        src: ['*.css'],
                        dest: '/www/locdel/css/'
                    },
                    {
                        expand: true,
                        cwd: './site/img/',
                        src: ['**/*'],
                        dest: '/www/locdel/images/'
                    },
                    {
                        expand: true,
                        cwd: './site/fonts/',
                        src: ['**/*'],
                        dest: '/www/locdel/fonts/'
                    }
                ]
            },
            develop: {
                files: [
                    {
                        expand: true,
                        cwd: './node_modules/angular',
                        src: 'angular.min.js',
                        dest: 'public/js/'
                    },
                    {
                        expand: true,
                        cwd: './node_modules/bootstrap/dist/css',
                        src: 'bootstrap.min.css',
                        dest: 'public/css/'
                    },
                    {
                        expand: true,
                        cwd: './node_modules/angular-ui-bootstrap',
                        src: 'ui-bootstrap.min.js',
                        dest: 'public/js/'
                    },
                    {
                        expand: true,
                        cwd: './node_modules/angular-ui-bootstrap',
                        src: 'ui-bootstrap-tpls.min.js',
                        dest: 'public/js/'
                    },
                    {
                        expand: true,
                        cwd: './node_modules/jsonformatter/dist',
                        src: 'json-formatter.min.js',
                        dest: 'public/js/'
                    },
                    {
                        expand: true,
                        cwd: './node_modules/moment/min',
                        src: 'moment.min.js',
                        dest: 'public/js/'
                    },
                    {
                        expand: true,
                        cwd: './node_modules/ob-daterangepicker/dist/scripts/min/',
                        src: 'ob-daterangepicker.js',
                        dest: 'public/js/'
                    },
                    {
                        expand: true,
                        cwd: './node_modules/ob-daterangepicker/dist/styles/',
                        src: 'ob-daterangepicker.css',
                        dest: 'public/css/'
                    },
                    {
                        expand: true,
                        cwd: './node_modules/jsonformatter/dist',
                        src: 'json-formatter.css',
                        dest: 'public/css'
                    },
                    {
                        expand: true,
                        cwd: './node_modules/angular-data-table/release',
                        src: 'dataTable.min.js',
                        dest: 'public/js/'
                    },
                    {
                        expand: true,
                        cwd: './node_modules/angular-bootstrap-lightbox/dist',
                        src: 'angular-bootstrap-lightbox.min.js',
                        dest: 'public/js/'
                    },
                    {
                        expand: true,
                        cwd: './node_modules/ajaxchimp',
                        src: 'jquery.ajaxchimp.min.js',
                        dest: 'public/js/'
                    },
                    {
                        expand: true,
                        cwd: './node_modules/angular-bootstrap-lightbox/dist',
                        src: 'angular-bootstrap-lightbox.min.css',
                        dest: 'public/css/'
                    },
                    {
                        expand: true,
                        cwd: './node_modules/angular-data-table/release',
                        src: ['*.css'],
                        dest: 'public/css/'
                    },
                    {
                        expand: true,
                        cwd: './assets/images/',
                        src: ['**/*.{png,jpg,svg}'],
                        dest: 'public/images/'
                    },
                    {
                        expand: true,
                        cwd: './assets/css/',
                        src: ['**/*.css'],
                        dest: 'public/css/'
                    },
                    {
                        expand: true,
                        cwd: './assets/fonts/',
                        src: ['**/*'],
                        dest: 'public/fonts/'
                    },
                    {
                        expand: true,
                        cwd: './assets/templates/',
                        src: ['**/*.html'],
                        dest: 'public/templates/'
                    },
                    {
                        expand: true,
                        cwd: './admin/',
                        src: ['**/*.html'],
                        dest: 'public/templates/'
                    }
                ]
            }
        },
        watch: {
            scripts: {
                files: ['admin/**/*','site/**/*', 'assets/**/*'],
                tasks: ['concat:develop_js', 'concat:develop_css', 'copy:develop'],
                options: {
                    spawn: false
                }
            }
        }

    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-concat-css');
    grunt.loadNpmTasks('grunt-babel');

    grunt.registerTask('default', ['concat:dist_js', 'concat:dist_css', 'babel:dist', 'copy:main']);
    grunt.registerTask('develop', ['concat:develop_js', 'concat:develop_css', 'copy:develop', 'watch']);

};