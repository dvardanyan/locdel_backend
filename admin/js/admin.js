var ll = angular.module('app', ['ui.bootstrap', 'ngRoute', 'data-table', 'jsonFormatter', 'bootstrapLightbox', 'ngMessages', 'obDateRangePicker', 'toggle-switch'])
    .controller('Login', ['$scope', '$element', '$http', '$window', function ($scope, $element, $http, $window) {
        $scope.submit = function () {
            var form = $scope.loginForm,
                username = form.username,
                password = form.password;

            if (form.$valid) {
                $scope.err = null;
                $http.post('login', {
                    email: username.$modelValue,
                    password: password.$modelValue,
                    usertype: 0
                }).then(function (res) {
                    if (res.data.status === 'OK') {
                        $window.location.href = '';
                    } else {
                        $scope.err = res.data;
                        $scope.$apply();
                    }
                }, function (err) {
                    $scope.err = err;
                    $scope.$apply();
                });
            }
        }
        $scope.myFunct = function(keyEvent) {
            if (keyEvent.which === 13)
                $scope.submit();
        }
    }])
    .controller('RegisterController', ['$scope', '$http', function ($scope, $http) {
        $scope.submit = function () {
            var form = $scope.registerForm;

            if (form.$valid) {
                $http.post('register/user', {
                    email: form.username.$modelValue,
                    password: form.password.$modelValue,
                    userType: +$scope.userType,
                    firstName: form.firstName.$modelValue,
                    lastName: form.lastName.$modelValue
                }).then(function (res) {
                    $scope.state = {
                        type: res.data.status === 'OK' ? 'success' : 'danger',
                        message: res.data.status === 'OK' ? 'User Registered' : res.data.message
                    }
                }, function (err) {
                    $scope.state = {
                        type: 'danger',
                        message: err.data
                    }
                });
            }
        }
    }])
    .controller('PaymentsController', function ($scope, $routeParams) {
        $scope.name = "BookController";
        $scope.params = $routeParams;
    })

    .controller('ChapterController', function ($scope, $routeParams) {
        $scope.name = "ChapterController";
        $scope.params = $routeParams;
    })

    .controller('AdvertisementController',['$scope', '$http', function ($scope, $http) {

        $scope.data = [];
        $scope.dataforDriver = [];
        $scope.dataforCustomer = [];
        $scope.options = {
            rowHeight: 30,
            headerHeight: 30,
            footerHeight: 50,
            scrollbarV: false,
            columnMode: 'flex',
            columns: [
                {
                    name: "Id",
                    prop: "_id",
                    flexGrow: 1
                },
                {
                    name: "Text",
                    prop: "text",
                    flexGrow: 1
                },
                {
                    name: "ForCustomer",
                    prop: "forCustomer",
                    flexGrow: 1,
                    cellRenderer: function ({$row}) {
                        return '<input type="button" ng-value="$row.forCustomer" ng-click="UpdateC($row)" > </input>';
                    }
                },
                {
                    name: "ForDriver",
                    prop: "forDriver",
                    flexGrow: 1,
                    cellRenderer: function ({$row}) {
                        return '<input type="button" ng-value="$row.forDriver" ng-click="UpdateD($row)" />';
                    }
                },
                {
                    name: "IsDeleted",
                    prop: "isDeleted",
                    flexGrow: 1,
                    cellRenderer: function ({$row}) {
                        return '<input class="isDeleted" type="button" ng-click="Delete($row)" value="Delete" />';
                    }
                },
                {
                    name: "File",
                    prop: "file",
                    flexGrow: 1,
                    cellRenderer: function ({$row}) {
                        var str = $row.file.url;
                        var url = str.replace('/www/locdel/','/');
                        console.log(url);
                        return '<span><img  class="advertismentImg"  src='+url+'  /></span>';
                    }
                }]
        };

        function render() {

            $http.get('get-advertisements', {

            }).then(function (data) {
                $scope.data = [];
                $scope.dataforDriver = [];
                $scope.dataforCustomer = [];
                var ijd=0;
                var ijc=0;
                if(data.data.data!=undefined) {
                    data.data.data.advertisements.forEach(function (i, j) {
                        console.log(i);
                        $scope.data[j] = i;
                        var str = i.file.url;
                        var url = str.replace('/www/locdel/', '/');
                        console.log(url);
                        if (i.forDriver) {
                            $scope.dataforDriver[ijd] = i;
                            $scope.dataforDriver[ijd].file.url = url;

                        }
                        if (i.forCustomer) {
                            $scope.dataforCustomer[ijc] = i;
                            $scope.dataforCustomer[ijc].file.url = url;
                        }
                        ijd++;
                        ijc++;
                    });
                }
            })

        }
        render();


        $scope.$on('uploaded', function (event, attachment) {
            $scope.path = attachment.path;
            $scope.filename = attachment.filename;
        });
        $scope.Delete= function (row) {
            console.log(row);
            $http.post('delete-advertisement', {
                params: {
                    fileid: row._id,
                    isDeleted : true
                }
            }).then(function (data) {
                render();
            })

        }
        $scope.UpdateD = function (row) {
            console.log(row);
            if(row.forDriver==false)
            {
                row.forDriver=true;
            }
            else
            {
                row.forDriver=false;
            }
            $http.post('update-advertisement', {
                params: {
                    fileid: row._id,
                    forDriver : row.forDriver,
                    forCustomer:row.forCustomer
                }
            }).then(function (data) {
                render();
            })

        }
        $scope.UpdateC = function (row) {
            console.log(row);
            if(row.forCustomer==false)
            {
                row.forCustomer=true;
            }
            else
            {
                row.forCustomer=false;
            }
            $http.post('update-advertisement', {
                params: {
                    fileid: row._id,
                    forDriver : row.forDriver,
                    forCustomer: row.forCustomer
                }
            }).then(function (data) {
                render();
            })

        }
        $scope.Submit=function (event, attachment) {
            console.log($scope.filename);
            if($scope.filename) {
                console.log($scope.filename);
                $http.get('add-advertisementImg', {
                    params: {
                        path: $scope.path,
                        filename: $scope.filename,
                        text: $scope.text
                    }
                }).then(function (data) {
                    render();
                })
            }
        };

    }])

    .config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
        $locationProvider.html5Mode(true);
        $routeProvider
            .when('/', {
                templateUrl: 'orders-view',
                controller: 'OrdersController'
            })
            .when('/register', {
                templateUrl: 'register-view',
                controller: 'RegisterController'
            })
            .when('/payments', {
                templateUrl: 'payments-view',
                controller: 'PaymentsController'
            })
            .when('/users', {
                templateUrl: 'users-view',
                controller: 'UsersController'
            })
            .when('/orders', {
                templateUrl: 'orders-view',
                controller: 'OrdersController'
            })
            .when('/user/:id', {
                templateUrl: function (url) {
                    return 'user-view?id=' + url.id;
                },
                controller: 'UserOrdersController'
            })
            .when('/order/:id', {
                templateUrl: function (url) {
                    return 'order-view?id=' + url.id;
                },
                controller: 'OrderController'
            })
            .when('/chat', {
                templateUrl: 'chat-view'
            })
            .when('/payment/:id', {
                templateUrl: function (url) {
                    return 'payment-view?id=' + url.id;
                }
            })
            .when('/register_admin', {
                templateUrl: 'register-admin-view',
                controller: 'AdminUserController'
            })
            .when('/advertisement', {
                templateUrl: 'advertisement-view',
                controller: 'AdvertisementController'
            })
            .when('/bannerDriver', {
                templateUrl: 'bannerDriver-view',
                controller: 'AdvertisementController'
            })
            .when('/bannerCustomer', {
                templateUrl: 'bannerCustomer-view',
                controller: 'AdvertisementController'
            });


    }]);
