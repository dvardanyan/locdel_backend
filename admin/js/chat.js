ll.directive('chat', ['user', '$timeout', 'env', '$sce', function (user, $timeout, env, $sce) {
    function link($scope) {
        var socket = io(env === 'production' ? 'https://www.locdel.com' : undefined);

        $scope.dateFormat = function (date) {
            var m = moment(date);
            var month = moment.months()[m.month()];
            var day = m.date();
            var timestamp = m.format("H:mm:ss");

            var TODAY = moment().startOf('day');
            var isToday = m.isSame(TODAY, 'd');

            if (isToday) {
                return timestamp;
            }

            return day + ' of ' + month;
        };

        $scope.room = {
            admin: {
                number: "1324"
            }
        };

        $scope.filter = {
            user: 2
        };

        $scope.changeRoom = function (room) {
            $scope.room = room;
            $scope.fetching = true;
            socket.emit('room-changed', room._id);
        };

        $scope.rooms = [];

        $scope.roomDate = (room) => {
            return new Date(room.modified || room.created);
        };

        $scope.$watch('filter.user', function () {
            $scope.room = {};
            $scope.messages = [];
            $scope.pending = {};
            $scope.sending = false;
        });

        socket.on('connect', function () {
            socket.emit('rooms-get', $scope.single ? $scope.user : null);
        });

        socket.on('rooms', function (rooms) {
            $scope.rooms = rooms;
            if ($scope.single && $scope.rooms.length) {
                $scope.changeRoom(rooms[0]);
            }
            $scope.$apply();
        });

        socket.on('ok', function (ok) {
            $scope.messages.push($scope.pending);
            $scope.pending = {};
            $scope.sending = false;
            $scope.$apply();
        });

        socket.on('room-messages', function (messages) {
            $scope.messages = messages;
            $timeout(function () {
                $scope.fetching = false;
            }, 200);
            $scope.room.unread = 0;
            $scope.$apply();
            socket.emit('room-read', $scope.room._id);
        });

        socket.on('newroom', function (room) {
            $scope.rooms.push(room);
        });

        socket.on('new-message', function (message) {
            var roomId = message.room, room;

            if ($scope.room._id == roomId) {
                message.new = true;
                $scope.messages.push(message);
                room = $scope.room;

                socket.emit('room-read', $scope.room._id);
            } else {
                room = $scope.rooms.find(function (r) {
                    return roomId === r._id;
                });

                room.unread = room.unread || 0;
                room.unread++;
            }

            room.modified = message.date;

            $scope.$apply();
        });

        $scope.html = function (text) {
            return $sce.trustAsHtml(text);
        };

        $scope.self = function (message) {
            if (!$scope.room.for) {
                return;
            }

            return message.from !== $scope.room.for.email;
        };

        $scope.downloadPath = function (filename, path) {
            return 'download?path=' + encodeURIComponent(path) + '&filename=' + encodeURIComponent(filename);
        };

        $scope.pending = {};
        $scope.admin = {
            number: user.adminNumber || "Undefined",
            email: user.email
        };

        $scope.$on('uploaded', function (event, attachment) {
            event.stopPropagation();

            $scope.pending.attachments = $scope.pending.attachments || [];
            $scope.pending.attachments.push(attachment);
        });

        $scope.send = function () {
            $scope.pending.to = $scope.room.for.email;
            $scope.pending.from = $scope.admin.number;
            $scope.sending = true;

            socket.emit('message', $scope.pending);
        }
    }

    return {
        restrict: 'E',
        scope: {
            user: '=',
            single: '='
        },
        templateUrl: 'chat/chat.html',
        link: link
    }
}]).filter('room', function () {
    return function filter(room, type) {
        return room.filter(r=>r.for.type == type);
    }
});