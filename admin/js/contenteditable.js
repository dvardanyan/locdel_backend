/**
 * Two-way data binding for contenteditable elements with ng-model.
 * @example
 *   <p contenteditable="true" ng-model="text"></p>
 */
ll.directive('contenteditable', function() {
    return {
        require: 'ngModel',
        link: function(scope, elm, attrs, ctrl) {
            // view -> model
            elm.bind('blur', function() {
                scope.$apply(function() {
                    ctrl.$setViewValue(elm.html().replace(/<div>/gi,'\n').replace(/<\/div>/gi,''));
                });
            });

            // model -> view
            ctrl.$render = function() {
                elm.html(ctrl.$viewValue);
            };

            // load init value from DOM
            ctrl.$setViewValue(elm.html());
        }
    };
});