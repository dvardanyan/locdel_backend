'use strict';

// utility to add methods to prototype
ll.factory('methods', [
    function () {
        return function $methods(cls, funcs) {
            for (var name in funcs) {
                if (funcs.hasOwnProperty(name)) {
                    Object.defineProperty(cls.prototype, name, {
                        value: funcs[name]
                    });
                }
            }
        };
    }
]).factory('manager', ['$http', '$q', '$timeout', 'methods', 'query',
    function ($http, $q, $timeout, methods, Query) {
        var extend = angular.extend,
            min = Math.min,
            isString = angular.isString,
            isArray = angular.isArray;

        DataManager.IDLE = 0;
        DataManager.PENDING = 1;
        function DataManager(source, params) {
            if (!(this instanceof DataManager)) {
                return new DataManager(source, params);
            }

            // configuration

            // static source
            this.source = isArray(source) ? source : null;
            // remote source
            this.url = isString(source) ? source : null;
            this.params = params;

            // state maintenance
            this.status = DataManager.IDLE;
            this.timeout = null;
            this.deferred = null;
            this.query = new Query();

            this.max(10);
        }

        methods(DataManager, {
            set: function (query, resetPosition = false) {
                if (query && !(query instanceof Query)) {
                    throw new Error('Not Query type');
                }

                this.query = query;
                this.page = resetPosition ? 0 : this.page;

                return this;
            },

            max: function (limit) {
                this.limit = limit;

                return this;
            },

            offset: function (offset) {
                this.page = ~~(offset / this.limit);

                return this;
            },

            pg: function (page) {
                this.page = page;

                return this;
            },


            gap: function (gap) {
                this.count = gap;

                return this;
            },

            sortBy: function (sort) {
                if (isArray(sort) && isString(sort[0])) {
                    this.sort = sort;
                }
                else {
                    this.sort = null;
                }

                return this;
            },

            load: function () {
                var query = this.query;
                if (!query) {
                    this.results = null;
                    return $q.reject();
                }

                // get results from remote or cached source
                return this.http();
            },

            slice: function () {
                if (!this.results) {
                    return {
                        from: 0,
                        to: 0
                    };
                }

                return {
                    from: this.page * this.limit + 1,
                    to: min((this.page + 1) * this.limit, this.results.total)
                };
            },

            can: function (action) {
                if (!this.results) {
                    return false;
                }

                var target;
                switch (action) {
                    case 'prev':
                        target = this.page - 1;
                        break;
                    case 'next':
                        target = this.page + 1;
                        break;
                    case Number(action):
                        target = action;
                        break;
                }

                if (angular.isNumber(target)) {
                    return target >= this.page ? target * this.limit < this.results.total : target >= 0;
                }

                return false;
            },

            pages: function () {
                if (!this.results) {
                    return false;
                }

                var p = [],
                    gap = this.count || 1;

                for (var i = this.page - gap; i <= this.page + gap; i++) {
                    if (this.can(i)) {
                        p.push(i);
                    }
                }

                return p;
            },

            go: function (action) {
                if (!this.can(action)) {
                    return;
                }

                switch (action) {
                    case 'prev':
                        this.page--;
                        break;
                    case 'next':
                        this.page++;
                        break;
                    case Number(action):
                        this.page = action;
                        break;
                    default:
                        return $q.reject();
                }

                return this.load();
            },

            http: function () {
                // reset debounce timeout
                $timeout.cancel(this.debounce);
                this.debounce = $timeout(200);

                // defer request
                return this.debounce.then(function () {
                    // abort existing request
                    this.abort();
                    this.status = DataManager.PENDING;

                    // request parameters
                    var params = extend(this.params || {}, {
                        page: this.page || 0,
                        limit: this.limit || 10
                    });

                    // HTTP configuration
                    var config = {
                        url: this.url,
                        method: 'POST',
                        params: params,
                        data: JSON.stringify(this.query.compile()),
                        timeout: this.defer(),
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    };

                    // success callback
                    var success = function (response) {
                        this.status = DataManager.IDLE;

                        return (this.results = response.data);
                    }.bind(this);

                    // error callback
                    var error = function (response) {
                        // if its not user aborted request
                        if (response.status > 0) {
                            this.results = null;
                            this.status = DataManager.IDLE;
                        }
                    }.bind(this);
                    // send request
                    return $http(config).then(success, error);
                }.bind(this));
            },

            defer: function () {
                var timeout = this.timeout = $q.defer();
                return timeout.promise;
            },

            abort: function () {
                var timeout = this.timeout;
                if (!timeout) {
                    return;
                }
                timeout.resolve('abort');
            },

            is: function (status) {
                return this.status === DataManager[status.toUpperCase()];
            }

        });

        return DataManager;
    }
]).factory('query', function () {
    class Query {
        constructor() {
            this.sort = [];
            this.filter = {};
            this.query = null;
        }

        setFilter(query, fields) {
            this.query = query;
            this.filter = {};

            if(!fields){
                return
            }

            if (!Array.isArray(fields)) {
                fields = [fields];
            }

            var t = this;

            fields.forEach(f=>{
                t.filter[f] = query;
            });
        }

        addFilterField(field, query) {
            this.filter[field] = query;
        }

        setQuery(query) {
            this.query = query;
        }

        buildSort(field, reverse) {
            var prefix = reverse ? -1 : 1;

            return [field, prefix];
        }

        setSort(options) {
            this.sort = options.map(s=>this.buildSort(s.field, s.reverse));
        }

        addSort(field, reverse = false) {
            this.sort.push(this.buildSort(field, reverse));
        }

        compile() {
            return {
                sort: this.sort,
                filter: this.filter
            }
        }
    }

    return Query;
});