ll.directive('gallery', ['Lightbox', '$timeout', function (Lightbox, $timeout) {
    var link = function ($scope, $element) {
        $scope.images = [];

        $scope.$watchCollection(function () {
            return $element.find('img')
        }, function (imgs) {
            if (!imgs.length) {
                return;
            }

            function toImage($i, index) {
                function click() {
                    $scope.openLightboxModal(index);
                }

                $i.removeEventListener('click', click);
                $i.addEventListener('click', click);

                return {
                    url: $i.src,
                    thumbUrl: $i.src,
                    caption: $i.getAttribute('caption')
                }
            }

            $timeout(()=> {
                $scope.images = Array.from(imgs).map((a, i)=> toImage(a, i));
            }, 200);
        });

        $scope.openLightboxModal = function (index) {
            Lightbox.openModal($scope.images, index);
        };
    };

    return {
        restrict: 'A',
        link: {
            post: link
        }
    }
}]);