ll.constant('util', {
    toLocaleTime: function (date) {
        return new Date(date);
    }
}).controller('OrdersController', ['$scope', 'manager', 'util', '$timeout', '$sce', '$compile', 'dateFormat', 'query', 'OrderStatus', 'userType', function ($scope, manager, util, $timeout, $sce, $compile, dateFormat, query, OrderStatus, userType) {
    $scope.userType = userType;
    $scope.status = {
        selected: {
            name: "All",
            value: 0
        },
        options: ["All", "Pending", "Picked", "Completed", "Canceled", "Hold", "Fail", "Inactive"].map((a, i)=> {
            return {
                name: a,
                value: i
            }
        })
    };

    $scope.searchFields = [
        {
            name: "Invoice #",
            value: "invoice"
        }
    ];

    $scope.states = [
        {
            name: 'All',
            value: 'all'
        },
        {
            name: "CA",
            value: "california",
            cities: [
                {
                    name: 'All',
                    value: 'all'
                },
                {
                    name: 'Los Angeles',
                    value: 'los angeles'
                },
                {
                    name: 'San Francisco',
                    value: 'san francisco'
                },
                {
                    name: 'Sacramento',
                    value: 'sacramento'
                },
                {
                    name: 'San Diego',
                    value: 'san diego'
                },
                {
                    name: 'Fresno',
                    value: 'fresno'
                }
            ]
        },
        {
            name: "Co",
            value: "colorado",
            cities: []
        },
        {
            name: "Washington",
            value: "wa",
            cities: []
        }
    ];


    $scope.displayed = [];
    var filter = $scope.filter = {
        column: 'invoice',
        state: $scope.states[0],
        city: 'all'
    };


    var url = 'resource/orders?status=';

    function feedback(scope, feedback, isDriver) {
        var main = '<span>{0}</span>';

        if (!feedback || !feedback.mark) {
            return main.format(isDriver ? 'D-No' : 'C-No');
        }

        var link = '<feedback-popup row="$row" is-driver="{0}"></feedback-popup>'.format(isDriver);

        return link;
    }

    function stepTemplate(step, $row) {
        if (step) {
            var image = '<span gallery class="driver-photos"><img src="{0}"/></span>';
            var video = '<video width="130" height="130"  controls>'+
             '<source src="{0}" type="video/mp4">'+
                'Lianna Your browser does not support the video tag.' +
            '</video>'

            var VideoOrImageTemplate;
            if(step.photos[0].url.includes("mp4"))
            {
                VideoOrImageTemplate = video.format(step.photos[0].url);
            }
            else {
                VideoOrImageTemplate = image.format(step.photos[0].url)
            }

            // var imageTemplate = step.photos.map(i=>image.format(i)).join(''); // old version
            return '<a href="user/{0}">{1}</a><br/><span>{2}</span><br/>{3}'
               .format($row.driver._id, $row.driver.filename || $row.driver.email, dateFormat(step.time), VideoOrImageTemplate);
        }



        return '<span></span>';
    }

    $scope.dateRange = {};
    $scope.data = [];
    $scope.options = {
        rowHeight: 30,
        headerHeight: 30,
        footerHeight: 50,
        scrollbarV: false,
        columns: [
            {
                name: "Invoice #",
                prop: "invoice",
                flexGrow: 2,
                cellRenderer: function ({$row}) {
                    return '<a href="order/{0}">{1}</a>'.format($row.id, $row.invoiceNumber)
                }
            },
            {
                name: "User",
                prop: "user",
                flexGrow: 2,
                sort: 'asc',
                cellRenderer: function ({$row}) {
                    return '<a href="user/{0}">{1}</a><br/><span>{2}</span>'.format($row.user._id,
                        $row.user.filename || $row.user.email, dateFormat($row.created));
                }
            },
            {
                name: "Type",
                prop: "type",
                sortable: false,
                flexGrow: 1
            },
            {
                name: "Vehicle",
                prop: "vehicle",
                sortable: false,
                flexGrow: 1,
                cellRenderer: function ({$row}) {
                    if ($row.vehicle === 'All') {
                        return '<span>Car<br/>Moto</span>';
                    }

                    return '<span>{0}</span>'.format($row.vehicle);
                }
            },
            {
                name: "Pick up",
                prop: "pickup",
                sortable: false,
                flexGrow: 2,
                cellRenderer: function ({$row}) {
                    var step = $row.steps.filter(function (s) {
                        return s.type === 1;
                    })[0];

                    return stepTemplate(step, $row);
                }
            },
            {
                name: "Pick up",
                prop: "pickup",
                sortable: false,
                flexGrow: 1,
                cellRenderer: function ({$row}) {
                    var step = $row.steps.filter(function (s) {
                        return s.type === 1;
                    })[1];

                    return stepTemplate(step, $row);
                }
            },
            {
                name: "Drop off",
                prop: "pickup",
                sortable: false,
                flexGrow: 2,
                cellRenderer: function ({$row}) {
                    var step = $row.steps.filter(function (s) {
                        return s.type === 2;
                    })[0];

                    return stepTemplate(step, $row);
                }
            },
            {
                name: "Drop off",
                prop: "pickup",
                sortable: false,
                flexGrow: 1,
                cellRenderer: function ({$row}) {
                    var step = $row.steps.filter(function (s) {
                        return s.type === 2;
                    })[1];

                    return stepTemplate(step, $row);
                }
            },
            {name: "Status", prop: "status", flexGrow: 1},
            {
                name: "Feedback",
                prop: "feedback",
                sortable: false,
                flexGrow: 1,
                cellRenderer: function (scope) {
                    var r = '{0}<br/>{1}';


                    r = r.format(feedback(scope, scope.$row.customerFeedback, false),
                        feedback(scope, scope.$row.driverFeedback, true));

                    return r;
                }
            }
        ],
        columnMode: 'flex',
        paging: {
            externalPaging: true,
            size: 10,
            loadingIndicator: true
        }
    };
    if($scope.userType == 0){
        $scope.options.columns.push(
            {
                name: "Picked Cost",
                prop: "cost",
                flexGrow: 1
            });
        $scope.options.columns.push(
            {
                name: "Car Cost",
                prop: "carCost",
                flexGrow: 1
            });
        $scope.options.columns.push(
            {
                name: "Moto Cost",
                prop: "motoCost",
                flexGrow: 1
            });
        $scope.options.columns.push(
            {
                name: "Pick Up Cost",
                prop: "pickUpCost",
                flexGrow: 1
            });
    }

    var m = manager(url, {dateRange: $scope.dateRange});
    var qq = $scope.query = new query();

    $scope.search = function (q) {
        qq.setFilter(q, [filter.column]);

        m.set(qq);
        m.offset(0).load().then(render.bind(this, 0, 10));
    };

    $scope.total = {};
    $scope.OrderStatus = OrderStatus;

    function render(offset, size, result) {
        if (!result) {
            return;
        }

        var set = result.entities;

        $scope.data = [];
        set.forEach(function (r, i) {
            var idx = i + (offset * size);
            $scope.data[idx] = r;
        });

        $scope.total.amount = result.amount;
        $scope.total.statusTotal = result.statusTotal;
        $scope.options.paging.count = result.total;
        if (!result.total) {
            $scope.data = [];
        }

        $scope.options.paging.loadingIndicator = false;
    }

    $scope.sorting = function (options) {
        qq.setSort(options.map(o=> {
            return {
                field: o.prop,
                reverse: o.sort === 'asc'
            }
        }));

        m.set(qq);
        m.load().then(render.bind(this, m.page, m.limit));
    };

    $scope.$watch('dateRange', function (newVal) {
        if (newVal) {
            m.offset(0).load().then(render.bind(this, 0, m.limit));
        }
    }, true);


    $scope.paging = function (offset, size) {
        $scope.options.paging.loadingIndicator = true;
        m.max(size).pg(offset).load().then(render.bind(this, offset, size));
    };
}
]).controller('OrderController', ['$scope', '$http', '$location', '$routeParams', '$window', 'util', 'userType',  function ($scope, $http, $location, $routeParams, $window, util, userType) {
    $scope.dateFormat = function (str) {
        return moment(util.toLocaleTime(str)).format('YYYY-MM-DD hh:mm:ss');
    };

    $scope.addComment = function () {

        if (!$scope.comment) {
            return;
        }

        $http.post('/order/comment', {
            comment: $scope.comment,
            orderId: $routeParams.id
        }).success(function () {
            // hard reload
            $window.location.reload();
        })
    }
}]).constant('OrderStatus', {
    PENDING: 1,
    PICKED: 2,
    COMPLETED: 3,
    CANCELED: 4,
    HOLD: 5,
    FAIL: 6,
    INITIAL: 7,
    INACTIVE: 8
});
