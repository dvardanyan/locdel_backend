'use strict';

addPrototypeMethod(String, 'capitalize', function () {
    return this.substr(0, 1).toUpperCase() + this.substr(1);
});

addPrototypeMethod(String, 'format', function () {
    var args = arguments, i = 0;
    return this.replace(/{(\d+)?}/g, function (match, number) {
        return args[number !== void 0 ? number : i++];
    });
});

// http://jsperf.com/startswith-implementations
addPrototypeMethod(String, 'startsWith', function (prefix) {
    return this.indexOf(prefix) === 0;
});

addPrototypeMethod(String, 'endsWith', function (suffix) {
    var index = this.indexOf(suffix);
    return index !== -1 && index === this.length - suffix.length;
});

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/includes
addPrototypeMethod(String, 'includes', function () {
    return String.prototype.indexOf.apply(this, arguments) !== -1;
});

addPrototypeMethod(String, 'encodeHTML', function () {
    return this
        .replace(/&/g, '&amp;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;')

        // cannot replace all spaces with &nbsp; entities, since this does not give text ability to break at all.
        // instead, I am replacing every other space with &nbsp; since this retains the maximum number of possible
        // break points while retaining all white-space.
        //
        // Test fiddle for this mechanism: http://jsfiddle.net/zk5copeu/
        .replace(/[ ]{2}/gi, function () {
            return " &nbsp;";
        })
        .replace(/"/g, '&quot;');
});

addPrototypeMethod(String, 'decodeHTML', function () {
    return this
        .replace(/&amp;/g, '&')
        .replace(/&lt;/g, '<')
        .replace(/&gt;/g, '>')
        .replace(/&nbsp;/g, ' ')
        .replace(/&quot;/g, '"');
});

/* istanbul ignore next */
addPrototypeMethod(CanvasRenderingContext2D, 'roundRect', function (x, y, width, height, radius, fillStyle) {
    this.beginPath();
    this.moveTo(x + radius, y);
    this.lineTo(x + width - radius, y);
    this.quadraticCurveTo(x + width, y, x + width, y + radius);
    this.lineTo(x + width, y + height - radius);
    this.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
    this.lineTo(x + radius, y + height);
    this.quadraticCurveTo(x, y + height, x, y + height - radius);
    this.lineTo(x, y + radius);
    this.quadraticCurveTo(x, y, x + radius, y);
    this.fillStyle = fillStyle;
    this.fill();
});

addPrototypeMethod(Array, 'first', function () {
    return this[0];
});

addPrototypeMethod(Array, 'last', function () {
    return this[this.length - 1];
});

addPrototypeMethod(Array, 'peek', function () {
    return this.last();
});

addPrototypeMethod(Array, 'delete', function (item) {
    var index = this.indexOf(item);
    if (index > -1) {
        this.splice(index, 1);
        return true;
    }

    return false;
});

addPrototypeMethod(Array, 'add', function (item) {
    var index = this.indexOf(item);
    if (index === -1) {
        this.push(item);
        return true;
    }

    return false;
});

// http://stackoverflow.com/questions/281264/remove-empty-elements-from-an-array-in-javascript
addPrototypeMethod(Array, 'clean', function (deleteValue) {
    for (var i = 0; i < this.length; ++i) {
        if (this[i] == deleteValue) {
            this.splice(i--, 1);
        }
    }
    return this;
});

addPrototypeMethod(Array, 'empty', function () {
    this.splice(0, this.length);
});

/* istanbul ignore else */
if (!Array.prototype.find) {
    addPrototypeMethod(Array, 'find', function (predicate) {
        /* istanbul ignore if */
        if (this === null) {
            throw new TypeError('Array.prototype.find called on null or undefined');
        }
        if (typeof predicate !== 'function') {
            throw new TypeError('undefined is not a function');
        }
        var list = Object(this);
        var length = list.length >>> 0;
        var thisArg = arguments[ 1 ];
        var value;

        for (var i = 0; i < length; i++) {
            value = list[ i ];
            if (predicate.call(thisArg, value, i, list)) {
                return value;
            }
        }
        return undefined;
    });
}

// http://stackoverflow.com/questions/1669190/javascript-min-max-array-values
addPrototypeMethod(Array, 'max', function () {
    return Math.max.apply(null, this);
});

// http://stackoverflow.com/questions/1669190/javascript-min-max-array-values
addPrototypeMethod(Array, 'min', function () {
    return Math.min.apply(null, this);
});

addPrototypeMethod(Array, 'swap', function (i, j) {
    var tmp = this[i];
    this[i] = this[j];
    this[j] = tmp;

    return this;
});

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/includes
addPrototypeMethod(Array, 'includes', function (searchElement /*, fromIndex*/) {
    /* istanbul ignore if */
    if (this === undefined || this === null) {
        throw new TypeError('Cannot convert this value to object');
    }
    var O = Object(this);
    var len = parseInt(O.length) || 0;
    if (len === 0) {
        return false;
    }
    var n = parseInt(arguments[1]) || 0;
    var k;
    if (n >= 0) {
        k = n;
    } else {
        k = len + n;
        if (k < 0) {
            k = 0;
        }
    }
    while (k < len) {
        var currentElement = O[k];
        if (searchElement === currentElement) {
            return true;
        }
        k++;
    }
    return false;
});

addPrototypeMethod(Array, 'sortBy', function (property) {
    return this.sort(function (a, b) {
        var ap = a[property],
            bp = b[property];
        return ap < bp ? -1 : ap === bp ? 0 : 1;
    });
});

/* istanbul ignore else */
if (!Math.clip) {
    // http://jsperf.com/math-clip
    Math.clip = function (value, min, max) {
        if (min > max) {
            var tmp = min;
            min = max;
            max = tmp;
        }
        return value >= max ? max : value <= min ? min : value;
    };
}

/* istanbul ignore else */
if (!Math.roundTo) {
    Math.roundTo = function (value, multiple) {
        return Math.round(value / multiple) * multiple;
    };
}

/* istanbul ignore else */
if (!Math.sign) {
    Math.sign = function (value) {
        return value < 0 ? -1 : value > 0 ? 1 : 0;
    };
}

/* istanbul ignore next */
(function () {
    // http://paulirish.com/2011/requestanimationframe-for-smart-animating/
    // http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating

    // requestAnimationFrame polyfill by Erik Möller. fixes from Paul Irish and Tino Zijdel

    // MIT license

    var lastTime = 0;
    var vendors = [ 'ms', 'moz', 'webkit', 'o' ];
    for (var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[x] + 'CancelAnimationFrame'] ||
            window[vendors[x] + 'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame) {
        window.requestAnimationFrame = function (callback) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function () {
                    callback(currTime + timeToCall);
                },
                timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };
    }

    if (!window.cancelAnimationFrame) {
        window.cancelAnimationFrame = function (id) {
            clearTimeout(id);
        };
    }
}());

////////////////////

function addPrototypeMethod(cls, key, method) {
    var proto = cls.prototype;

    /* istanbul ignore else */
    if (!proto[key]) {
        Object.defineProperty(proto, key, {
            writable: true,
            enumerable: false,
            value: method
        });
    }
}
