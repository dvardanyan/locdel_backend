ll.config(['$uibTooltipProvider', function ($uibTooltipProvider) {
    $uibTooltipProvider.options({appendToBody: true});
}]).directive('loadingButton', function () {
    return {
        scope: {
            'loading': '=',
            'content': '@',
            'type': '@',
            'click': '='
        },
        template: '<button class="btn btn-{{type}} btn-sm" ng-disabled="loading" ng-click="click()">' +
        '<i class="spinner fa fa-circle-o-notch fa-spin" ng-show="loading"></i><span>{{content}}</span></button>' +
        '</div>'
    }
}).directive('feedback', function () {
    function link($scope) {
        $scope.max = $scope.max || 5;
        $scope.range = function (n) {
            n = n || 0;
            n = Math.round(n);

            if (n > $scope.max) {
                n = $scope.max;
            }

            if (n < 0) {
                n = 0;
            }

            return new Array(n).fill(1).map((a, i)=>i);
        }
    }

    return {
        scope: {
            'mark': '=',
            'max': '='
        },
        restriction: 'E',
        link: link,
        template: '<div class="feedback" tooltip="{{mark | number:2}}">' +
        '<i class="glyphicon glyphicon-star" ng-repeat="n in range(mark)"></i>' +
        '<i class="glyphicon glyphicon-star-empty" ng-repeat="n in range(max-mark)"></i>' +
        '</div>'
    }
}).directive('feedbackPopup', function () {
    return {
        scope: {
            isDriver: '=',
            row: '='
        },
        link: function ($scope) {
            var isDriver = $scope.isDriver;
            var d = $scope.row.driver;
            var c = $scope.row.user;
            var r = $scope.row;

            $scope.target = $scope.isDriver ? c : d;
            $scope.feedback = $scope.isDriver ? r.driverFeedback : r.customerFeedback;
            $scope.short = isDriver ? 'D' : 'C';
            $scope.long = isDriver ? 'Driver' : 'Customer';
        },
        restriction: 'E',
        template: '<span class="click-like" popover-title="Feedback from {{long}}" \
                popover popover-template="\'/templates/feedback.html\'">\
                {{short}}-Yes</span>'
    }
}).directive('datePicker', function () {
    return {
        restriction: 'E',
        scope: {
            range: '=',
            opens: '@'
        },
        link: function ($scope) {
            $scope.clearRange = function () {
                delete $scope.range.start;
                delete $scope.range.end;
            };

            $scope.options = {
                dropsUp: true,
                opens: 'right',
                disabled: false,
                autoApply: false,
                calendarsAlwaysOn: true,
                range: {
                    start: moment(),
                    end: moment()
                },

                apply: function (start, end) {
                    $scope.range.start = start;
                    $scope.range.end = end;
                },

                ranges: [
                    {
                        name: 'Today',
                        start: moment().startOf('day'),
                        end: moment().add(1, 'd').startOf('day')
                    },
                    {
                        name: 'Yesterday',
                        start: moment().subtract(1, 'd').startOf('day'),
                        end: moment().startOf('day')
                    },
                    {
                        name: 'Current Month',
                        start: moment().startOf('month'),
                        end: moment()
                    }
                ],
            }

        },
        template: '<div class="date-picker input-group">\
        <ob-daterangepicker ranges="options.ranges" format="options.format" on-apply="options.apply(start, end)"\
        calendars-always-on="options.calendarsAlwaysOn" class="{{opens}}" ng-class="{\'unused\': !range.start}">\
        </ob-daterangepicker>\
        <div class="input-group-btn">\
        <button class="btn btn-info" ng-click="clearRange()">\
        <span class="glyphicon glyphicon-remove"></span>\
        </button>\
        </div></div>'
    }
}).directive('search', function () {
        return {
            scope: {
                find: '='
            },
            restriction: 'ECA',
            templateUrl: 'templates/search.html',
        };
    }
);








