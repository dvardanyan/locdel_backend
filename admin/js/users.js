ll.controller('UsersController', ['$scope','$modal', 'manager', 'util', '$timeout', '$sce', '$compile', 'dateFormat', 'query', 'OrderStatus', '$http', '$routeParams', 'userType',
    function ($scope,$modal, manager, util, $timeout, $sce, $compile, dateFormat, query, OrderStatus, $http, $routeParams, userType,$modalInstance) {
        $scope.displayed = [];
        var filter = $scope.filter = {
            column: 'name'
        };


        var url = 'resource/users';

        $scope.dateRange = {};
        $scope.data = [];
        $scope.searchFields = [
            {
                name: "Name",
                value: "name"
            },
            {
                name: "Email",
                value: "email"
            },
            {
                name: "Contact Number",
                value: "contactNumber"
            },
            {
                name: "Address",
                value: "address"
            }
        ];

        $scope.blockedFields = [
            {
                name: "All",
                value: undefined
            },
            {
                name: "Blocked",
                value: true
            },
            {
                name: "Unblocked",
                value: false
            }
        ];

        $scope.options = {
            rowHeight: 30,
            headerHeight: 30,
            footerHeight: 50,
            scrollbarV: false,
            columnMode: 'flex',
            paging: {
                externalPaging: true,
                size: 10,
                loadingIndicator: true
            },
            columns: [
                {
                    name: "User",
                    prop: "user",
                    flexGrow: 2,
                    cellRenderer: function ({$row}) {
                        return '<a href="user/{0}">{1}</a><br/><span>{2}</span>'.format($row._id,
                            $row.filename || $row.email, dateFormat($row.created));
                    }
                },
                {
                    name: "Email",
                    prop: "email",
                    flexGrow: 1
                },
                {
                    name: "Billing Address",
                    prop: "billingAddress",
                    flexGrow: 1
                },
                {
                    name: "Contact Number",
                    prop: "contactNumber",
                    flexGrow: 1
                },
                {
                    name: "Created",
                    prop: "created",
                    flexGrow: 1,
                    sort: 'asc',
                    cellRenderer: function ({$row}) {
                        return '<span>{{$row.created | dateFormat}}</span>';
                    }
                },
                {
                    name: "Blocked",
                    prop: "blocked",
                    flexGrow: 1,
                    cellRenderer: function ({$row}) {
                        return '<span class="label label-default">{0}</span>'.format($row.blocked ? 'Yes' : 'No');
                    }
                }]
        };

        var m = manager(url, {dateRange: $scope.dateRange, user: $scope.userParam});
        var qq = $scope.query = new query();

        $scope.search = function (q) {
            qq.setFilter(q, [filter.column]);

            m.set(qq);
            m.offset(0).load().then(render.bind(this, 0, 10));
        };

        $scope.total = {};
        $scope.OrderStatus = OrderStatus;

        function render(offset, size, result) {
            if (!result) {
                return;
            }

            var set = result.entities;
            $scope.data = [];
            set.forEach(function (r, i) {
                var idx = i + (offset * size);
                $scope.data[idx] = r;
            });

            $scope.total.amount = result.amount;
            $scope.options.paging.count = result.total;
            if (!result.total) {
                $scope.data = [];
            }

            $scope.options.paging.loadingIndicator = false;
        }

        $scope.sorting = function (options) {
            qq.setSort(options.map(o=> {
                return {
                    field: o.prop,
                    reverse: o.sort === 'asc'
                }
            }));

            m.set(qq);
            m.load().then(render.bind(this, m.page, m.limit));
        };

        $scope.$watch('dateRange', function (newVal) {
            if (newVal) {
                m.offset(0).load().then(render.bind(this, 0, m.limit));
            }
        }, true);

        $scope.$watch('filter.blocked', function (newVal) {
            qq.addFilterField('blocked', filter.blocked);
            m.offset(0).load().then(render.bind(this, 0, m.limit));
        });

        $scope.paging = function (offset, size) {
            $scope.options.paging.loadingIndicator = true;
            m.max(size).pg(offset).load().then(render.bind(this, offset, size));
        };




        $scope.open = function() {
            $scope.$modalInstance = $modal.open({
                scope: $scope,
                templateUrl: "/templates/popup.html" ,
                size: '',
            })
        };
        $scope.mailtext="";
        $scope.ok = function(val) {

            m.max(10000).pg(0).load().then(function (alldata) {
                var resEmail='';
                alldata.entities.forEach(function (d) {
                    resEmail = resEmail.concat(d.email);
                })
                var mailOptions = {
                    from: resEmail,
                    text: val
                };
                $http.post('sendGroupMsg', mailOptions).then(function (res) {

                }, function (err) {

                });

            });
            $scope.$modalInstance.close();
        };

        $scope.cancel = function() {
            $scope.$modalInstance.dismiss('cancel');
        };


    }
]).controller('UserOrdersController', ['$scope', 'manager', 'util', '$timeout', '$sce', '$compile', 'dateFormat', 'query', 'OrderStatus', '$http', '$routeParams', '$route', '$modal', 'userType',
    function ($scope, manager, util, $timeout, $sce, $compile, dateFormat, query, OrderStatus, $http, $routeParams, $route, $modal, userType) {
        $scope.displayed = [];
        $scope.userType = userType;
        var filter = $scope.filter = {};
        var url = 'resource/user-orders';

        function feedback(scope, feedback, isDriver) {
            var main = '<span>{0}</span>';

            if (!feedback || !feedback.mark) {
                return main.format(isDriver ? 'D-No' : 'C-No');
            }

            var link = '<feedback-popup row="$row" is-driver="{0}"></feedback-popup>'.format(isDriver);

            return link;
        }

        function stepTemplate(step, $row) {
            if (step) {
                var image = '<span gallery class="driver-photos"><img src="{0}"/></span>';
                var video = '<video width="130" height="130"  controls>'+
                    '<source src="{0}" type="video/mp4">'+
                    'Your browser does not support the video tag.' +
                    '</video>'

                var VideoOrImageTemplate;
                if(step.photos[0].url.includes("mp4"))
                {
                    VideoOrImageTemplate = video.format(step.photos[0].url);
                }
                else {
                    VideoOrImageTemplate = image.format(step.photos[0].url)
                }

                // var imageTemplate = step.photos.map(i=>image.format(i)).join(''); // old version
                return '<a href="user/{0}">{1}</a><br/><span>{2}</span><br/>{3}'
                    .format($row.driver._id, $row.driver.filename || $row.driver.email, dateFormat(step.time), VideoOrImageTemplate);
            }

            return '<span></span>';
        }

        var userParam = $scope.userParam = {};
        $http.get('user-info', {params: {id: $routeParams.id}}).then(({data})=> {
            if (data.status === 'OK') {
                var user = $scope.user = data.data;
                userParam.id = user._id;
                userParam.type = user.type;
                userParam.blocked = user.blocked;

                $scope.$watch('userParam.blocked', function (newVal, old) {
                    if (typeof(newVal) !== "boolean" || newVal == old) {
                        return;
                    }

                    $scope.disableSwitch = true;

                    $http.get('block', {params: {userId: userParam.id, unblock: !newVal}}).then(({data})=> {
                        if (data.status != 'OK') {
                            userParam.blocked = !userParam.blocked;
                        }
                        $scope.disableSwitch = false;
                        $scope.apply();
                    })
                });

                if (user.type == 'Driver') {
                    var imageMap = {
                        vehicleFront: 'Vehicle Front',
                        vehicleBack: 'Vehicle Back',
                        vehicleReg: 'Vehicle Reg.',
                        vehicleInsurance: 'Vehicle Insurance',
                        driverLicence: 'Driver License',
                        driverPhoto: 'Driver Photo'
                    };

                    var images = Object.keys(imageMap).filter(s=>user.car.images[s]).map(s=> {
                        return {name: imageMap[s], value: user.car.images[s]}
                    });

                    user.car.images = images;
                }
            }
        });

        $scope.downloadPath = function (path, filename) {
            if (!filename || !path) {
                return '#';
            }

            return 'download?path=' + encodeURIComponent(path) + '&filename=' + encodeURIComponent(filename);
        };

        $scope.fileTypeOptions = [
            {name: 'Claim', value: 'claims'},
            {name: 'Bg.Ch.', value: 'backgroundCheck'},
            {name: 'Au. Ins.', value: 'insurance'},
            {name: 'Au. Reg.', value: 'registration'},
        ];

        $scope.fileType = $scope.fileTypeOptions[0].value;

        $scope.$on('uploaded', function (event, attachment) {
            $http.get('add-file', {
                params: {
                    path: attachment.path,
                    filename: attachment.filename,
                    user: $scope.userParam.id,
                    type: $scope.fileType
                }
            }).then(function (data) {
                $route.reload();
            })
        });

        $scope.dateRange = {};
        $scope.data = [];
        $scope.options = {
            rowHeight: 30,
            headerHeight: 30,
            footerHeight: 50,
            scrollbarV: false,
            columns: [
                {
                    name: "Invoice #",
                    prop: "invoice",
                    flexGrow: 2,
                    cellRenderer: function ({$row}) {
                        return '<a href="order/{0}">{1}</a>'.format($row.id, $row.invoiceNumber)
                    }
                },
                {
                    name: "User",
                    prop: "user",
                    sort: 'asc',
                    flexGrow: 2,
                    cellRenderer: function ({$row}) {
                        return '<a href="user/{0}">{1}</a><br/><span>{2}</span>'.format($row.user._id,
                            $row.user.fi || $row.user.email, dateFormat($row.created));
                    }
                },
                {
                    name: "Type",
                    prop: "type",
                    sortable: false,
                    flexGrow: 1
                },
                {
                    name: "Vehicle",
                    prop: "vehicle",
                    sortable: false,
                    flexGrow: 1,
                    cellRenderer: function ({$row}) {
                        if ($row.vehicle === 'All') {
                            return '<span>Car<br/>Moto</span>';
                        }

                        return '<span>{0}</span>'.format($row.vehicle);
                    }
                },
                {
                    name: "Pick up",
                    prop: "pickup",
                    sortable: false,
                    flexGrow: 2,
                    cellRenderer: function ({$row}) {
                        var step = $row.steps.filter(function (s) {
                            return s.type === 1;
                        })[0];

                        return stepTemplate(step, $row);
                    }
                },
                {
                    name: "Pick up",
                    prop: "pickup",
                    sortable: false,
                    flexGrow: 1,
                    cellRenderer: function ({$row}) {
                        var step = $row.steps.filter(function (s) {
                            return s.type === 1;
                        })[1];

                        return stepTemplate(step, $row);
                    }
                },
                {
                    name: "Drop off",
                    prop: "pickup",
                    sortable: false,
                    flexGrow: 2,
                    cellRenderer: function ({$row}) {
                        var step = $row.steps.filter(function (s) {
                            return s.type === 2;
                        })[0];

                        return stepTemplate(step, $row);
                    }
                },
                {
                    name: "Drop off",
                    prop: "pickup",
                    sortable: false,
                    flexGrow: 1,
                    cellRenderer: function ({$row}) {
                        var step = $row.steps.filter(function (s) {
                            return s.type === 2;
                        })[1];

                        return stepTemplate(step, $row);
                    }
                },
                {name: "Status", prop: "status", flexGrow: 1},
                {
                    name: "Feedback",
                    prop: "feedback",
                    sortable: false,
                    flexGrow: 1,
                    cellRenderer: function (scope) {
                        var r = '{0}<br/>{1}';


                        r = r.format(feedback(scope, scope.$row.customerFeedback, false),
                            feedback(scope, scope.$row.driverFeedback, true));

                        return r;
                    }
                }
            ],
            columnMode: 'flex',
            paging: {
                externalPaging: true,
                size: 10,
                loadingIndicator: true
            }
        };
        if($scope.userType == 0){
            $scope.options.columns.push(
                {
                    name: "Amount",
                    prop: "cost",
                    flexGrow: 1
                });
        }
        var m = manager(url, {dateRange: $scope.dateRange, user: $scope.userParam});
        var qq = $scope.query = new query();

        $scope.search = function (q) {
            qq.setFilter(q, [filter.column]);

            m.set(qq);
            m.offset(0).load().then(render.bind(this, 0, 10));
        };

        $scope.total = {};
        $scope.OrderStatus = OrderStatus;

        function render(offset, size, result) {
            if (!result) {
                return;
            }

            var set = result.entities;

            $scope.data = [];
            set.forEach(function (r, i) {
                var idx = i + (offset * size);
                $scope.data[idx] = r;
            });

            $scope.total.amount = result.amount;
            $scope.total.statusTotal = result.statusTotal;
            $scope.options.paging.count = result.total;
            if (!result.total) {
                $scope.data = [];
            }

            $scope.options.paging.loadingIndicator = false;
        }

        $scope.sorting = function (options) {
            qq.setSort(options.map(o=> {
                return {
                    field: o.prop,
                    reverse: o.sort === 'asc'
                }
            }));

            m.set(qq);
            m.load().then(render.bind(this, m.page, m.limit));
        };

        $scope.$watch('dateRange', function (newVal) {
            if (newVal) {
                m.offset(0).load().then(render.bind(this, 0, m.limit));
            }
        }, true);


        $scope.paging = function (offset, size) {
            $scope.options.paging.loadingIndicator = true;
            m.max(size).pg(offset).load().then(render.bind(this, offset, size));
        };

        var modalInstance;
        // modal for feedback
        $scope.openFeedbacks = function () {
            modalInstance = $modal.open({
                templateUrl: '/templates/feedbacks.html',
                scope: $scope
            });
        };

        $scope.closeFeedbacks = function () {
            modalInstance.dismiss('cancel');
        }
    }
]).controller('AdminUserController', ['$scope', '$http', function ($scope, $http) {
    $scope.save = function () {
        var form = $scope.newAdminUser,
            username = form.username,
            password = form.password,
            userType = $scope.userType,
            firstName = form.firstName,
            lastName = form.lastName;

        if (form.$valid) {
            $scope.err = null;
            $http.post('register/user', {
                email: username.$modelValue,
                password: password.$modelValue,
                userType: userType,
                firstName: firstName.$modelValue,
                lastName: lastName.$modelValue
            }).then(function (res) {
                $scope.state = {
                    type: res.data.status === 'OK' ? 'success' : 'danger',
                    message: res.data.status === 'OK' ? 'Admin User Registered' : res.data.message
                }
            }, function (err) {
                $scope.state = {
                    type: 'danger',
                    message: err.data
                }
            });
        }
    }
}]);
