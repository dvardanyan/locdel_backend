// Load the http module to create an http server.
var http = require('http'),
    express = require('express'),
    mongoose = require('mongoose'),
    bodyParser = require('body-parser'),
    passport = require('passport'),
    flash = require('connect-flash'),
    errorhandler = require('errorhandler'),
    expressSession = require('express-session'),
    util = require('util'),
    configs = require('./config/config'),
    subdomain = require('express-subdomain');

var app = express();

// env
var isProd = app.get('env') === 'production';

if (isProd) {
    process.env.HOME = '/home/ubuntu';
}

MongoStore = require('connect-mongo')(expressSession);
app.use(expressSession({
    secret: 'LucyInTheSkyWithDiamonds',
    resave: true,
    saveUninitialized: true,
    cookie: {maxAge: 2628000000},
    store: new MongoStore({mongooseConnection: mongoose.connection})
}));

app.use(passport.initialize());
app.use(passport.session());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(flash());
app.use(express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/site'));
app.use('/admin', express.static(__dirname + '/public'));
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');


app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'https://customer.locdel.com');
    res.setHeader('Access-Control-Allow-Origin', 'https://advert.locdel.com');
    //res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

if (isProd) {
    app.use(errorhandler({dumpExceptions: true, showStack: true}));
} else {
    app.use(errorhandler());
}

//init routes
var routers = require('./routes/index.js')(passport);
//init passport
require('./passport/init.js')(passport);
require('./controllers/upload.js')(app, routers.api);

// api router
app.use('/api', routers.api);
app.use('/admin', routers.admin);
app.use(routers.site);
app.use('/advertisement',routers.site);

var mongooseOptions = isProd ? {user: configs.db.user, pass: configs.db.password} : {};
var dbUrl = isProd ? 'mongodb://172.31.13.163/locdel' : 'mongodb://localhost/locdel';

mongoose.connect(dbUrl, mongooseOptions,
    function (a) {
        if (a) {
            util.log("Cannot connect to DB");
        }
        else {
            util.log("Connected to DB");
        }
    });

var Scheduler = require('./controllers/scheduler.js');
var server = app.listen(8000, function () {
    util.log('Start working.....');

    Scheduler.reload(function (err, actives) {
        if (err) {
            return console.log('Error during jobs activation ' + JSON.stringify(err));
        }
        console.log('Active scheduled jobs count = ' + actives.length);
    });

    // if run as root, downgrade to the owner of this file
    if (process.getuid() === 0)
        require('fs').stat(__filename, function (err, stats) {
            if (err) return console.log(err);
            process.setuid(stats.uid);
        });
});

// mail server
require('./mail-chat/chat')(server);


// payment processor
require('./payment-processor/drivers');
require('./payment-processor/orders');
