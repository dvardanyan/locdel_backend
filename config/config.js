/**
 * Created by vahagn on 10/12/16.
 */
var fs = require('fs');
var os = require('os');
var isProd = process.env.NODE_ENV === 'production';
var configPath = (isProd ? '/home/ubuntu' : os.homedir()) + '/locdel-config/sensitive.json';
var parsed = JSON.parse(fs.readFileSync(configPath, 'UTF-8'));
module.exports = parsed;


