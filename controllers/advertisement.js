var Advertisement = require('../models/advertisement/advertisement.js'),
    errors = require('../public/errors.js'),
    validator = require('validator'),
    _ = require('underscore');

function getAdvertisements(callback) {
    Advertisement
        .find({isDeleted: false },
            function (err, advertisements) {
            if (err) {
                return callback(errors.get(120, [err]));
            }

            var result = errors.noError();

            result.count = advertisements.length;

            result.advertisements = advertisements;

            callback(null, result);
        });

}

function setAdvertisement(userId, type, url, filename, text, callback) {

    var advertisement = new Advertisement();
    var file = {
        url: url,
        filename: filename,
        date: Date.now()
    };
    advertisement.file = file;
    advertisement.text = text;

    advertisement.save(function (err) {
        if (err) {
            return callback(errors.get(120, [err]));
        }

        callback(advertisement);
    });
}

function deleteAdvertisement(fileid, isDeleted ,callback) {
    Advertisement.update({
        _id: fileid,

    }, {$set: {isDeleted: isDeleted }}, function (err, modified) {
        if (err || modified.nModified !== 1) {
            return callback(errors.get(120, [err]));
        }

        callback(null);
    });
}
function updateAdvertisement(fileid, forDriver, forCustomer ,callback) {
    Advertisement.update({
        _id: fileid,

    }, {$set: {forDriver: forDriver, forCustomer: forCustomer }}, function (err, modified) {
        if (err || modified.nModified !== 1) {
            return callback(errors.get(120, [err]));
        }

        callback(null);
    });
}
module.exports = {
    GetAdvertisements: getAdvertisements,
    SetAdvertisement: setAdvertisement,
    DeleteAdvertisement: deleteAdvertisement,
    UpdateAdvertisement: updateAdvertisement
};