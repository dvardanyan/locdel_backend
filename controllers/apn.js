var apn = require('apn');
var expandPath = require('../util/core-util').expandPath;
var isProd = process.env.NODE_ENV === 'production';
var configs = require('../config/config');
var util = require('util');


var devOptions = {
    token: {
        key: expandPath(configs.certificates.apn_p8),
        keyId: configs.credentials.apn.keyId,
        teamId: configs.credentials.apn.teamId
    },
    production: false
};

var prodOptions = {
    token: {
        key: expandPath(configs.certificates.apn_p8),
        keyId: configs.credentials.apn.keyId,
        teamId: configs.credentials.apn.teamId
    },
    connectionTimeout: 0,
    production: true
};

var apnProvider = new apn.Provider(prodOptions),
    apnDevProvider = new apn.Provider(devOptions);

module.exports = {
    create: function (token, message, payload, action, usertype) {
        var note = new apn.Notification();
        token = token.replace(/ /g, '')


        note.expiry = Math.floor(Date.now() / 1000) + 5 * 3600;
        note.alert = message;
        note.payload = payload;
        note.sound = "default";
        note.action = action;
        //note.topic = "com.ios.locdel";
        if(usertype == 2)
        {
            note.topic = "com.locdel.LocdelDriver";
        }
        else {
            note.topic = "com.locdel.LocdelCustomer";
        }


        if (isProd) {
            return apnDevProvider.send(note, token).then((result) => {

                if (result.failed.length) {
                    util.log(result.failed);
                }
            });
        }

        apnDevProvider.send(note, token).then((result) => {
            if (result.failed.length) {
                util.log(result.failed);
            }
        });
    }
};