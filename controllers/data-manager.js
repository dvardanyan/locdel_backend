'use strict';

var _ = require('underscore');

var User = require('../models/user/user.js'),
    UserType = require('../models/user/user-type.js'),
    Order = require('../models/order/order.js'),
    Notification = require('../models/order/notification.js'),
    NotificationStatus = require('../models/order/notification-status.js'),
    OrderStatus = require('../models/order/order-status.js'),
    OrderType = require('../models/order/order-type.js'),
    OrderFilters = require('../models/order/order-filters.js'),
    utils = require('../util/core-util.js');

class Manager {
    constructor(params) {
        this.Mapper = {};
        this.params = params;
        this.limit = parseInt(params.limit, 10) || 10;
        this.offset = (params.page || 0) * this.limit;
        this.onlyCount = params.onlyCount || false;
    }

    static sortingOptions(sorting, fieldMapper) {
        if (!sorting) {
            return null;
        }

        if (!Array.isArray(sorting)) {
            return [];
        }

        var result = {};

        sorting.forEach(s=> {
            if (!Array.isArray(sorting)) {
                return;
            }

            if (fieldMapper[s[0]]) {
                result[fieldMapper[s[0]]] = s[1];
            }
        });

        return result;
    }

    static filterOptions(search, fieldMapper) {
        if (!search) {
            return null;
        }

        var result = {};

        for (var c in search) {
            if (fieldMapper[c]) {
                var val = typeof search[c] === 'string' ? {'$regex': new RegExp(search[c], "i")} : search[c];

                result[fieldMapper[c]] = val;
            }
        }

        return result;
    }

    get model() {
        if (!this._model) {
            throw new Error('Model should be defined');
        }

        return this._model
    }

    set model(model) {
        this._model = model;
    }

    get filter() {
        return Manager.filterOptions(this.params.filter, this.Mapper);
    }

    get sorting() {
        return Manager.sortingOptions(this.params.sort, this.Mapper);
    }

    query() {
        return this.model.find(this.filter).sort(this.sorting);
    }

    call(cb) {
        this.result(function (err, results) {
            cb(err, results);
        });
    }

    count(cb) {
        this.query().count(function (err, count) {
            if (err) {
                return cb(err);
            }

            cb(null, count)
        });
    }

    result(cb) {
        var self = this;

        if(self.onlyCount){
            self.count(function (err, count) {
                if (err) {
                    return cb(err);
                }

                var r = {
                    'total': count,
                    'count': count
                };

                cb(null, r);
            });

            return;
        }

        self.query().limit(this.limit).skip(this.offset).exec(function (err, entities) {
            if (err) {
                return cb(err);
            }

            self.count(function (err, count) {
                if (err) {
                    return cb(err);
                }

                var r = {
                    'entities': entities.map(self.entity),
                    'total': count,
                    'count': entities.length
                };

                cb(null, r);
            });
        });
    }

    entity(e) {
        return e;
    }
}

class OrderManager extends Manager {
    constructor(params) {
        super(params);
        this.Mapper = {
            invoice: "invoiceNumber",
            cost: 'cost',
            user: 'created'
        };
        this.model = Order;
    }

    call(cb) {
        var self = this;

        this.total(function (err, totalResult) {
            if (err) {
                return cb(err);
            }

            self.result(function (err, result) {
                if (err) {
                    return cb(err);
                }

                result = _.extend(result, totalResult);

                cb(null, result);
            })
        })
    }

    get filter() {
        var additional = {};

        if (this.params.dateRange) {
            var dateRange = JSON.parse(this.params.dateRange);

            if (dateRange.start) {
                additional.created = additional.created || {};
                additional.created['$gte'] = new Date(dateRange.start);
            }

            if (dateRange.end) {
                additional.created = additional.created || {};
                additional.created['$lte'] = new Date(dateRange.end);
            }
        }

        return _.extend({}, super.filter, additional);
    }

    query() {
        var q = super.query();

        q = q.populate('user', 'email firstName');
        q = q.populate('driver', 'email firstName');

        return q;
    }

    total(cb) {
        this.model.aggregate([
            {$match: this.filter || {}},
            {
                $group: {
                    '_id': '$status',
                    'count': {
                        '$sum': 1
                    },
                    'amount': {
                        '$sum': {'$add': ['$cost', {$ifNull: ["$tip", 0]}]}
                    }
                }
            }
        ], function (err, result) {
            if (err) {
                return cb(err);
            }

            var completed = result.filter(o=>o._id === OrderStatus.COMPLETED)[0];
            var r = {
                statusTotal: result.reduce((p, o)=> {
                    p[o._id] = {count: o.count};

                    return p;
                }, {}),
                amount: completed ? completed.amount : 0
            };

            cb(null, r);
        });
    }

    entity(e) {
        return {
            id: e._id,
            user: e.user,
            driver: e.driver,
            created: e.created,
            invoiceNumber: e.invoiceNumber,
            customerFeedback: e.customerFeedback,
            driverFeedback: e.driverFeedback,
            status: utils.toProperCase(utils.getEnumKey(OrderStatus, e.status)),
            steps: e.steps.map(step=> {
                return {
                    type: step.type,
                    time: step.completeDate,
                    photos: step.driverImages
                }
            }).filter(step=> {
                return step.time;
            }),
            type: utils.toProperCase(utils.getEnumKey(OrderType, e.type)),
            history: e.history,
            cost: e.cost ? e.cost.toFixed(2) : 'Unknown',

            carCost: e.carCost ? e.carCost.toFixed(2) : '',
            motoCost: e.motoCost ? e.motoCost.toFixed(2) : '',
            pickUpCost: e.pickUpCost ? e.pickUpCost.toFixed(2) : '',
            vehicle: utils.toProperCase(utils.getEnumKey(OrderFilters.TransportType, e.filters.transportType))
        };
    }
}

class UserManager extends OrderManager {
    constructor(params) {
        if (!params.user) {
            throw new Error('User should be authorized');
        }

        super(params);

        this.user = JSON.parse(params.user);
    }

    get filter() {
        var filter = {};
        var prop = this.user.type === 'Driver' ? 'driver' : 'user';
        filter[prop] = this.user.id;


        return _.extend({}, super.filter, filter);
    }
}

class UsersManager extends Manager {
    constructor(params) {
        super(params);
        this.Mapper = {
            email: "email",
            firstName: 'firstName',
            lastName: 'lastName',
            address: 'billingAddress',
            city: 'city',
            contactNumber: 'contactNumber',
            created: 'created',
            blocked: 'blocked'
        };
        this.model = User;
    }

    get filter() {
        var additional = {};

        if (this.params.dateRange) {

            var dateRange = JSON.parse(this.params.dateRange);

            if (dateRange.start) {
                additional.created = additional.created || {};
                additional.created['$gte'] = new Date(dateRange.start);
            }

            if (dateRange.end) {
                additional.created = additional.created || {};
                additional.created['$lte'] = new Date(dateRange.end);
            }

        }

        return _.extend({}, super.filter, additional);
    }
}

class NotificationManager extends Manager {
    constructor(params) {
        if (!params.user) {
            throw new Error('User should be authorized');
        }

        super(params);
        this.user = params.user;
        this.model = Notification;
    }

    get filter() {
        var additional = {
            user: this.user
        };

        if (this.params.offset) {
            additional._id = {'$lt': this.params.offset}
        }

        additional.number = {'$in': [2, 5, 6]}; // hold cancel and complete
        additional.status = {$ne: NotificationStatus.DELETED};

        return _.extend({}, super.filter, additional);
    }

    query() {
        var q = super.query();
        q = q.populate('order', 'invoiceNumber steps status created cost tip driver');
        q = q.deepPopulate('order.driver');

        return q;
    }

    get sorting() {
        return {'date': -1}
    }

    entity(e) {
        return {
            id: e._id,
            date: e.date,
            status: e.status,
            number: e.number,
            orderId: e.order._id,
            cost: e.order.calculateCost(),
            invoiceNumber: e.order.invoiceNumber,
            orderDate: e.order.created,
            orderStatus: e.order.status,
            steps: e.order.steps.map(step=> {
                return {
                    type: step.type,
                    address: step.address,
                    time: step.completeDate,
                    photos: step.driverImages
                }
            }),
            driverPhoto:e.order.driver.car.images.driverPhoto
        };
    }
}

module.exports = {
    Manager,
    OrderManager,
    UserManager,
    UsersManager,
    NotificationManager
};
