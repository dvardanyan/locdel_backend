var GoogleMapsAPI = require('googlemaps');
var errors = require('../public/errors.js');
var configs = require('../config/config');

var publicConfig = {
    key: configs.credentials.direction.key,
    secure: true
};

var gmAPI = new GoogleMapsAPI(publicConfig);

function getDistance(params, cb) {
    var isArray = Array.isArray(params);
    cb = cb || function () {};

    params = {
        origin: isArray ? params[0] : params.startPoint,
        destination: isArray ? params[params.length - 1] : params.endPoint,
        waypoints: (isArray ? params.slice(1, params.length - 1) : params.waypoints).join('|')
    };
    gmAPI.directions(params, function (err, result) {
        if (err || result.status !== 'OK') {
            var error = errors.get(212, [err || result.status]);

            return cb(error, null);
        }

        var distance = result.routes[0].legs.reduce((prev, leg)=>leg.distance.value + prev, 0);
        var eta = result.routes[0].legs.reduce((prev, leg)=>leg.duration.value + prev, 0);

        cb(null, distance, eta);
    });
}

module.exports = {
    distance: getDistance
};