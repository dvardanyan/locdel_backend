var FCM = require('fcm-node');
var configs = require('../config/config');
var fcm = new FCM(configs.credentials.fcm.key);

module.exports = {
    create: function (token, message, payload,action) {
        // create new message
        var m = {
            to: token,

            data: {
                title: message,
                message: message,
                payload: payload,
                action:action
            },
            priority: 'high',
            content_available: true,
            time_to_live: 5 * 3600

        };

        fcm.send(m, function (err) {
            if (err) {
                console.error(err);
            }
        });
    }
};