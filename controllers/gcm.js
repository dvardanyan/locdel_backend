var gcm = require('node-gcm');
var configs = require('../config/config');
var sender = new gcm.Sender(configs.credentials.gcm.key);

module.exports = {
    create: function (token, message, payload) {
        // create new message
        var note = new gcm.Message({
            priority: 'high',
            restrictedPackageName: "com.locdel",
            timeToLive: 5 * 3600,
            data: {
                title: message,
                message: message,
                payload: payload
            },
            notification: {
                title: message,
                message: message,
                payload: payload
            }
        });

        // send the message
        sender.send(note, {registrationTokens: [token]}, function (err) {
            if (err) {
                console.error(err);
            }
        });
    }
};