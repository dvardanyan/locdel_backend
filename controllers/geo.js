var distance = require('google-distance-matrix');
var configs = require('../config/config');

distance.key(configs.credentials.distance.key);
distance.units('imperial');

function filterOrders(origin, orders, cb) {
    // in mongodb spherical coordinates storing by long, lat order
    var origins = [origin.slice().reverse().join(',')];

    var destinations = orders.slice(0, 25).map(function (order) {
        return order.location.slice().reverse().join(',');
    });

    var result = [];

    if (!origins.length || !destinations.length) {
        return cb(false, []);
    }

    distance.matrix(origins, destinations, function (err, distances) {
        if (err || distances.status !== 'OK') {
            return cb(error || true);
        }

        for (var i = 0; i < destinations.length; i++) {
            if (distances.rows[0].elements[i].status == 'OK') {
                var distance = distances.rows[0].elements[i].distance;

                // trim whitespaces
                distance.text = distance.text.replace(/\s/g, '');

                result.push({
                    startDistance: distance,
                    order: orders[i]
                });
            }
        }

        result.sort(function (o1, o2) {
            return o1.startDistance.value - o2.startDistance.value;
        });

        cb(false, result);

    });
}

module.exports = {
    filterOrders: filterOrders
};