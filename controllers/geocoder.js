var NodeGeocoder = require('node-geocoder');
var configs = require('../config/config');

var options = {
    provider: 'google',
    apiKey: configs.credentials.geocoder.key,
    formatter: null
};

var geocoder = NodeGeocoder(options);

function getAddress(point, cb) {
    geocoder.reverse({lat: point.lat, lon: point.long}, function (err, result) {
        if (err) {
            return cb(err, null)
        }

        cb(null, result[0].formattedAddress);
    })
}

function getPoint(address, cb) {
    if (!address) {
        return cb(true);
    }

    geocoder.geocode(address, function (err, result) {
        if (err) {
            return cb(err, null)
        }

        var first = result[0];

        cb(null, {
            lat: first.latitude,
            long: first.longitude
        })
    })
}

module.exports = {
    address: getAddress,
    point: getPoint
};
