var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var configs = require('../config/config');
var support = configs.email.support;

var create = function () {
    return nodemailer.createTransport(smtpTransport({
        service: 'gmail',
        auth: {
            user: support.username,
            pass: support.password
        }
    }));
};

module.exports = {
    create: create
};
