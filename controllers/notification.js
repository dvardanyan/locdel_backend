var User = require('../models/user/user.js'),
    UserType = require('../models/user/user-type'),
    format = require('string-format'),
    apn = require('../controllers/apn.js'),
    fcm = require('../controllers/fcm.js'),
    mail = require('../controllers/mailer'),
    site = require('../config/site'),
    Notification = require('../models/order/notification.js'),
    UserDeviceType = require('../models/user/user-device.js');

const notifications = {
    1: {
        message: function (payload) {
            return `Inv. # ${payload.invoiceNumber} \n ${payload.user.firstName} ${payload.user.lastName} has accept the job.`
        },
        action: 'order-pick',
        params: ['order', 'user', 'invoiceNumber']
    },
    2: {
        message: function (payload) {
            return `Inv. # ${payload.invoiceNumber} \n ${payload.user.firstName} ${payload.user.lastName} has cancel the order.`
        },
        action: 'order-cancel',
        params: ['order', 'user', 'invoiceNumber', 'reason']
    },
    3: {
        message: 'Order Failed',
        action: 'order-fail',
        params: ['order', 'user', 'invoiceNumber']
    },
    4: {
        message: 'Order Completed',
        action: 'order-complete',
        params: ['order', 'user', 'invoiceNumber', 'cost']
    },
    5: {
        message: function (payload) {
            return `Inv. # ${payload.invoiceNumber} \n is on hold.`
        },
        action: 'order-hold',
        params: ['order', "invoiceNumber" ]
    },
    6: {
        message: function (payload) {
            return `Inv. # ${payload.invoiceNumber} \n ${payload.user.firstName}  ${payload.user.lastName} has complete the order.`
        },
        action: 'order-complete-with-feedback',
        params: ['order', 'user', 'invoiceNumber', 'cost']
    },
    7: {
        message: 'Driver crashed, please wait until other driver will pick it',
        action: 'order-crash',
        params: ['order', 'user', 'invoiceNumber']
    },
    8: {
        message: 'Your scheduled order was not created. Please update card info',
        action: 'schedule-pay-error',
        params: ['order', 'error', 'invoiceNumber'],
        needLog: ['error']
    },
    9: {
        message: function (payload) {
            return `Inv. # ${payload.invoiceNumber} \n ${payload.user.firstName}  ${payload.user.lastName} has picked up the item.`
        },
        action: 'order-step-complete',
        params: ['order', 'user', 'invoiceNumber', 'photos'],
        pushOnly: true
    }
};

function getTarget(user, callback) {
    function cb(userModel) {
        var device = getUserDevice(userModel);
        callback(device);
    }

    if (typeof user === 'string') {
        User.findOne({
            _id: user
        }, 'devices', function (err, user) {
            if (!err && user) {
                cb(user);
            }
        })
    } else {
        cb(user);
    }

}

function getUserDevice(user) {
    var devices = user.devices, device;

    for (var i = 0; i < devices.length; i++) {
        if (devices[i].active) {
            device = devices[i];
            break;
        }
    }

    return device;
}

format.extend(String.prototype);

function sendMail(user, notification, payload, cb) {
    const mailer = mail.create();
    const cost = payload.cost;
    const costMessage = cost ? ('Order Cost:' + cost) : '';
    const message = typeof notification.message === 'string' ? notification.message : notification.message(payload);
    const text = `${message}\n${costMessage}\n`;

    var mailOptions = {
        to: user.email,
        from: site.email,
        subject: 'Order Status Change',
        text: text,
        html: text.replace(/\n/g, '<br/>'),
    };

    mailer.sendMail(mailOptions, err=> {
        cb(err);
    });
}

function createNotificationLog(user, order, notificationNumber, payload, cb) {
    var n = new Notification();
    n.user = user._id || user;
    n.order = order._id || order;
    n.number = notificationNumber;
    n.payload = getLoggingData(payload);

    n.save(function (err) {
        if (err) {
            console.error(JSON.stringify(err));
        }
        if(cb) {
            cb(err);
        }
    });
}

function getLoggingData(payload) {
    const number = payload.number;
    const n = notifications[number];
    const loggingProperties = n.needLog || [];

    return loggingProperties.map(p=> {
        return {[p]: payload[p]};
    });
}

module.exports = {
    send: function (user, number, params, cb) {
        getTarget(user, function (device) {
            const n = notifications[number],
                payload = {},
                action = n.action;

            if (!n) {
                return cb && cb({
                        message: 'Wrong notification number'
                    });
            }

            var reqParams = n.params;

            reqParams.forEach(function (rp) {
                if (!params[rp]) {
                    return cb && cb({
                            message: 'Required param not found: {}'.format(params[rp])
                        })
                }

                payload[rp] = params[rp];
            });

            payload.number = number;

            if (!n.pushOnly) {
                // send mail anyway
                sendMail(user, n, payload, ()=> {
                });

                createNotificationLog(user, payload.order, number, payload, cb);
            }

            // not send any push notification if target was not found
            if (!device) {
                return;
            }

            const deviceType = device.type || UserDeviceType.IOS;

            const message = typeof n.message === 'string' ? n.message : n.message(payload);

            switch (deviceType) {
                case UserDeviceType.IOS:
                    apn.create(device.token, message, payload, action, user.type);
                    break;
                case UserDeviceType.ANDROID:
                    fcm.create(device.token, message, payload, action);
                    break;
                default:
                    break;
            }
        });
    }
};