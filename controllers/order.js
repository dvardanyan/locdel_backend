var Order = require('../models/order/order.js'),
    Step = require('../models/order/step.js'),
    Payment = require('../models/payment/payment.js'),
    Tracking = require('../models/order/tracking.js'),
    PaymentStatus = require('../models/payment/payment-status.js'),
    OrderStatus = require('../models/order/order-status.js'),
    OrderFilters = require('../models/order/order-filters.js'),
    OrderType = require('../models/order/order-type.js'),
    geo = require('../controllers/geo.js'),
    geocoder = require('../controllers/geocoder.js'),
    direction = require('../controllers/direction.js'),
    geoUtils = require('../util/geo.js'),
    errors = require('../public/errors.js'),
    async = require('async'),
    paypal = require('../controllers/paypal.js'),
    stripe = require('../controllers/stripe.js'),
    TransportType = require('../models/user/user-transport-type.js'),
    sh = require("shorthash");
    _ = require('underscore');


function createOrder(user, source, location, cb, doPayment) {
    var order = new Order();

    //set order creator id
    order.user = user._id;
 
    ['filters', 'steps', 'type', 'distance', 'schedule', 'status', 'cost', 'tip', 'parent', 'accidentDriverNumber', 'history', 'encodedRoute'].forEach(function (prop) {
        if (source[prop]) {
            order[prop] = source[prop];
        }
    });

    if (!location) {
        return cb(errors.get(168));
    }

    order.location = location;

    async.waterfall([
        function (next) {
            addCostData(order, next);
        },
        function (next) {
            incrementInvoiceNumber(user, next);
        },
        function (invoiceNumber, next) {

            order.invoiceNumber = sh.unique(user._id+'') + '-' + invoiceNumber;
            pushHistory(order);
            order.save(err=> {
                if (err) {
                    var error = errors.get(115, err.errors);
                    return next(error);
                }

                next(null);
            });
        },
        function (next) {
            saveSteps(user._id, order.steps, location, next);
        }
    ], function (err) {
        if (err) {
            return cb(err);
        }

        savePayments(order, user, cb, doPayment);
    });
}
function payPayments (user,configs, type, orderId, cb){
    var configs = configs || {},
        type = type;
    if (!orderId) {
        return cb(errors.get(136));
    }
    var pay = function (sdk) {
        sdk.pay(user, orderId, configs || {}, function (err) {
            if (err) {
                return failOrder(orderId, function () {
                    cb(errors.get(137, [err]));
                })
            }
            var history = {
                status: OrderStatus.PENDING,
                date: Date.now()
            };

            Order.update({_id: orderId}, {
                $set: {
                    status: OrderStatus.PENDING
                },
                $push: {history: history}
            }, function (err) {
                if (err) {
                    cb(errors.get(161));
                }
                cb(errors.noError('Thank You! Your order has been submitted successfully. We will contact you shortly. You can manage your orders in Pendings tab.', {
                    orderId: orderId
                }));
            });

       });
    };
    switch (type) {
        case 'paypal':
            return pay(paypal);
        case 'stripe':
            return pay(stripe);
        default:
            return cb(errors.get(134));
    }

}
function savePayments(order, user, cb, doPayment) {
    var payment = new Payment({
        order: order._id,
        status: PaymentStatus.PENDING
    });

    payment.save(function (err) {
        if (err) {
            var error = errors.get(115, err.errors);

            return order.remove(function () {
                return cb(error);
            });
        }
        if (doPayment) {
            payPayments(user, '', 'stripe', order._id, cb);
        } else  {
            cb(errors.noError('Thank You! Your order has been submitted successfully. We will contact you shortly. You can manage your orders in Pendings tab.', {
                orderId: order._id
            }));
        }


    });
}

function saveSteps(user, steps, location, callback) {
    var count = steps.length;

    steps.forEach(function (step) {
        var query = {
            address: step.address,
            long: location[0],
            lat: location[1],
            name: step.name,
            contactNumber: step.contactNumber,
            user: user,
            apartmentNo: step.apartmentNo
        };

        var update = _.extend({}, query, {disabled: false});

        // making this inserts more tolerance for errors
        Step.findOneAndUpdate(query, update, {upsert: true}, function () {
            if (--count === 0) {
                return callback();
            }
        })
    })
}

function parseOrder(order) {
    order.id = order._id;

    var driver = order.driver;
    if (driver && driver.car && driver.car.images) {
        order.driver = {
            id: driver._id,
            firstName: driver.firstName,
            lastName: driver.lastName,
            contactNumber: driver.contactNumber,
            feedback: driver.feedback.average,
         /*   profileImage: driver.car.images.driverPhoto,
            car:{
                type: driver.type,
                model: driver.car.model
            }*/
            //profileImage: driver.profileImage,
            car:{
                type: driver.type,
                model: driver.car.model
            },

            profileImage: driver.car.images.driverPhoto
        }

    }

    if (_.isObject(order.user)) {
        var u = order.user;

        order.user = {
            id: u._id,
            firstName: u.firstName || "",
            lastName: u.lastName || "",
            contactNumber: u.contactNumber || ""
        }
    }

    return order;
}

function failOrder(orderId, cb) {
    Order.findById(orderId, function (err, order) {
        if (!err && order) {
            order.status = OrderStatus.FAIL;
            order.save(function (error) {
                cb(error);
            })
        } else {
            cb(true);
        }
    })
}

function pushHistory(order, data) {
    order.history.push({
        status: order.status,
        date: Date.now(),
        data: data
    })
}

function incrementInvoiceNumber(user, cb) {
    user.invoiceNumber = ((user.invoiceNumber || 0) + 1);

    user.save(function (err) {
        if (err) {
            return cb(errors.get(115, [err]));
        }

        cb(null, user.invoiceNumber);
    })
}

function getPendingFilter(user) {
    var carType = user.car && user.car.type;

    return {
        'filters.transportType': {$bitsAllSet: carType},
        status: OrderStatus.PENDING
    }
}

// dirty filtering by spherical locations
function getNearestOrders(point, options, cb) {
    options = options || {};

    Order.aggregate([{
        $geoNear: {
            near: {type: "Point", coordinates: point},
            distanceField: 'startDistance',
            query: options.filter,
            spherical: true,
            num: options.number || 10
        }
    }], function (err, orders) {
        if (err) {
            return cb(err);
        }

        // filter by google api
        geo.filterOrders(point, orders, cb)

    })
}

function getPendingJobs(user, cb) {
    Order
        .find({
            driver: user._id,
            status: OrderStatus.PICKED
        })
        .populate('driver user')
        .exec(function (err, orders) {
            if (err) {
                return cb(errors.get(120, [err]));
            }

            var result = errors.noError();

            result.count = orders.length;

            result.orders = orders.map(function (order) {
                return parseOrder(order.toObject());
            });

            cb(null, result);
        });
}


function crash(order, cb) {
    var steps = order.steps.filter(step=>!step.driverImages.length);

    // if zero steps complete, just make order pending again
    if (steps.length === order.steps.length) {
        order.status = OrderStatus.PENDING;
        order.driver = undefined;

        return order.save(function (err) {
            if (err) {
                return cb(errors.get(197, [err]));
            }

            cb(false);
        });
    }

    function makeNewOrder(order, steps, location) {
        var copy = order.toObject();
        copy.steps = steps;
        copy.status = OrderStatus.PENDING;
        copy.parent = order._id;
        copy.accidentDriverNumber = order.driver.contactNumber;
        copy.history = [{
            status: OrderStatus.CANCELED,
            date: Date.now(),
            data: {
                message: `Order was previously canceled, because of vehicle crash.
                 Driver ${order.driver.firstName} ${order.driver.lastName}, Contact Number: ${order.driver.contactNumber}`
            }
        }];

        addCostData(copy, function (err) {
            if (err) {
                return cb(err);
            }

            createOrder(order.user, copy, location, err => {
                if (err.status != 'OK') {
                    return cb(err);
                }

                order.status = OrderStatus.CANCELED;
                order.save(function (error) {
                    cb(error);
                });
            }, true);
        });
    }

    Tracking.findOne({
        order: order._id
    }, function (err, tracking) {
        if (err || !tracking) {
            return cb(errors.get(196));
        }

        geocoder.address({
            lat: tracking.latitude,
            long: tracking.longitude
        }, function (err, address) {
            if (err) {
                return cb(errors.get(196));
            }

            var step = {
                address: address
            };

            steps.unshift(step);
            steps.forEach(step=>_.omit(step, '_id'));

            makeNewOrder(order, steps, [tracking.longitude, tracking.latitude]);
        })
    });
}

function getCost(miles, eta, transportType, stepsPoints) {

    if (miles === 0) {
        return 0;
    }

    eta = eta || 0;

    const minimalMoto = 5;
    const sitChargeMoto = 2.22;
    const chargePerMileMoto = 0.85;
    const chargePerMinuteMoto = 0.18;
    const extraMoto = 2;

    const minimalCar = 5;
    const sitChargeCar = 1.9;
    const chargePerMileCar = 0.95;
    const chargePerMinuteCar = 0.2;
    const extraCar = 2;

    const minimalPickup = 8;
    const sitChargePickup = 2.70;
    const chargePerMilePickup = 1.45;
    const chargePerMinutePickup = 0.4;
    const extraPickup = 5;

    if (transportType === TransportType.Motorcycle)
    {
        var cost = Math.max(Math.round((sitChargeMoto + miles * chargePerMileMoto + eta * chargePerMinuteMoto) * 10) / 10, minimalMoto);
        if(stepsPoints.length > 2)
        {
            cost +=  (stepsPoints.length - 2) * extraMoto;
        }
        return cost;
    }

    if (transportType === TransportType.Car)
    {
        var cost = Math.max(Math.round((sitChargeCar + miles * chargePerMileCar + eta * chargePerMinuteCar) * 10) / 10, minimalCar);
        if(stepsPoints.length > 2)
        {
            cost +=  (stepsPoints.length - 2) * extraCar;
        }
        return cost;
    }

    if (transportType === TransportType.PickUp)
    {
        var cost = Math.max(Math.round((sitChargePickup + miles * chargePerMilePickup + eta * chargePerMinutePickup) * 10) / 10, minimalPickup);
        if(stepsPoints.length > 2)
        {
            cost +=  (stepsPoints.length - 2) * extraPickup;
        }
        return cost;
    }

}

function addCostData(order, cb) {
    direction.distance(order.steps.map(step=>step.address), function (err, distance, eta) {
        if (err) {
            return cb(err);
        }

        order.distance = geoUtils.metersToMiles(distance);
        order.eta = eta / 60;


        if (order.filters.transportType >= 4 ) {
            order.pickUpCost = getCost(order.distance, order.eta, TransportType.PickUp, order.steps);
        }

        if (order.filters.transportType !== 2 && order.filters.transportType !== 4 && order.filters.transportType !== 6) {
            order.carCost = getCost(order.distance, order.eta, TransportType.Car, order.steps);
        }

        if (order.filters.transportType !== 1 && order.filters.transportType !== 4 && order.filters.transportType !== 5) {
            order.motoCost = getCost(order.distance, order.eta, TransportType.Motorcycle, order.steps);
        }


        cb(null);
    })
}

function getStatusDate(order, status) {
    history = order.history;
    for (var i = 0; i < history.length; i++) {
        if (status === history[i].status) {
            return history[i].date;
        }
    }

    return null;
}

module.exports = {
    create: createOrder,
    pendingFilters: getPendingFilter,
    parse: parseOrder,
    pushHistory: pushHistory,
    nearestOrders: getNearestOrders,
    pendingJobs: getPendingJobs,
    cost: getCost,
    fail: failOrder,
    crash: crash,
    statusDate: getStatusDate,
    addCostData: addCostData,
    payPayments: payPayments
};
