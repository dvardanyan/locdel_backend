var errors = require('../public/errors.js'),
    paypal = require('../controllers/paypal.js'),
    stripe = require('../controllers/stripe.js'),
    OrderStatus = require('../models/order/order-status.js'),
    OrderController = require('../controllers/order'),
    OrderCancelReason = require('../models/order/order-cancel-reason.js'),
    Payment = require('../models/payment/payment.js');


function capturePayment(user, order, callback) {
    var orderId = order._id;

    Payment.findOne({
        order: orderId
    }, function (err, payment) {
        var cb = function (err) {
            if (err) {
                return callback(errors.get(137, [err]));
            }

            callback(null);
        };

        if (err) {
            return cb({
                message: 'Backend error occurred'
            })
        }

        if (!payment) {
            return cb({
                message: 'Payment not found'
            })
        }

        var id = payment.authId;

        if (!id && payment.method !== 'stripe') {
            return cb(errors.get(138));
        }

        var configs = {
            cost: order.calculateCost()
        };

        var capture = function (sdk) {
            sdk.capture(user, payment, configs, cb);
        };


        switch (payment.method) {
            case 'paypal':
                return capture(paypal);
            case 'stripe':
                return capture(stripe);
            default:
                return callback(errors.get(134));
        }
    });

}

function cancelPayment(user, order, reason, callback) {
    var orderId = order._id;

    Payment.findOne({
        order: orderId
    }, function (err, payment) {
        var cb = function (err) {
            if (err) {
                return callback(errors.get(162, [err]));
            }

            callback(false);
        };

        if (err) {
            return cb({
                message: 'Backend error occurred'
            })
        }

        if (!payment) {
            return cb({
                message: 'Payment not found'
            })
        }

        var id = payment.authId;

        if (!id && payment.method !== 'stripe') {
            return cb(errors.get(138));
        }

        var os = order.status;

        var refund = function (sdk) {
            function capturePenal() {
                // must pay 5 dollar for canceling
                var captureConfigs = {
                    cost: 5
                };

                sdk.capture(order.user, payment, captureConfigs, cb);
            }

            var fromCustomer = order.user._id.equals(user._id);
            var pickedOrHold = (os === OrderStatus.PICKED || os === OrderStatus.HOLD);

            // Cancel cases when it fired from customer
            if (fromCustomer) {
                // if still pending just cancel payment and refund
                if (os === OrderStatus.PENDING) {
                    return sdk.refund(order.user, payment, cb);
                }

                // if picket just 30 seconds ago just cancel and refund
                var n = Date.now();
                var pickedDate = OrderController.statusDate(order, OrderStatus.PICKED);
                var secondDiff = (n - pickedDate) / 1000;
                if (pickedOrHold && (secondDiff < 30)) {
                    return sdk.refund(order.user, payment, cb);
                }

                // if previous cases passed means order in old picked state or on hold state, so capture penal
                return capturePenal();
            }

            // Driver cases
            switch (reason) {
                case OrderCancelReason.WRONG_VEHICLE:
                    return capturePenal();
                case OrderCancelReason.CUSTOMER_UNAVAILABLE:
                    return capturePenal();
                case OrderCancelReason.CUSTOMER_CANCELED_ORDER:
                    return capturePenal();
                case OrderCancelReason.OTHER:
                    return capturePenal();
                default:
                    return cb({
                        message: 'Order can not be canceled whit this status code : ' + reason
                    })
            }
        };


        switch (payment.method) {
            case 'paypal':
                return refund(paypal);
            case 'stripe':
                return refund(stripe);
            default:
                return res.send(errors.get(134));
        }
    });
}

module.exports = {
    capture: capturePayment,
    cancel: cancelPayment
};
