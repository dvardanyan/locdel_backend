"use strict";
var paypal = require('paypal-rest-sdk'),
    PaymentStatus = require('../models/payment/payment-status.js'),
    errors = require('../public/errors.js'),
    Payment = require('../models/payment/payment.js'),
    configs = require('../config/config');

paypal.configure({
    mode: 'sandbox',
    client_id: configs.credentials.paypal.id,
    client_secret: configs.credentials.paypal.secret
});

var controller = {
    generateToken: function (code, user, cb) {
        var authCode = {'authorization_code': code};
        paypal.generateToken(authCode, function (error, rt) {
            if (error) {
                cb(error)
            } else {
                user.paymentRefreshToken = rt;

                user.save(function (err) {
                    if (err) {
                        cb({message: 'Can\'t save refresh token'});
                    } else {
                        cb(false);
                    }
                });
            }
        });
    },
    pay: function (user, orderId, configs, cb) {
        if (!user.paymentRefreshToken) {
            return cb({
                message: 'User has not refresh token'
            })
        }

        var authConfig = {'correlation_id': configs.token, 'refresh_token': user.paymentRefreshToken};

        //Set up payment details
        var paymentConfig = {
            "intent": "authorize",
            "payer": {
                "payment_method": "paypal"
            },
            "transactions": [{
                "amount": {
                    "currency": "USD",
                    "total": configs.cost
                },
                "description": "Pay from heaven"
            }]
        };

        //Create future payment
        paypal.payment.create(paymentConfig, authConfig, function (error, paymentResponse) {
            if (error) {
                cb(error);
            } else {
                Payment.findOne({
                    order: orderId
                }, function (err, payment) {
                    if (err) {
                        return cb({
                            message: 'Payment authorized but backend error occurred'
                        })
                    }

                    if (!payment) {
                        return cb({
                            message: 'Payment authorized but order not found'
                        })
                    }

                    payment.authResponse = paymentResponse;
                    payment.authPaymentConfig = paymentConfig;
                    payment.status = PaymentStatus.AUTHORIZED;
                    payment.method = 'paypal';

                    var transactions = payment.authResponse.transactions,
                        id = (((((transactions || [])[0] || {}).related_resources || [])[0] || {}).authorization || {}).id;


                    if (!id) {
                        return cb(errors.get(138));
                    }

                    payment.authId = id;

                    payment.save(function (err) {
                        if (err) {
                            return cb({
                                message: 'Payment authorized but backend error occurred'
                            });
                        }

                        cb(false);
                    });
                })
            }
        });
    },
    capture: function (user, payment, configs, cb) {
        var capturePayment = {
            "amount": {
                "currency": "USD",
                "total": configs.cost
            },
            "is_final_capture": true
        };

        var authConfig = {'refresh_token': user.paymentRefreshToken};

        paypal.authorization.capture(payment.authId, capturePayment, authConfig, function (error, captureResponse) {
            if (error) {
                cb(error);
            } else {
                payment.captureResponse = captureResponse;
                payment.capturePaymentConfig = capturePayment;
                payment.amountCaptured = Number(captureResponse.amount.total) || 0;
                payment.status = PaymentStatus.COMPLETED;

                payment.save(function (err) {
                    if (err) {
                        return cb({
                            message: 'Payment captured but backend error occurred'
                        });
                    }

                    cb(false);
                });
            }
        })

    },

    refund: function (user, payment, cb) {
        var authConfig = {'refresh_token': user.paymentRefreshToken};

        paypal.authorization.void(payment.authId, authConfig, function (error, refundResponse) {
            if (error) {
                cb(error);
            } else {
                payment.status = PaymentStatus.REFUNDED;
                payment.refundResponse = refundResponse;

                payment.save(function (err) {
                    if (err) {
                        return cb({
                            message: 'Payment refunded but backend error occurred'
                        });
                    }

                    cb(false);
                });
            }
        });
    }
};

module.exports = controller;