var Schedule = require('../models/order/schedule.js'),
    OrderController = require('../controllers/order.js'),
    OrderStatus = require('../models/order/order-status.js'),
    Order = require('../models/order/order.js'),
    errors = require('../public/errors.js'),
    moment = require('moment'),
    notification = require('../controllers/notification.js'),
    stripe = require('../controllers/stripe.js'),
    schedule = require('node-schedule');

// save active jobs, but with schedule id.
var actives = [];

function cancelAll() {
    actives.forEach(function (a) {
        a.job.cancel();
    });

    actives = [];
}

var pay = function (user, orderId, cb) {
    stripe.pay(user, orderId, {}, function (err) {
        if (err) {
            notification.send(user, 8, {
                order: orderId,
                error: err
            });

            return OrderController.fail(orderId, function () {
                cb(errors.get(137, [err]));
            })
        }

        var history = {
            status: OrderStatus.PENDING,
            date: Date.now()
        };


        Order.update({_id: orderId}, {
            $set: {
                status: OrderStatus.PENDING
            },
            $push: {history: history}
        }, function (err, numEffected) {
            if (err) {
                return cb(errors.get(161));
            }

            cb(false);
        });
    })
};

function momentUser(offset) {
    var initial = moment.utc();

    if (offset) {
        initial.utcOffset(offset);
    }

    return initial;
}

function finished(job, rule) {
    var next = job.nextInvocation();

    if (!next) {
        return true;
    }

    // should check if last job was fired
    if (rule.dayOfWeek && rule.once) {
        var remaining = rule.results.filter(o=> {
            // some little fault can be accured during next calculation
            return o.date.toDate() > +next - 1000;
        });

        return !remaining.length;
    }

    return false;
}

var empty = arr=> Array.isArray(arr) && arr.length === 0;

function getCorrectRule(rule) {
    var mDate = {
        year: rule.year,
        month: rule.month,
        date: rule.date,
        hour: rule.hour,
        minute: rule.minute || 0,
        second: rule.second || 0
    };

    var newRule = {};

    if (empty(rule.dayOfWeek)) {
        rule.dayOfWeek = undefined;
    }

    // if once rule exists but dayOfWeek is
    if (!rule.dayOfWeek && rule.once) {
        var dd = momentUser(rule.offset).set(mDate).local();

        rule.dayOfWeek = [(dd < moment() ? dd.add(1, 'days') : dd).get('day')];
    }

    if (rule.dayOfWeek) {
        var weekDays = Array.isArray(rule.dayOfWeek) ? rule.dayOfWeek : [rule.dayOfWeek];

        // if weekly repeat is disabled need to save weekly day: full date pairs
        if (rule.once) {
            rule.results = []
        }

        var normalWeekDays = weekDays.map(function (d) {
            var serverWeekDate = momentUser(rule.offset).set(mDate).set('day', d).local();

            if (rule.once) {
                rule.results.push({
                    day: d,
                    date: serverWeekDate < moment() ? serverWeekDate.add(7, 'days') : serverWeekDate
                });
            }
            return serverWeekDate.get('day');
        });

        newRule.dayOfWeek = normalWeekDays;
    }

    var date = momentUser(rule.offset).set(mDate).local();

    newRule.year = rule.year && date.get('year');
    newRule.month = rule.month && date.get('month');
    newRule.date = rule.date && date.get('date');
    newRule.hour = date.get('hour');
    newRule.minute = date.get('minute');
    newRule.second = date.get('second');

    return newRule;
}

function addJob(date, orderId, cb) {
    var rule = getCorrectRule(date);

    cb = cb || function () {
        };

    var job = schedule.scheduleJob(rule, function () {
        Order
            .findById(orderId)
            .populate('user')
            .exec(function (err, order) {
                if (err) {
                    return cb(err);
                }

                // get schedule id
                var j = actives.filter(function (a) {
                    return a.job === job;
                })[0];

                order.schedule = j.id;

                // if order will be created from inactive order
                order.status = OrderStatus.INITIAL;

                OrderController.create(order.user, order, order.location, function (res) {
                    // log errors
                    if (res.status !== 'OK') {
                        console.log(res);
                    }
                    if (err) {
                        console.log(err);
                    }
                    if (finished(job, date)) {
                        disable(j.id, () => {
                        })
                    }
                    /*pay(order.user, res.data.orderId, function (err) {
                        if (err) {
                            console.log(err);
                        }


                    });*/
                },true);
            })
    });

    return job;
}

function add(date, orderId, userId, cb) {
    var j = addJob(date, orderId);

    // invalid job, caused from invalid schedule
    if (!j) {
        return cb({
            message: 'Wrong schedule'
        })
    }

    var schedule = new Schedule();

    schedule.date = date;
    schedule.order = orderId;
    schedule.user = userId;

    schedule.save(function (err) {
        // db error
        if (err) {
            j.cancel();
            return cb(err);
        }

        // make object with schedule id identifier
        actives.push({
            id: schedule._id,
            job: j
        });

        // success
        cb(null, schedule._id);
    });
}

function update(date, scheduleId, cb) {
    Schedule.findOne({
        _id: scheduleId
    }, function (err, schedule) {
        if (err || !schedule) {
            return cb(true);
        }

        schedule.date = date;
        var j = addJob(date, schedule.order);

        // invalid job, caused from invalid schedule
        if (!j) {
            return cb({
                message: 'Wrong schedule'
            })
        }


        schedule.save(function (err) {
            // db error
            if (err) {
                j.cancel();
                return cb(err);
            }

            // make object with schedule id identifier
            actives.push({
                id: schedule._id,
                job: j
            });

            // success
            cb(null);
        });
    })

}

function reload(cb) {
    // cancel all actives
    cancelAll();

    Schedule.find({
        disabled: {$ne: true}
    }, function (err, schedules) {
        if (err) {
            return cb(err);
        }


        schedules.forEach(function (schedule) {
            var j = addJob(schedule.date, schedule.order);

            if (j) {
                actives.push({
                    id: schedule._id,
                    job: j
                });
            }
        });

        cb(null, actives);
    });
}

function cancel(scheduleId) {
    var j = actives.filter(function (a) {
        return a.id.equals(scheduleId);
    })[0];

    if (j) {
        j.job.cancel();

        //delete from actives
        var index = actives.indexOf(j);

        if (index > -1) {
            actives.splice(index, 1);
        }
    }
}

function disable(scheduleId, cb) {
    cancel(scheduleId);

    Schedule.update({_id: scheduleId}, {$set: {disabled: true}}, function (err) {
        if (err) {
            return cb(errors.get(184, [err]));
        }

        cb(errors.noError());
    });
}

function enable(scheduleId, cb) {
    // job can be active for some unexpected reasons.
    cancel(scheduleId);

    Schedule.findById(scheduleId, function (err, schedule) {
        if (err) {
            return cb(errors.get(185, [err]));
        }

        addJob(schedule.date, schedule.order);

        schedule.disabled = false;
        schedule.save(function (err) {
            if (err) {
                cancel(scheduleId);

                return cb(errors.get(185, [err]));
            }

            cb(errors.noError());
        });
    });

}

function reschedule(scheduleId, spec, cb) {
    cancel(scheduleId);
    update(spec, scheduleId, cb);
}

module.exports = {
    add: add,
    reload: reload,
    disable: disable,
    enable: enable,
    reschedule: reschedule
};