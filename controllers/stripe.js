"use strict";
const PaymentStatus = require('../models/payment/payment-status.js'),
    configs = require('../config/config'),
    stripeTestToken = configs.credentials.stripe.test_key,
    Payment = require('../models/payment/payment.js'),
    stripe = require("stripe")(stripeTestToken);

const controller = {
    pay: function (user, orderId, configs, cb) {
        function savePayment() {
            Payment.findOne({
                order: orderId
            }, function (err, payment) {
                if (err) {
                    return cb({
                        message: 'Payment authorized but backend error occurred'
                    })
                }

                if (!payment) {
                    return cb({
                        message: 'Payment authorized but order not found'
                    })
                }

                payment.status = PaymentStatus.AUTHORIZED;
                payment.method = 'stripe';

                payment.save(function (err) {
                    if (err) {
                        return cb({
                            message: 'Payment authorized but backend error occurred'
                        });
                    }

                    cb(false);
                });
            })
        }

        if (controller.check(user)) {
            return savePayment();
        }

        return cb({
            message: "User does'nt have stripe card"
        });
    },

    customer: function (user, token, cardInfo, cb) {
        if(!cardInfo){
            return cb({
                message: 'Card info not found'
            })
        }

        const infoFields = ['lastDigits', 'expYear', 'expMonth'];
        for (let field of infoFields) {
            if (!Number.isInteger(cardInfo[field])) {
                return cb({
                    message: `Wrong card info, field ${field}`
                })
            }
        }

        function stripeCb(error, customer) {
            if (error) {
                return cb(error);
            }

            user.stripeCustomerId = customer.id;
            user.cardInfo = cardInfo;

            user.save(function (err) {
                if (err) {
                    return cb({
                        message: 'Backend error raised'
                    })
                }

                return cb(false);
            })
        }

        controller.generateCustomer(token, stripeCb, user.email);
    },
    generateCustomer: function (token, cb, name) {
        name = name || 'Unknown';

        if (!token) {
            return cb({
                message: "Token not found"
            })
        }

        stripe.customers.create({
            description: 'Customer for ' + name,
            source: token
        }, cb);
    },
    capture: function (user, payment, configs, cb) {
        var cost = parseInt(+configs.cost * 100) || payment.paymentConfig.cost;

        if (!user.stripeCustomerId) {
            return cb({
                message: 'User card not found'
            })
        }

        var captureConfig = {
            amount: cost,
            currency: "usd",
            description: "Charged from Locdel",
            customer: user.stripeCustomerId
        };

        stripe.charges.create(captureConfig, function (error, captureResponse) {
            if (error) {
                cb(error);
            } else {
                payment.captureResponse = captureResponse;
                payment.capturePaymentConfig = captureConfig;
                payment.status = PaymentStatus.COMPLETED;
                payment.amountCaptured = Number(captureResponse.amount) / 100;

                payment.save(function (err) {
                    if (err) {
                        return cb({
                            message: 'Payment captured but backend error occurred'
                        });
                    }

                    cb(false);
                });
            }
        })
    },

    check: function (user) {
        return !!user.stripeCustomerId;
    },

    charge: function (customer, amount, cb) {
        stripe.charges.create({
            amount: amount, // Amount in cents
            currency: "usd",
            customer: customer
        }, function (err, charge) {
            cb(err, charge)
        });
    },

    refund: function (user, payment, cb) {
        // stripe doesn't need to refund anything
        return cb(false);
    }
};

module.exports = controller;