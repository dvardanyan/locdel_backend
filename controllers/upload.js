var express = require('express');
var app = express();
var isProd = app.get('env') === 'production';
// env
var multer = require('multer'),
    uuid = require('uuid'),
    fs = require('fs'),
    path = require('path'),
    errors = require('../public/errors.js'),
    imagesDest = isProd ? '/www/locdel/images' : 'public/images',
    attachmentDest = isProd ? '/www/locdel/images' : 'public/attachment';


//create directory if doesn't exists
if (!fs.existsSync(imagesDest)) {
    fs.mkdirSync(imagesDest);
}

var imageUploader = multer({
    dest: imagesDest
});

var attachment = multer({
    dest: attachmentDest
});

function init(app, router) {
    /*Handling routes.*/
    app.post('/api/photo', imageUploader.single('photo'), function (req, res) {
        var newName = uuid.v4().replace(/-/g, ''),
            file = req.file;

        if (!file) {
            return res.send(errors.get(172));
        }

        var filePath = file.path,
            newFullName = newName + path.extname(file.originalname);

        fs.rename(filePath, file.destination + '/' + newFullName, function (err) {
            var name = err ? file.filename : newFullName;
            return res.send(errors.noError(null, {
                'url': 'https://www.locdel.com/images/' + name
            }));
        });
    });

    app.post(['/admin/api/upload', '/api/upload'], attachment.single('file'), function (req, res) {
        var newName = uuid.v4().replace(/-/g, ''),
            file = req.file;

        if (!file) {
            return res.send(errors.get(172));
        }

        var filePath = file.path,
            newFullName = newName + path.extname(file.originalname);

        fs.rename(filePath, file.destination + '/' + newFullName, function (err) {
            var name = err ? file.filename : newFullName;
            return res.send({
                'status': 'OK',
                'url': attachmentDest + '/' + name
            })
        });
    });

    router.get('/image-test', function (req, res) {
        res.render('images-test');
    });
}

module.exports = init;
