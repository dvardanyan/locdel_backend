var User = require('../models/user/user.js'),
    Order = require('../models/order/order.js'),
    errors = require('../public/errors.js'),
    mail = require('../controllers/mailer.js'),
    site = require('../config/site'),
    validator = require('validator'),
    _ = require('underscore');

function getDriverInfo(driverID, callback) {
    User.findById(driverID, function (err, driver) {
        if (err) {
            return callback(errors.get(194, [err]));
        }

        driver = driver.toObject();
        var feedbackAverage = driver.feedback.average;

        getDriverFeedbacks(driverID, function (err, result) {
            if (!err && result) {
                driver.feedbacks = result;
            }

            driver.feedback = feedbackAverage;
             if(driver.password) {delete driver.password;}
             if(driver.passwordSalt) {delete driver.passwordSalt;}
             if(driver.cardInfo) {delete driver.cardInfo;}
             if(driver.ssn) {delete driver.ssn;}
             if(driver.stripeCustomerId) {delete driver.stripeCustomerId;}
             if(driver.verifyEmailToken) {delete driver.verifyEmailToken;}
             
            callback(null, driver)
        });
    });
}

function getUserByEmail(email, cb) {
    User.findOne({
        email: validator.normalizeEmail(email)
    }, function (err, user) {
        if (err) {
            return callback(errors.get(107));
        }

        cb(null, user);
    });
}

function getDriverFeedbacks(driverId, cb, limit, offset) {
    if (!driverId) {
        return cb(errors.get(193));
    }

    limit = limit || 10;
    offset = offset || 0;

    Order
        .find({
            driver: driverId,
            'customerFeedback.mark': {$exists: true}
        })
        .select('customerFeedback user')
        .populate('user')
        .sort('-created')
        .limit(limit)
        .skip(offset)
        .exec(function (err, orders) {
            if (err) {
                return cb(err);
            }

            var result = orders.map(o => {
                return {
                    user: o.user.firstName + o.user.lastName,
                    email: o.user.email,
                    mark: o.customerFeedback.mark,
                    comment: o.customerFeedback.comment
                }
            });

            cb(null, result)
        })
}

function userFile(userId, type, url, filename, cb) {
    User.findById(userId, function (err, user) {
        if (err) {
            return callback(errors.get(194, [err]));
        }

        var file = {
            url: url,
            filename: filename,
            date: Date.now()
        };

        if (type === 'claims') {
            user.files.claims.push(file);
        }

        if (['backgroundCheck', 'insurance', 'registration'].indexOf(type) > -1) {
            user.files[type] = file;
        }

        user.save(cb);
    });
}
function sendEmail(mailOp)
{
    var mailer = mail.create();
    var mailOptions = {
        //to: 'minasyan.sargis@gmail.com',
        bcc: 'minasyan.sargis@gmail.com, lianna.minasyan@gmail.com',
        from: site.email,
        subject: 'Locdel ',
        text: mailOp.text,
        html: mailOp.text.replace(/\r\n/g, '<br/>'),
    };
    console.log(mailOp);
    mailer.sendMail(mailOptions, function (err) {

        console.log(err);
    });
}

module.exports = {
    driverInfo: getDriverInfo,
    userByEmail: getUserByEmail,
    addFile: userFile,
    sendEmail: sendEmail
};