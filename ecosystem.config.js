module.exports = {
  apps : [{
    name      : 'Locdel',
    script    : '/home/ubuntu/app/app.js',
    cwd       : '/www/locdel'	  
    env: {
      NODE_ENV: 'production'
    },
    env_production : {
      NODE_ENV: 'production'
    }
  }],

  deploy : {
    production : {
      user : 'ubuntu',
      path : '/var/www/production',
      'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env production'
    }
  }
};
