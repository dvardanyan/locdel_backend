var mongoose = require('mongoose'),
   // validate = require('../../util/validator.js'),
   // validator = require('validator'),
    Schema = mongoose.Schema;

var AdvertisementSchema = new Schema({
    id: {
        type: 'ObjectId'
    },
    text: {
        type: 'String'
    },
    file: {
        url: {
            type: 'String'
        },
        filename: {
            type: 'String'
        },
        date: {
            type: 'Date'
        }
    },
    isDeleted:{
        type: 'Boolean',
        default: false
    },
    forCustomer:{
        type: 'Boolean',
        default: true
    },
    forDriver:{
        type: 'Boolean',
        default: true
    }
});

var Advertisement = mongoose.model('Advertisement', AdvertisementSchema);


module.exports = Advertisement;
