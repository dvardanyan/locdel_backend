var mongoose = require('mongoose'),
    deepPopulate = require('mongoose-deep-populate')(mongoose),
    OrderStepType = require('./order-step-type.js'),
    NotificationStatus = require('./notification-status.js'),
    Schema = mongoose.Schema;

var NotificationSchema = new Schema({
    user: {
        type: 'ObjectId',
        ref: 'User',
        required: true
    },
    number: {
        type: 'Number',
        required: true
    },
    status: {
        type: 'Number',
        default: NotificationStatus.UNREAD,
        required: true
    },
    date: {
        type: 'Date'
    },
    order: {
        type: 'ObjectId',
        ref: 'Order',
        required: true
    },
    payload: ['Mixed']
});

NotificationSchema.pre('save', function (next) {
    var now = new Date();
    if (!this.date) {
        this.date = now;
    }
    next();
});

NotificationSchema.plugin(deepPopulate);
var Notification = mongoose.model('Notification', NotificationSchema);

module.exports = Notification;