const OrderCancelReason = {
    CUSTOMER: 0,
    WRONG_VEHICLE: 1,
    CUSTOMER_UNAVAILABLE: 2,
    CRASH: 3,
    VEHICLE_BROKE: 4,
    CUSTOMER_CANCELED_ORDER: 5,
    OTHER: 6
};

module.exports = OrderCancelReason;