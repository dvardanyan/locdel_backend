var orderFilters = {
    TransportType: {
        Car: 1,
        Motorcycle: 2,
        PickUp: 4,
        CarMotorcycle:3,
        CarPickUp:5,
        MotorcyclePickUp:6,
        MotorcycleCarPickUp:7
    }
};

module.exports = orderFilters;