var OrderStatus = {
    PENDING: 1,
    PICKED: 2,
    COMPLETED: 3,
    CANCELED: 4,
    HOLD: 5,
    FAIL: 6,
    INITIAL: 7,
    INACTIVE: 8 // state which holds need data but not available for payment or pick
};

OrderStatus.name = function (value) {
    return Object.keys(OrderStatus).filter(function (prop) {
        return OrderStatus[prop] === value;
    })[0];
};

module.exports = OrderStatus;