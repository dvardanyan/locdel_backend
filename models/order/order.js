var mongoose = require('mongoose'),
    OrderStepType = require('./order-step-type.js'),
    OrderFilters = require('./order-filters.js'),
    OrderStatus = require('./order-status.js'),
    OrderType = require('./order-type.js'),
    validate = require('../../util/validator.js'),
    validator = require('validator'),
    Schema = mongoose.Schema;

var OrderSchema = new Schema({
    user: {
        type: 'ObjectId',
        ref: 'User'
    },
    driver: {
        type: 'ObjectId',
        ref: 'User'
    },
    parent: {
        type: 'ObjectId',
        ref: 'Order'
    },
    location: {
        type: ['Number'],
        index: '2dsphere',
        required: true
    },
    steps: [
        {
            address: {
                type: 'String',
                required: true
            },
            lat: {
                type:  'Number'
            },
            long: {
                type:  'Number'
            },
            name: {
                type: 'String'
            },
            contactNumber: {
                type: 'String'
            },
            description: {
                type: 'String'
            },
            specialRequest: {
                type: 'String'
            },
            type: {
                type: 'Number',
                default: OrderStepType.PICK
            },
            images: [
                {
                    url: {
                        type: 'String'
                    }
                }
            ],
            completeDate: {
                type: 'Date'
            },
            driverImages: [
                {
                    url: {
                        type: 'String'
                    },
                    thumbnail:{
                        type: 'String'
                    }
                }

            ],
            apartmentNo: {
                type: 'String'
            }
        }
    ],
    status: {
        type: 'Number',
        default: OrderStatus.INITIAL
    },
    signURL: {
        type: 'String'
    },
    created: {
        type: 'Date'
    },
    distance: {
        type: 'Number'
    },
    eta: {
        type: 'Number',
        default: 0
    },
    invoiceNumber: {
        type: 'String'
    },
    cost: {
        type: 'Number',

    },
    carCost: {
        type: 'Number',

    },
    motoCost: {
        type: 'Number',

    },
    pickUpCost: {
    type: 'Number',

    },
    tip: {
        type: 'Number'
    },
    type: {
        type: 'Number',
        default: OrderType.REGULAR
    },
    filters: {
        transportType: {
            type: 'Number',
            default: OrderFilters.TransportType.Car
        },
        category: {
            type: 'Number'
        }
    },
    schedule: {
        type: 'ObjectId',
        ref: 'Schedule'
    },
    history: [
        {
            status: {
                type: 'Number'
            },
            date: {
                type: 'Date'
            },
            data: {
                type: 'Mixed'
            }
        }
    ],
    customerFeedback: {
        mark: {
            type: 'Number'
        },
        comment: {
            type: 'String'
        }
    },
    driverFeedback: {
        mark: {
            type: 'Number'
        },
        comment: {
            type: 'String'
        }
    },
    comments: [
        {
            type: 'String'
        }
    ],
    cancelStatus: {
        type: 'Number'
    },
    cancelComment: {
        type: 'String'
    },
    paymentProceed: {
        type: 'Boolean',
        default: false
    },
    accidentDriverNumber: {
        type: 'String'
    },
    encodedRoute: {
        type: 'String'
    }
});

OrderSchema.pre('save', function (next) {
    var now = new Date();
    if (!this.created) {
        this.created = now;
    }
    next();
});

OrderSchema.methods = {
    calculateCost: function () {
        return (this.cost || 0) + (this.tip || 0);
    }
};

var Order = mongoose.model('Order', OrderSchema);

Order.schema.path('steps').validate(function (steps) {
    return !steps || Array.isArray(steps);
});

Order.schema.path('steps').schema.path('address').validate(function (address) {
    return validate('address', address);
});

Order.schema.path('steps').schema.path('type').validate(function (type) {
    return validate('enum', OrderStepType, type);
});

Order.schema.path('steps').schema.path('contactNumber').validate(function (contactNumber) {
    return !contactNumber || validate('contactNumber', contactNumber);
});

Order.schema.path('steps').schema.path('images').schema.path('url').validate(function (url) {
    return !url || validator.isURL(url, {
            allow_protocol_relative_urls: true,
            require_tld: false
        });
});


Order.schema.path('steps').validate(function (steps) {
    return !steps || Array.isArray(steps)
});

Order.schema.path('status').validate(function (status) {
    return validate('enum', OrderStatus, status);
});

Order.schema.path('type').validate(function (type) {
    return validate('enum', OrderType, type);
});

Order.schema.path('filters.transportType').validate(function (type) {
    return validate('enum', OrderFilters.TransportType, type);
});

module.exports = Order;
