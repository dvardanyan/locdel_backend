var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ScheduleSchema = new Schema({
    order: {
        type: 'ObjectId',
        ref: 'Order',
        required: true
    },
    user: {
        type: 'ObjectId',
        ref: 'User',
        required: true
    },
    date: {
        type: 'Mixed',
        required: true
    },
    disabled: {
        type: 'Boolean',
        default: false
    }
});

var Schedule = mongoose.model('Schedule', ScheduleSchema);

module.exports = Schedule;
