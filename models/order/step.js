var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var StepSchema = new Schema({
    address: {
        type: 'String',
        required: true
    },
    name: {
        type: 'String'
    },
    contactNumber: {
        type: 'String'
    },
    disabled: {
        type: 'Boolean',
        default: false
    },
    user: {
        type: 'ObjectId',
        ref: 'User',
        required: true
    },
    lat: {
        type: 'String'
    },
    long: {
        type: 'String'
    },
    apartmentNo: {
        type: 'String'
    }
});

var Step = mongoose.model('Step', StepSchema);

module.exports = Step;
