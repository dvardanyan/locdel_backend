var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var TrackingSchema = new Schema({
    order: {
        type: 'ObjectId',
        ref: 'Order'
    },
    latitude :{
        type: 'Number',
        required: true
    },
    longitude:{
        type: 'Number',
        required: true
    }
});

var Tracking = mongoose.model('Tracking', TrackingSchema);

module.exports = Tracking;
