var PaymentStatus = {
    COMPLETED: 1,
    ERROR: 2,
    PENDING: 3,
    AUTHORIZED: 4,
    REFUNDED: 5
};

module.exports = PaymentStatus;