var mongoose = require('mongoose'),
    deepPopulate = require('mongoose-deep-populate')(mongoose),
    PaymentStatus = require('./payment-status.js'),
    validate = require('../../util/validator.js'),
    validator = require('validator'),
    Schema = mongoose.Schema;

var PaymentSchema = new Schema({
    order: {
        type: 'ObjectId',
        ref: 'Order'
    },
    status: {
        type: 'Number',
        default: PaymentStatus.PENDING
    },
    method: {
        type:'String'
    },
    authPaymentConfig: {
        type: 'Mixed'
    },
    authResponse: {
        type: 'Mixed'
    },
    captureResponse: {
        type: 'Mixed'
    },
    capturePaymentConfig: {
        type: 'Mixed'
    },
    authId: {
        type: 'String'
    },
    amountCaptured: {
        type: 'Number'
    },
    refundResponse: {
        type: 'Mixed'
    },
    errorResponse: {
        type: 'Mixed'
    },
    processed: {
        type: 'Boolean',
        default: false
    }
});


PaymentSchema.plugin(deepPopulate);


var Payment = mongoose.model('Payment', PaymentSchema);


Payment.schema.path('status').validate(function (status) {
    return validate('enum', PaymentStatus, status);
});

module.exports = Payment;
