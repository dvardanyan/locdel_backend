var mongoose = require('mongoose'),
    validate = require('../../util/validator.js'),
    TransportType = require('./user-transport-type.js'),
    Schema = mongoose.Schema;

var UserDataSchema = new Schema({
    id: {
        type: 'ObjectId',
        required: true,
        ref: 'User'
    },
    firstName: {
        type: 'String'
    },
    lastName: {
        type: 'String'
    },
    billingAddress: {
        type: 'String'
    },
    city: {
        type: 'String'
    },
    state: {
        type: 'String'
    },
    zipCode: {
        type: 'String'
    },
    contactNumber: {
        type: 'String'
    },
    ssn: {
        type: 'String'
    },
    dateOfBirth: {
        type: 'Date'
    },
    car: {
        type: {
            type: 'Number',
            default: TransportType.Car
        },
        manufacturer: {
            type: 'String'
        },
        model: {
            type: 'String'
        },
        year: {
            type: 'Number'
        },
        color: {
            type: 'String'
        },
        licensePlate: {
            type: 'String'
        },
        backgroundCheck: {
            type: 'String'
        },
        images: [{
            type: 'String'
        }]
    }
});

var UserData = mongoose.model('UserData', UserDataSchema);

UserData.schema.path('billingAddress').validate(function (address) {
    return validate('address', address);
});

UserData.schema.path('ssn').validate(function (ssn) {
    return validate('ssn', ssn);
});

UserData.schema.path('contactNumber').validate(function (number) {
    return validate('contactNumber', number);
});

UserData.schema.path('car.type').validate(function (type) {
    return validate('enum', TransportType, type);
});

module.exports = UserData;
