const UserDeviceType = {
  IOS: 1,
  ANDROID: 2
};

module.exports = UserDeviceType;