const UserRegStatus = {
    JUSEDREGISTERED: 0,
    NEWUSER: 1,
    OLDUSER: 2
};

module.exports = UserRegStatus;