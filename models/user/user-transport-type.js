const TransportType = {
    Car: 1,
    Motorcycle: 2,
    PickUp: 4
};

module.exports = TransportType;