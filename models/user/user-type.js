const UserType = {
    OWNER: 0,
    GENERALMANAGER: 3,
    REGULARMANAGER: 4,
    EMPLOYEE: 5,
    CUSTOMER: 1,
    DRIVER: 2
};

module.exports = UserType;