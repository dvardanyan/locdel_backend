var mongoose = require('mongoose'),
    UserType = require('./user-type.js'),
    TransportType = require('./user-transport-type.js'),
    Schema = mongoose.Schema,
    uuid = require('uuid'),
    validate = require('../../util/validator.js'),
    crypto = require('crypto');

var CounterSchema = new Schema({
    _id: {type: String, required: true},
    seq: {type: Number, default: 0}
});

var counter = mongoose.model('counter', CounterSchema);

var UserSchema = new Schema({
    profileImage: {
        type: 'String'
    },
    email: {
        type: 'String'
    },
    password: {
        type: 'String'
    },
    passwordSalt: {
        type: 'String'
    },
    adminNumber: {
        type: 'Number'
    },
    blocked: {
        type: 'Boolean',
        default: false
    },
    type: {
        type: 'Number',
        default: UserType.CUSTOMER
    },
    checked: {
        type: 'Boolean',
        default: false
    },
    isNewUser: {
        type: 'Number',
        default: 0
    },
    epGenerated: {
        type: 'Boolean',
        default: false
    },
    emailChecked: {
        type: 'Boolean',
        default: false
    },
    firstName: {
        type: 'String'
    },
    lastName: {
        type: 'String'
    },
    billingAddress: {
        type: 'String'
    },
    city: {
        type: 'String'
    },
    state: {
        type: 'String'
    },
    zipCode: {
        type: 'String'
    },
    contactNumber: {
        type: 'String'
    },
    ssn: {
        type: 'String'
    },
    dateOfBirth: {
        type: 'Date'
    },
    created: {
        type: 'Date'
    },
    car: {
        type: {
            type: 'Number',
            default: TransportType.Car
        },
        manufacturer: {
            type: 'String'
        },
        model: {
            type: 'String'
        },
        year: {
            type: 'Number'
        },
        color: {
            type: 'String'
        },
        licensePlate: {
            type: 'String'
        },
        backgroundCheck: {
            type: 'String'
        },
        images: {
            vehicleFront: {
                type: 'String'
            },
            vehicleBack: {
                type: 'String'
            },
            vehicleReg: {
                type: 'String'
            },
            vehicleInsurance: {
                type: 'String'
            },
            driverLicence: {
                type: 'String'
            },
            driverPhoto: {
                type: 'String'
            }
        }
    },
    feedback: {
        average: {
            type: 'Number'
        },
        count: {
            type: 'Number'
        },
        list: [{
            from: {
                type: 'ObjectId'
            },
            average: {
                type: 'Number'
            },
            count: {
                type: 'Number'
            }
        }]
    },
    invoiceNumber: {
        type: 'Number'
    },
    verifyUserToken: {
        type: 'String'
    },
    verifyEmailToken: {
        type: 'String'
    },
    resetPasswordToken: {
        type: 'String'
    },
    resetPasswordExpires: {
        type: 'Date'
    },
    paymentRefreshToken: {
        type: 'String'
    },
    stripeCustomerId: {
        type: 'String'
    },
    cardInfo: {
        lastDigits: {type: 'Number'},
        expYear: {type: 'Number'},
        expMonth: {type: 'Number'}
    },
    devices: [
        {
            token: {
                type: 'String'
            },
            active: {
                type: 'Boolean'
            },
            type: {
                type: 'Number'
            }
        }
    ],
    files: {
        claims: [{
            url: {type: 'String'},
            filename: {type: 'String'},
            date: {type: 'Date'}
        }],
        backgroundCheck: {
            url: {type: 'String'},
            filename: {type: 'String'},
            date: {type: 'Date'}
        },
        insurance: {
            url: {type: 'String'},
            filename: {type: 'String'},
            date: {type: 'Date'}
        },
        registration: {
            url: {type: 'String'},
            filename: {type: 'String'},
            date: {type: 'Date'}
        },
    }
});

UserSchema.pre('save', function (next) {
    var doc = this;
    if (this.type === UserType.OWNER && this.adminNumber == null) {
        counter.findByIdAndUpdate({_id: 'Users'}, {$inc: {seq: 1}}, {
            new: true,
            upsert: true
        }, function (error, counter) {
            if (error) {
                return next(error);
            }

            doc.adminNumber = counter.seq;
            next();
        });
    } else {
        next();
    }
});


UserSchema.methods = {
    generateSalt: function () {
        return uuid.v4();
    },
    createPasswordHash: function (password) {
        if (password === '' || typeof password !== 'string') {
            return null;
        }

        this.passwordSalt = this.generateSalt();

        var hash = this.encryptPassword(password);

        if (hash === '') {
            return null;
        }

        this.password = hash;

        return true;
    },
    encryptPassword: function (password) {
        if (!password) return '';
        try {
            return crypto
                .createHmac('sha1', this.passwordSalt)
                .update(password)
                .digest('hex');
        } catch (err) {
            return '';
        }
    },
    authenticate: function (plainText) {
        return this.encryptPassword(plainText) === this.password;
    }
};

UserSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        delete ret.password;
        delete ret.passwordSalt;
        delete ret.paymentRefreshToken;
        delete ret.resetPasswordToken;
        delete ret.stripeCustomerId;
        delete ret.resetPasswordExpires;
        delete ret.verifyUserToken;
        delete ret.files;

        if (ret.car) {
            delete ret.car.licensePlate;
            delete ret.car.backgroundCheck;
        }

        return ret;
    }
});

var User = mongoose.model('User', UserSchema);

// User.schema.path('billingAddress').validate(function (address) {
//     return validate('address', address);
// });
//
// User.schema.path('ssn').validate(function (ssn) {
//     return validate('ssn', ssn);
// });

// User.schema.path('contactNumber').validate(function (number) {
//     return validate('contactNumber', number);
// });

User.schema.path('car.type').validate(function (type) {
    return validate('enum', TransportType, type);
});

module.exports = User;
