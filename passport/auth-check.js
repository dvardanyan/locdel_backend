var errors = require('../public/errors.js');

module.exports = function isAuthenticated(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    res.send(errors.get(114));
};