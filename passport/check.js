var errors = require('../public/errors.js'),
    UserType = require('../models/user/user-type.js');

function isDriver(req, res, next) {
    if (req.user.type === UserType.DRIVER) {
        return next();
    }

    res.send(errors.get(119));
}

function isAuthenticated(req, res, next) {
    if (req.isAuthenticated()) {
        if (req.user.blocked) {
            return res.send(errors.get(213));
        }

        return next();
    }

    res.send(errors.get(114));
}

function isCustomer(req, res, next) {
    if (req.user.type === UserType.CUSTOMER) {
        return next();
    }

    res.send(errors.get(127));
}

function isOwner(req, res, next) {
    if (!req.isAuthenticated()) {
        return res.send(errors.get(114));
    }

    if (req.user.type === UserType.OWNER) {
        return next();
    }
    res.send(errors.get(139));
}

function isAdminGroup1(req, res, next) {
    if (!req.isAuthenticated()) {
        return res.send(errors.get(114));
    }

    if (req.user.type === UserType.OWNER || req.user.type === UserType.GENERALMANAGER
        || req.user.type === UserType.REGULARMANAGER || req.user.type === UserType.EMPLOYEE
    ) {
        return next();
    }
    res.send(errors.get(221));
}

function isAdminGroup2(req, res, next) {
    if (!req.isAuthenticated()) {
        return res.send(errors.get(114));
    }

    if (req.user.type === UserType.OWNER || req.user.type === UserType.GENERALMANAGER || req.user.ty === UserType.REGULARMANAGER) {
        return next();
    }
    res.send(errors.get(222));
}

function isAdminGroup3(req, res, next) {
    if (!req.isAuthenticated()) {
        return res.send(errors.get(114));
    }

    if (req.user.type === UserType.OWNER || req.user.type === UserType.GENERALMANAGER) {
        return next();
    }
    res.send(errors.get(223));
}


module.exports = {
    isDriver: isDriver,
    isCustomer: isCustomer,
    isAuthenticated: isAuthenticated,
    isOwner: isOwner,
    isAdminGroup1: isAdminGroup1,
    isAdminGroup2: isAdminGroup2,
    isAdminGroup3: isAdminGroup3,
};