var User = require('../models/user/user.js'),
    errors = require('../public/errors.js'),
    validator = require('validator'),
    LocalStrategy = require('passport-local').Strategy;


module.exports = function (passport) {
    passport.use('login', new LocalStrategy({
            passReqToCallback: true,
            usernameField: 'email'
        },
        function (req, username, password, done) {
            // normalize email
            var usernameNorm = validator.normalizeEmail(username);
            if(!usernameNorm){
                return done(null, false, errors.get(101));
            }
            // if(req.body.usertype!=1)
            //     req.body.usertype=1;
            // if a user with username exists or not
            User.findOne({'email': usernameNorm,'type': req.body.usertype },
                function (err, user) {
                    // In case of any error, return using the done method
                    if (err)
                        return done(err);
                    // Username does not exist, log error & redirect back
                    if (!user) {
                        return done(null, false, errors.get(103));
                    }
                    // User exists but wrong password, log the error
                    if (!user.authenticate(password)) {
                        return done(null, false, errors.get(103));
                    }


                    //user is not verified
                    if(!user.checked){
                        return done(null, false, errors.get(214));
                    }

                    if (user.blocked) {
                        return done(null, false, errors.get(213));
                    }
                    // User and password both match, return user from
                    // done method which will be treated like success
                    return done(null, user);
                }
            );
        }));
};