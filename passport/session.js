var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    Session = mongoose.model('Session', new Schema(), 'sessions');

function logoutAllSessions(userId, cb) {
    var filter = {'session':{'$regex': '.*"user":"'+userId+'".*'}}
    Session.remove(filter, function (err) {
        cb(err);
    })
}

module.exports = {
    logoutAllSessions: logoutAllSessions
};