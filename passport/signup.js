/**
 * Created by Vahagn on 8/22/15.
 */

var User = require('../models/user/user.js'),
    UserType = require('../models/user/user-type.js'),
    UserRegStatus = require('../models/user/user-reg-status.js'),
    validator = require('validator'),
    uuid = require('uuid'),
    errors = require('../public/errors.js'),
    LocalStrategy = require('passport-local').Strategy,
    mail = require('../controllers/mailer.js'),
    stripe = require('../controllers/stripe.js'),
    site = require('../config/site.js'),
    configs = require('../config/config'),
    twilio = configs.credentials.twilio,
    generator = require('generate-password');
const client = require('twilio')(
    twilio.accountSId,
    twilio.authToken
);
const crypto = require('crypto');

var setUserData = function (model, userData) {
    function setProp(prop) {
        if (userData[prop]) {
            model[prop] = userData[prop];
        }
    }

    [ 'profileImage','firstName', 'lastName', 'billingAddress', 'city',
        'state', 'zipCode', 'contactNumber', 'car', 'dateOfBirth', 'ssn'].forEach(function (prop) {
        setProp(prop);
    });
};

var beginVerify = function (user, req) {
    var mailer = mail.create();
    var link = 'http://' + req.headers.host + '/api/emailVerify/' + user._id + '/' + user.verifyEmailToken ;
    var tag = `<a href="${link}">Verify</a>`;

    var mailOptions = {
        to: user.email,
        from: site.email,
        subject: 'Locdel Profile Verification',
        text: 'You are receiving this because you (or someone else) have requested to verify your account.\n\n' +
        'Please click on the following link:\n\n' +
        tag + '\n\n' +
        'If you did not request this, please ignore this email.\n',
        html: 'You are receiving this because you (or someone else) have requested to verify your account.<br/>' +
        'Please click on the following link: ' +
        tag + '<br/>' +
        'If you did not request this, please ignore this email.<br/>'
    };
    mailer.sendMail(mailOptions);
};

var sendMailForSnn = function (user) {
    var mailer = mail.create();

    var mailOptions = {
        to: user.email,
        from: site.email,
        subject: 'Locdel Profile Verification',
        text: 'You are receiving this because you (or someone else) have requested to verify your account.\n\n' +
        'Please click on the following link ...If you did not request this, please ignore this email.\n',
        html: 'You are receiving this because you (or someone else) have requested to verify your account.<br/>' +
        'Please click on the following link: ....If you did not request this, please ignore this email.<br/>'
    };
    mailer.sendMail(mailOptions);
};

module.exports = function (passport) {
    passport.use('signup', new LocalStrategy({
            passReqToCallback: true,
            usernameField: 'email'
        },
        function (req, username, password, done) {
            findOrCreateUser = function () {

                username = validator.normalizeEmail(req.body.email);
                if(!username){
                    return done(null, false, errors.get(101));
                }

                function saveUser(user) {
                    user.save(function (err) {
                        if (err) {
                            return done(null, false,errors.get(104, err.errors));
                        }
                        var response = user;
                        return done(null, response);
                    });
                }
                //  username = validator.normalizeEmail(username);
                if (!req.body.contactNumber) {
                    return done(null, false,errors.get(217));
                }
                var type = parseInt(req.body.usertype);

                if (type === UserType.DRIVER)
                {
                    var driverPhoneNumberCount = 0;
                    User.find({
                        contactNumber: req.body.contactNumber,
                        type: type
                    }, function (err, users) {
                       driverPhoneNumberCount = users.length;
                    });
                }

                if (!type) {
                    return done(null, false,errors.get(219));
                }
                // if (!req.body.verifyUserToken && type === UserType.CUSTOMER) {
                //     return done(null, false,errors.get(218));
                // }
                else
                {
                   /* var foundData = {$or :[
                            {
                                email: username,
                                //contactNumber: req.body.contactNumber,
                                type: req.body.usertype

                            },
                            {
                                // email: username,
                                contactNumber: req.body.contactNumber,
                                verifyUserToken: req.body.verifyUserToken,
                                type: req.body.usertype

                            }
                        ]};*/
                   if (!req.body.verifyUserToken) {
                        var foundData = {
                            email: username,
                            //contactNumber: req.body.contactNumber,
                            type: req.body.usertype
                        }
                    }
                    else if(req.body.verifyUserToken && type === UserType.CUSTOMER){

                        var foundData = {
                            contactNumber: req.body.contactNumber,
                            verifyUserToken: req.body.verifyUserToken,
                            type: req.body.usertype
                        }
                    }
                }

                User.findOne(foundData, function (err, user) {
                    // In case of any error return
                    if (err) {
                        return done(null, false,err);
                    }


                    var newUser = new User();
                    if (!user && type === UserType.CUSTOMER && req.body.verifyUserToken) {
                        return done(null, false,errors.get(220));
                    }
                    else if (req.body.verifyUserToken && user && type === UserType.CUSTOMER) {
                        var newUser = user;
                        newUser.checked = true;

                        if(newUser.isNewUser == UserRegStatus.NEWUSER)
                            newUser.isNewUser = UserRegStatus.OLDUSER
                        if(newUser.isNewUser == UserRegStatus.JUSEDREGISTERED)
                            newUser.isNewUser = UserRegStatus.NEWUSER

                        newUser.verifyUserToken = undefined;
                        if(!user.email || !user.password){
                            newUser.email = username;
                            var isOk = newUser.createPasswordHash(password);

                            if (!isOk) {
                                return done(null, false,errors.get(103));
                            }
                            newUser.epGenerated = true;
                        }

                        // return res.send(errors.get(100));
                    }else if (user) {
                        return done(null, false,errors.get(100));
                    }

                    if (!(type === UserType.CUSTOMER || type === UserType.DRIVER)) {
                        return done(null, false,errors.get(102));
                    }


                    // set the user's local credentials
                    // need to save card info for drivers
                    if (!req.body.verifyUserToken) {


                        var smsToken = Math.floor(Math.random()*9000) + 1000;

                        newUser.email = username;

                        newUser.emailChecked = false;
                        newUser.created = Date.now();
                        newUser.verifyEmailToken = newUser.generateSalt();
                        newUser.verifyUserToken = smsToken;
                        newUser.contactNumber = req.body.contactNumber;
                        newUser.checked = false;
                        newUser.type = type;
                        if (type === UserType.DRIVER){
                            /*if (!req.body.ssn) {
                                return done(null, false,errors.get(203));
                            }*/
                            //Lianna must be reveted newUser.blocked = true;
                            if (driverPhoneNumberCount > 0)
                            {
                                return done(null, false, errors.get(230));
                            }
                            newUser.billingAddress = req.body.billingAddress;
                            newUser.zipCode = req.body.zipCode;
                            newUser.city = req.body.city;
                            newUser.state = req.body.state;
                            newUser.ssn = req.body.ssn;

                            const cardInfo = req.body.cardInfo
                            if (!cardInfo) {
                                return done(null, false, errors.get(216));
                            }

                            newUser.cardInfo = cardInfo;
                            stripe.generateCustomer(cardInfo, function (err, customer) {
                                if (err) {
                                    return done(null, false, errors.get(204));
                                }

                                newUser.stripeCustomerId = customer.id;

                            });

                            if (newUser.verifyUserToken) {
                                client.messages.create(
                                    {
                                        body: 'Locdel: ' + newUser.verifyUserToken + ' your code',
                                        to: newUser.contactNumber,
                                        from: twilio.number,
                                    }, function (err, message) {
                                        if (err) {
                                            return done(null, false, errors.get(229));
                                        }
                                    });
                            }

                        }
                        beginVerify(newUser, req);
                        var isOk = newUser.createPasswordHash(password);

                        if (!isOk) {
                            return done(null, false,errors.get(103));
                        }





                    }
                    newUser.firstName = req.body.firstName;
                    newUser.lastName = req.body.lastName;
                    setUserData(newUser, req.body);
                    saveUser(newUser);


                });
            };

            // Delay the execution of findOrCreateUser and execute
            // the method in the next tick of the event loop
            process.nextTick(findOrCreateUser);
        })
    );
};