var schedule = require('node-schedule'),
    PaymentStatus = require('../models/payment/payment-status.js'),
    Payment = require('../models/payment/payment.js'),
    stripe = require('../controllers/stripe.js'),
    mail = require('../controllers/mailer'),
    site = require('../config/site'),
    util = require('util');

var rule = {
    second: 0,
    hour: 0,
    minute: 0,
    dayOfWeek: 0
};

function sendMail(user, subject, text, cb) {
    var mailer = mail.create();

    var mailOptions = {
        to: user,
        from: site.email,
        subject: subject,
        text: text,
        html: text.replace(/\r\n/g, '<br/>'),
    };
    mailer.sendMail(mailOptions, function (err) {
        cb && cb(err);
    });
}


function error(email, label, err) {
    var fullLabel = "Payment Processor-Driver: " + label;
    var receivers = site.paymentSupportEmails.slice();

    err = util.inspect(err);

    if (email) {
        receivers.push(email);
    }

    receivers.forEach(u => sendMail(u, fullLabel, err));

    util.log(fullLabel);
    util.log(err);
    util.log(fullLabel + " : end");
}


function processOne(data) {
    if (!data.stripeCustomerId) {
        sendMail(data.email, "Weekly Payment Processing Error", "Your card was not found, please add card for making payment possible");

        const message = "Can't find drvire card, for: " + data.email;
        return error(null, "Driver card not found", message);
    }

    if (!data.amount || !data.payments.length) {
        return util.log("Payment Processor-Driver: Amount of transferred is 0, for: " + data.email);
    }

    stripe.charge(data.stripeCustomerId, Math.round(data.amount), function (err, charge) {
        if (err) {
            error(null, "Transfer error happened", err);

            return;
        }

        Payment.update({'_id': {$in: data.payments}},
            {
                $set: {processed: true}
            }, {
                multi: true
            },
            function (err, modified) {
                if (err) {
                    error(null, "Payment status update error: Payments charged for driver but remained as not proceed", err);

                    return;
                }

                if (modified.nModified !== data.payments.length) {
                    const message = "Payment status update error : Payments charged for driver but some of maybe remained as not proceed, need for manual check";

                    error(null, message, err);

                    return;
                }

                util.log("Payment Processor-Driver: Successful payment process");
                util.log(data);
                util.log("----------------------------------------------------");
                util.log(charge);
                util.log("Payment Processor-Driver: Successful payment process : end");

                sendMail(data.email, "Weekly Payment Processing: Success", `${(data.amount / 100).toFixed(2)}$ transferred to your card`);
            })
    })
}

function processingAll() {
    var filters = {
        status: PaymentStatus.COMPLETED,
        processed: false,
    };

    Payment.aggregate([
        {$match: filters},
        {
            $lookup: {
                from: 'orders',
                localField: "order",
                foreignField: "_id",
                as: "orderDetails"
            }
        },
        {$unwind: "$orderDetails"},
        {
            $project: {
                payment: '$_id',
                amount: '$amountCaptured',
                user: '$orderDetails.driver'
            }
        },
        {
            $group: {
                _id: '$user',
                payments: {$push: '$payment'},
                overall: {$sum: '$amount'}
            }
        },
        {
            $lookup: {
                from: 'users',
                localField: "_id",
                foreignField: "_id",
                as: "userDetails"
            }
        },
        {$unwind: "$userDetails"},
        {
            $project: {
                '_id': 0,
                payments: '$payments',
                // driver charge is 80 % of all amount(notice stripe using cents, that's because multiplier = 80(0.8 * 100))
                amount: {$multiply: ['$overall', 80]},
                user: '$_id',
                email: '$userDetails.email',
                stripeCustomerId: '$userDetails.stripeCustomerId'
            }
        },

    ], function (err, results) {
        if (err) {
            util.log("Payment Processor-Driver: Error during aggregation ");
            return util.log(err);
        }

        results.forEach(r=>processOne(r));
    })
}

var isProd = process.env.NODE_ENV === 'production';

if (isProd) {
    schedule.scheduleJob(rule, processingAll);
}
