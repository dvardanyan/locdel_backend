var schedule = require('node-schedule'),
    OrderStatus = require('../models/order/order-status.js'),
    Order = require('../models/order/order.js'),
    Payment = require('../models/payment/payment.js'),
    payment = require('../controllers/payment.js'),
    mail = require('../controllers/mailer'),
    site = require('../config/site'),
    util = require('util');

var rule = "0 */2 * * *";

function sendMail(user, subject, text, cb) {
    var mailer = mail.create();

    var mailOptions = {
        to: user,
        from: site.email,
        subject: subject,
        text: text,
        html: text.replace(/\r\n/g, '<br/>'),
    };
    mailer.sendMail(mailOptions, function (err) {
        cb && cb(err);
    });
}


function error(user, label, err) {
    var fullLabel = "Payment Processor-Order: " + label;
    var receivers = site.paymentSupportEmails.slice();

    if (user) {
        receivers.push(user.email);
    }

    receivers.forEach(u => sendMail(u, fullLabel, err));

    util.log(fullLabel);
    util.log(err);
    util.log(fullLabel + " : end");
}

function successMailText(order) {
    var text = `Order: ${order.invoiceNumber}. Driver ${order.driver.lastName || 'Unknown'}.
    Order Payment proceed successfully. Order cost(included tip): ${order.calculateCost()}`;
    return text;
}

function processOne(order) {
    payment.capture(order.user, order, function (err) {
        if (err) {
            var message = "Order id: " + order._id + '\r\n' + JSON.stringify(err);
            return error(order.user, "Payment process error", message);
        }

        order.paymentProceed = true;
        order.save(function (err) {
            if (err) {
                var message = "Order id: " + order._id + '\r\n' + JSON.stringify(err);
                return error(order.user, "Payment status update error", message);
            }

            sendMail(order.user.email, 'Order payment processing finished', successMailText(order));
        });
    });
}

function processingAll() {
    var hourAgo = Date.now() - 60 * 60 * 1000;
    var filters = {
        status: OrderStatus.COMPLETED,
        paymentProceed: {$ne: true},
        history: {$elemMatch: {status: OrderStatus.COMPLETED, date: {$lte: hourAgo}}}
    };

    util.log("Payment Processor-Order: Starting to work");

    Order.find(filters).populate('user driver').exec(function (err, orders) {
        if (err) {
            util.log("Payment Processor-Order: Error during db query ");
            return util.log(err);
        }
        orders.forEach(o=>processOne(o));
    })
}

var isProd = process.env.NODE_ENV === 'production';

if (isProd) {
    schedule.scheduleJob(rule, processingAll);
}