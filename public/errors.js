var util = require('util');

var errors = {
    100: 'User Already Exists.',
    101: 'Invalid email.',
    102: 'Invalid user type.',
    103: 'Invalid username or password. Please try again.',
    104: 'Can\'t save user.', //*
    105: 'Can\'t save user data.', //deprecated
    106: 'Can\'t verify user.', //*
    107: 'We are sorry but our database did not find an account with this email address.',
    108: 'Can\'t reset password.', //*
    109: 'Can\'t create verification url.', //*
    110: 'Can\'t reset user password.', //*
    111: 'Password reset token is invalid or has expired.',
    112: 'Wrong token.',
    113: 'Can\'t verify user.', // deprecated
    114: 'User is not authenticated.',
    115: 'Can\'t create order.', //*
    116: 'Can\'t get user data.', //deprecated
    117: 'Can\'t create reset url.', //*
    118: 'Driver car data is not specified.', //*
    119: 'User is not driver.',
    120: 'Can\'t find orders.', //*
    121: 'Can\'t find driver data.', //deprecated
    122: 'OrderId not found.',
    123: 'Can\'t pick order.', //*
    124: 'Order already picked',
    125: 'Order already canceled',
    126: 'Order already completed',
    127: 'User is not a customer',
    128: 'It\'s not your order.',
    129: 'Order is not picked.',
    130: 'Can\'t complete order.', //*
    131: 'Can\'t cancel order.', //*
    132: 'Payment code not found.',
    133: 'Can\'t get user refresh token.', //*
    134: 'Payment type not found.',
    135: 'Payment completed but order not found.',
    136: 'Order id not found.',
    137: 'Can\'t process payment.', //*
    138: 'Capture id not found.',
    139: 'User is not super admin.',
    140: 'Can\'t get users.', //*
    141: 'Can\'t get data.', //depricated
    142: 'Can\'t find order.', //*
    143: 'Can\'t find latitude or longitude.', //*
    144: 'Can\'t create order tracking.', // *
    145: 'Can\'t update order tracking.', //*
    146: 'Can\'t find order tracking.', //*
    147: 'Can\'t update user data.',//*
    148: 'User sign not found.',
    149: 'Invalid date.',
    150: 'Can\'t find payments.', //8
    151: 'Order already on hold.',
    152: 'Order already failed.',
    153: 'Can\'t hold order.', // *
    154: 'Driver id not found.',
    155: 'Mark not found.',
    156: 'Wrong driver id.',
    157: 'User already gives a feedback.',
    158: 'User not gives a feedback for this driver.',
    159: 'Can\'t feedback user.', // *
    160: 'Cost not found in configs.',
    161: 'Payment authorized but error occurred.',
    162: 'Payment refund error.',
    163: 'No device token found.',
    164: 'Can\'t save device.', //*
    165: 'Order not found.',
    166: 'Target driver not found.',
    167: 'Feedback already exists.',
    168: 'Order start location does not exists.',
    169: 'Driver location not found.',
    170: 'Can\'t find nearest orders.', // depricated
    171: 'Error during search query.', //*
    172: 'File not found in request.',
    173: 'Old password is wrong.',
    174: 'Can\'t change password.', //*
    175: 'Order in initial state(no payment).',
    176: 'Step Id not found.',
    177: 'Can\'t find order by this step id.',
    178: 'No photo(s) found.',
    179: 'Can\'t save photos.', //*
    180: 'Step not found.',
    181: 'Can\'t add step.',
    182: 'Date not found.',
    183: 'Schedule Id not found.',
    184: 'Can\'t disable schedule.', //*
    185: 'Can\'t enable schedule.', // *
    186: 'Can\'t get schedule(s).',
    187: 'Can\'t create schedule.', // *
    188: 'Can\'t delete schedule.', //*
    189: 'StepId not found.',
    190: 'Final cost not found.',
    191: 'Wrong final cost.',
    192: 'Photo can\'t be add to step in this status ({}).',
    193: 'Driver Id not found.',
    194: 'Can\'t find driver info.', // *
    195: 'Driver lying.',
    196: 'Driver location not found.',
    197: 'Unexpected error.', // *
    198: 'Can\'t update schedule.',
    199: 'Can\'t do a payment.',
    201: 'User id not found.',
    202: 'Can\'t update user block status.',
    203: 'Ssn not found.',
    204: 'Wrong card info.',
    205: 'Can\'t add step to order in this status {}.',
    206: 'Can\'t add driver file.',
    207: 'Notification Id not found.',
    208: 'Can\' update notification status.', //*
    209: 'Error during loading notifications.',
    210: 'Wrong tip.',
    211: 'Can\'t set tip for this order.', //*
    212: 'Error during calculating order cost.',
    213: 'You are blocked. Please contact Locdel support.',
    214: 'User wasn\'t verified with sms.',
    215: 'Wrong field in card info.',
    216: 'Card info not found.',
    217: 'Contact number is required.',
    218: 'Sms verify code is required.',
    219: 'User type is required.',
    220: 'Code is not correct.',
    221: 'User does not belong to admin group 1.',
    222: 'User does not belong to admin group 2.',
    223: 'User does not belong to admin group 3.',
    224: 'User does not belong to admin group 4.',
    225: 'User does not belong to admin group 5.',
    226: 'User already exist,please change email.',
    227: 'Driver car type is not satisfy for this order.',
    228: 'User wasn\'t verified with email.',
    229: 'Send sms failed.',
    230: 'The contact number is already used.',

};

errors.noError = function (message, data) {
    var d = data, m = message;

    if (arguments.length === 1 && typeof arguments[0] === 'object') {
        d = message;
        m = null;
    }

    return {
        status: 'OK',
        code: 200,
        message: m || 'Success',
        data: d
    }
};

errors.get = function (code, errs) {
    var error = {
        'code': code,
        'status': 'error',
        'message': errors[code]
    };

    if (errs) {
        util.log('Backedn error occurred: Error code ' + code);
        util.log(errs);
        util.log('------------------------------------')
    }

    if (errs) {
        if (Array.isArray(errs)) {
            error.errors = errs;
        } else if (typeof errs === 'object') {
            error.errors = process(errs);
        }
    }

    return error;
};

function process(errors) {
    if (!errors) {
        return;
    }

    return Object.keys(errors).map(function (prop) {
        return {
            'property': prop,
            'message': (errors[prop] || {}).message
        }
    });
}

module.exports = errors;