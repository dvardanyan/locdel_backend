"use strict";

let PaymentStatus = require('../models/payment/payment-status.js'),
    Payment = require('../models/payment/payment.js'),
    errors = require('../public/errors.js'),
    UserType = require('../models/user/user-type.js'),
    UserTransportType = require('../models/user/user-transport-type.js'),
    User = require('../models/user/user.js'),
    UserController = require('../controllers/user'),
    AdvertisementController = require('../controllers/advertisement'),
    Order = require('../models/order/order.js'),
    OrderStatus = require('../models/order/order-status.js'),
    OrderFilters = require('../models/order/order-filters'),
    OrderStepType = require('../models/order/order-step-type.js'),
    OrderType = require('../models/order/order-type.js'),
    check = require('../passport/check.js'),
    validate = require('../util/validator.js'),
    validator = require('validator'),
    manager = require('../controllers/data-manager'),
    _ = require('underscore'),
    utils = require('../util/core-util.js');


function getUserData(model) {
    const result = {};

    ['firstName','lastName', 'billingAddress', 'city',
        'state', 'zipCode', 'contactNumber', 'type', 'dateOfBirth', 'ssn'].forEach(function (prop) {
        result[prop] = model[prop];
    });

    if (model.type === UserType.DRIVER) {
        result.car = model.car;
    }

    return result;
}

function setUserData(model, userData) {
    function setProp(prop) {
        if (userData[prop]) {
            model[prop] = userData[prop];
        }
    }

    ['firstName','lastName', 'billingAddress', 'city',
        'state', 'zipCode', 'contactNumber', 'car'].forEach(function (prop) {
        setProp(prop);
    });
}

function getFeedBacksList(user, cb) {
    const isDriver = user.type === UserType.DRIVER;
    const filter = {};
    const prop = isDriver ? 'driver' : 'user';
    const fProp = isDriver ? 'customerFeedback' : 'driverFeedback';

    filter[prop] = user._id;
    filter[fProp + '.mark'] = {$exists: true};

    Order
        .find(filter)
        .select('customerFeedback user driver driverFeedback')
        .populate('user driver')
        .sort('-created')
        .exec(function (err, orders) {
            if (err) {
                return cb(err);
            }


            const feedbacks = orders.map(o => {
                const from = isDriver ? o.user : o.driver;

                return {
                    feedback: o[fProp],
                    from: {
                        id: from._id,
                        firstName: from.firstName,
                        lastName: from.lastName,
                        email: from.email
                    }
                }
            });

            cb(null, feedbacks);
        });
}

module.exports = function (router, passport) {
    router.get('/register-view', check.isAdminGroup1, function (req, res) {
        res.render('register');
    });

    router.get('/register-admin-view', check.isAdminGroup3, function (req, res) {
        res.render('register-admin');
    });
    router.get('/advertisement-view', check.isAdminGroup3, function (req, res) {
        res.render('advertisement');
    });
   /* router.get('/bannerDriver-view', check.isDriver, function (req, res) {
        res.render('banner-driver');
    });
    router.get('/bannerCustomer-view', check.isCustomer, function (req, res) {
        res.render('banner-customer');
    });*/

    router.get('/bannerDriver-view', function (req, res) {
        res.render('bannerDriver');
    });
    router.get('/bannerCustomer-view',  function (req, res) {
        res.render('bannerCustomer');
    });

    router.get('/payments-view', check.isAdminGroup1, function (req, res) {
        Payment
            .find({captureResponse: {$exists: true}})
            .deepPopulate('order.user order.driver')
            .exec(function (err, payments) {
                res.render('payments', {payments: payments});
            })
    });

    router.get('/users-view', check.isAdminGroup1, function (req, res) {
        res.render('users');
    });

    router.get('/orders-view', check.isAdminGroup1, function (req, res) {
        res.render('orders', {status: OrderStatus});
    });

    router.post('/order/comment', check.isAdminGroup1, function (req, res) {
        var orderId = req.body.orderId,
            comment = req.body.comment;

        if (orderId && comment) {
            Order.findByIdAndUpdate(
                orderId,
                {$push: {"comments": comment}},
                {safe: true, upsert: true},
                function (err) {
                    if (err) {
                        return res.send(errors.get(165));
                    }
                    return res.send(errors.noError());
                }
            );
        }
    });

    router.get('/order-view', check.isAdminGroup1, function (req, res) {
        Order
            .findById(req.query.id)
            .populate('user driver')
            .exec(function (err, order) {
                if (!order) {
                    return res.send(errors.get(142))
                }

                const getKey = utils.getEnumKey,
                    orderStatus = OrderStatus;

                order = JSON.parse(JSON.stringify(order));
                order.status = utils.toProperCase(getKey(orderStatus, order.status));
                order.type = utils.toProperCase(getKey(OrderType, order.type));

                order.steps.forEach(function (step) {
                    step.type = utils.toProperCase(getKey(OrderStepType, step.type))
                });

                order.history.forEach(function (h) {
                    h.status = utils.toProperCase(getKey(OrderStatus, h.status));
                });


                order.filters = {
                    transportType: getKey(OrderFilters.TransportType, order.filters.transportType)
                };

                res.render('order', {order: order});
            });
    });

    router.get('/payment-view', check.isAdminGroup1, function (req, res) {
        Payment
            .findById(req.query.id)
            .deepPopulate('order.user order.driver')
            .exec(function (err, payment) {
                if (!payment) {
                    return res.send(errors.get(142))
                }

                const getKey = utils.getEnumKey;

                payment = JSON.parse(JSON.stringify(payment));
                payment.status = utils.toProperCase(getKey(PaymentStatus, payment.status));

                res.render('payment', {payment: payment});
            });
    });

    router.get('/user-view', check.isAdminGroup1, function (req, res) {
        res.render('user-profile');
    });

    router.get('/chat-view', check.isAdminGroup1, function (req, res) {
        res.render('chat');
    });

    router.get('/user-info', check.isAdminGroup1, function (req, res) {
        User.findById(req.query.id, function (err, user) {
            if (!user) {
                res.send(errors.get(107))
            }

            const getKey = utils.getEnumKey;

            const userJSON = JSON.parse(JSON.stringify(user));
            userJSON.type = utils.toProperCase(getKey(UserType, user.type));
            if (user.type === UserType.DRIVER) {
                userJSON.car.type = utils.toProperCase(getKey(UserTransportType, user.car.type));
                userJSON.car.licensePlate = user.car.licensePlate;
                userJSON.files = user.files;
            }

            userJSON.hasStripeCard = !!user.stripeCustomerId;
            userJSON.hasPaypal = !!user.paymentRefreshToken;

            getFeedBacksList(user, function (err, feedbacks) {
                if (err) {
                    return res.send(errors.get(194, [err]))
                }

                userJSON.feedbacks = feedbacks;
                res.send(errors.noError(userJSON));
            })
        });
    });

    router.post('/resource/users', check.isAdminGroup1, function (req, res) {
        let p = req.body;

        p = _.extend(p, req.query);
        const m = new manager.UsersManager(p);
        m.call(function (err, result) {
            if (err) {
                return [];
            }

            return res.send(result);
        });

    });

    router.post('/resource/orders', check.isAdminGroup1, function (req, res) {

	    let p = req.body;

        p = _.extend(p, req.query);
        const m = new manager.OrderManager(p);

        m.call(function (err, result) {
            if (err) {

            return res.send(err);
              //  return [];
            }

            return res.send(result);
        });

    });

    router.post('/resource/user-orders', check.isAdminGroup1, function (req, res) {
        let p = req.body;

        p = _.extend(p, req.query);
        const m = new manager.UserManager(p);
        m.call(function (err, result) {
            if (err) {
                return [];
            }

            return res.send(result);
        });

    });

    router.get('/download', check.isAdminGroup1, function (req, res) {
        const file = req.query.path;
        const name = req.query.filename;

        res.download(file, name);
    });

    router.get('/add-file', check.isAdminGroup1, function (req, res) {
        const file = req.query.path;
        const name = req.query.filename;
        const type = req.query.type;
        const user = req.query.user;

        UserController.addFile(user, type, file, name, function (err) {
            if (err) {
                return res.send(errors.get(206))
            }

            return res.send(errors.noError());
        })
    });
    router.get('/add-advertisementImg', check.isOwner, function (req, res) {
        const file = req.query.path;
        const name = req.query.filename;
        const type = req.query.type;
        const user = req.query.user;
        const text = req.query.text;
        AdvertisementController.SetAdvertisement(user, type, file, name,text, function (err) {
            if (err) {
                return res.send(errors.get(206))
            }

            return res.send(errors.noError());
        })
    });
    router.get('/get-advertisements', function (req, res) {
        const file = req.query.path;
        const name = req.query.filename;
        const type = req.query.type;
        const user = req.query.user;
        const text = req.query.text;

        return AdvertisementController.GetAdvertisements(function (err, advertisements) {
           // const advertisementJSON = JSON.parse(JSON.stringify(advertisements));
            if (err) {
                return res.send(errors.get(206))
            }

            return res.send(errors.noError(advertisements));
        })
    });
    router.get('/get-advertisementsFor', check.isOwner, function (req, res) {
         const dc = req.body.params.DC;
        return AdvertisementController.GetAdvertisementsFor(dc,function (err, advertisements) {
            const advertisementJSON = JSON.parse(JSON.stringify(advertisements));
            if (err) {
                return res.send(errors.get(206))
            }

            return res.send(errors.noError(advertisementJSON));
        })
    });
    router.post('/delete-advertisement', check.isOwner, function (req, res) {
        const fileid = req.body.params.fileid;
        const isDeleted = req.body.params.isDeleted;

        AdvertisementController.DeleteAdvertisement(fileid,isDeleted, function (err) {
            if (err) {
                return res.send(errors.get(206))
            }

            return res.send(errors.noError());
        })
    });
    router.post('/update-advertisement', check.isOwner, function (req, res) {
        const fileid = req.body.params.fileid;
        const forDriver= req.body.params.forDriver;
        const forCustomer= req.body.params.forCustomer;

        AdvertisementController.UpdateAdvertisement(fileid, forDriver,forCustomer, function (err) {
            if (err) {
                return res.send(errors.get(206))
            }

            return res.send(errors.noError());
        })
    });
    router.get('/block', check.isAdminGroup2, function (req, res) {
        const userId = req.query.userId;
        const block = req.query.unblock !== "true";

        if (!userId) {
            return errors.get(201);
        }

        User.update({'_id': userId}, {$set: {blocked: block}}, function (err, modified) {
            if (err || modified.nModified !== 1) {
                return res.send(errors.get(202, err));
            }

            res.send(errors.noError());
        });
    });
    router.post('/sendGroupMsg', check.isAuthenticated, function (req, res) {

        console.log(req.body.text);
        console.log(req.body.to);
        UserController.sendEmail(req.body);
        return res.send(errors.noError());

    });
    router.post('/register/user', check.isAdminGroup1, function (req, res) {
        const username = req.body.email,
            password = req.body.password;
        function done(error) {
            res.send(error)
        }

        User.findOne({'email': username}, function (err, user) {
            // In case of any error return
            if (err) {
                return done(err);
            }
            // already exists
            if (user) {
                return done(errors.get(100));
            } else {
                // if there is no user with that email
                // create the user
                const newUser = new User();

                const type = parseInt(req.body.userType);
                // set the user's local credentials
                // if (!validator.isEmail(username)) {
                //     return done(errors.get(101));
                // }

                if (!validate('enum', UserType, type)) {
                    return done(errors.get(102));
                }

                newUser.email = username;
                newUser.type = type;
                newUser.checked = true;
                newUser.created = Date.now();

                const isOk = newUser.createPasswordHash(password);

                if (!isOk) {
                    return done(errors.get(103));
                }

                //save user data
                setUserData(newUser, req.body);

                // save the user
                newUser.save(function (err) {
                    if (err) {
                        return done(errors.get(104, err.errors));
                    }

                    done(errors.noError());
                });

            }
        });
    });

    router.post('/login', function (req, res, next) {
        passport.authenticate('login', function (err, user, info) {

            if (err) {
                return next(err);
            }
            if (!user) {
                return res.send(info);
            }


            if (user.type == UserType.CUSTOMER
                || user.type == UserType.DRIVER
                ) {
                return res.send(errors.get(139));
            }

            req.logIn(user, function (err) {
                if (err) {
                    return next(err);
                }

                const response = errors.noError();

                const info = getUserData(user);

                info.type = user.type;

                _.extend(response, info);

                return res.send(response);
            });
        })(req, res, next);
    });

    router.get('/signout', function (req, res) {
        req.logout();
        res.redirect('/admin');
    });

    router.post('/signout', function (req, res) {
        req.logout();
        res.send(errors.noError());
    });

    router.get('/*', function (req, res) {
        res.render('index', {
            isAuthenticated: req.isAuthenticated(),
            user: req.user,
            env: process.env.NODE_ENV
        });
    });

};
