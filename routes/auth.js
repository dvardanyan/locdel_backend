var express = require('express');
var app = express();
var isProd = app.get('env') === 'production';
// env

var async = require('async'),
    uuid = require('uuid'),
    request = require('request'),
    errors = require('../public/errors.js'),
    check = require('../passport/check.js'),
    site = require('../config/site.js'),
    validator = require('validator'),
    mailer = require('../controllers/mailer.js'),
    mail = require('../controllers/mailer.js'),
    User = require('../models/user/user.js'),
    UserType = require('../models/user/user-type.js'),
    session = require('../passport/session.js'),
    UserDeviceType = require('../models/user/user-device.js'),
    OrderController = require('../controllers/order.js'),
    _ = require('underscore'),
    configs = require('../config/config'),
    stripe = require('../controllers/stripe.js'),
    fs = require('fs'),
    multer = require('multer'),
    imagesDest = isProd ? '/www/locdel/images' : 'public/images',
    path = require('path'),
    errors = require('../public/errors.js'),

    twilio = configs.credentials.twilio;
const client = require('twilio')(
    twilio.accountSId,
    twilio.authToken
);
const crypto = require('crypto');

if (!fs.existsSync(imagesDest)) {
    fs.mkdirSync(imagesDest);
}

var imageUploader = multer({
    dest: imagesDest
});
var setUserData = function (model, userData) {

    function setProp(prop) {
        if (userData[prop]) {
            model[prop] = userData[prop];
        }
    }

    ['verifyEmailToken','checked','profileImage','firstName', 'lastName', 'billingAddress', 'city',
        'state', 'zipCode', 'contactNumber', 'car',  'ssn', 'email'].forEach(function (prop) {
        setProp(prop);
    });
    if (model.type === UserType.DRIVER) {
        setProp('car');
    }
};

var beginVerify = function (user,req) {
    var mailer = mail.create();
    var link = 'https://' + req.headers.host + '/api/emailVerify/' + user._id + '/' + user.verifyEmailToken ;
    var tag = `<a href="${link}">Verify</a>`;

    var mailOptions = {
        to: user.email,
        from: site.email,
        subject: 'Locdel Profile Verification',
        text: 'You are receiving this because you (or someone else) have requested to verify your account.\n\n' +
        'Please click on the following link:\n\n' +
        tag + '\n\n' +
        'If you did not request this, please ignore this email.\n',
        html: 'You are receiving this because you (or someone else) have requested to verify your account.<br/>' +
        'Please click on the following link: ' +
        tag + '<br/>' +
        'If you did not request this, please ignore this email.<br/>'
    };
    mailer.sendMail(mailOptions);
};


var getUserData = function (model) {
    var result = {};

    ['profileImage','email','firstName','lastName','contactNumber','billingAddress', 'city',
        'state', 'zipCode',  'dateOfBirth', 'ssn','isNewUser'].forEach(function (prop) {
                result[prop] = model[prop];
    });
        result.cardInfo = model.cardInfo;
    if (model.type === UserType.DRIVER) {
        result.car = model.car;
        result.type = model.type;
    }

    return {data: result};
};

module.exports = function (router, passport) {

    router.post('/sendSmsToken', function (req, res) {
        if (!req.body.contactNumber) {
            return res.send(errors.get(100));
        }
        var smsToken = Math.floor(Math.random()*9000) + 1000;

        User.findOne({contactNumber: req.body.contactNumber, type: 1}, function (err, user) {
                        // In case of any error return
                        if (err) {
                            return res.send(err);
                        }
                        if (!user) {
                            var user = new User();
                        } else {
                            if(user.checked) {
                                if (user.blocked) {
                                    return res.send(errors.get(213));
                                }
                                var generatedEmail = req.body.contactNumber;
                                generatedEmail = generatedEmail.substr(1); // remove '+' sign
                                generatedEmail += "@locdel.com"

                                // return duplicate error if user not generated
                                if (user.email !==  generatedEmail) {
                                    return res.send(errors.get(230));
                                }

                            }
                        }
                            user.contactNumber = req.body.contactNumber;
                            user.verifyUserToken = smsToken;
                            user.checked = false;
                            user.save(function (err) {
                                if (err) {
                                    return res.send( errors.get(104, err.errors));
                                }

                               // var response = errors.noError(null,{verifyUserToken: user.verifyUserToken});


                                 var response = errors.noError(null,{verifyUserToken: user.verifyUserToken});
                                client.messages.create(
                                     {
                                         body: 'Locdel: ' + smsToken + ' your code',
                                         to: req.body.contactNumber,
                                         from: twilio.number,
                                     }, function (err, message) {
                                         if (err) {
                                            return res.send(err);
                                        }
                                        return res.send(response);
                                 });


                            });

                    });

    });

    router.post('/sendSmsTokenDriver', function (req, res) {
        if (!req.body.contactNumber) {
            return res.send(errors.get(217));
        }
        var smsToken = Math.floor(Math.random()*9000) + 1000;

        User.findOne({contactNumber: req.body.contactNumber, type: 2}, function (err, user) {
            // In case of any error return
            if (err) {
                return res.send(err);
            }
            if (!user) {
                return res.send(errors.get(106));
            }

            user.contactNumber = req.body.contactNumber;
            user.verifyUserToken = smsToken;
            user.checked = false;
            user.save(function (err) {
                if (err) {
                    return res.send( errors.get(104, err.errors));
                }

                // var response = errors.noError(null,{verifyUserToken: user.verifyUserToken});


                var response = errors.noError(null,{verifyUserToken: user.verifyUserToken});
                client.messages.create(
                    {
                        body: 'Locdel: ' + smsToken + ' your code',
                        to: req.body.contactNumber,
                        from: twilio.number,
                    }, function (err, message) {
                        if (err) {
                            return res.send(err);
                        }
                        return res.send(response);
                    });


            });

        });

    });





    router.post('/login', function (req, res, next) {
        passport.authenticate('login', function (err, user, info) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return res.send(info);
            }

            req.logIn(user, function (err) {
                if (err) {
                    return next(err);
                }

                var response = errors.noError();

                var info = getUserData(user);


                _.extend(response, info);

                if (user.type === UserType.DRIVER) {
                    return OrderController.pendingJobs(user, function (err, orders) {
                        // be tolerance to orders bug
                        if (!err) {
                            response.pendingJobs = orders;
                        }

                        return res.send(response);
                    })
                }

                return res.send(response);
            });


        })(req, res, next);
    });

    /* Handle Registration POST */
    router.post('/signup', function (req, res, next) {
        passport.authenticate('signup', function (err, user, info) {
            if (err) {
                return next(err);
            }

            if (!user) {
                return res.send(info);
            }
            if (user.verifyUserToken) {
                return res.send(errors.noError(null, {verifyUserToken: user.verifyUserToken}));
            }else if (user.type === UserType.CUSTOMER) {

                req.logIn(user, function (err) {
                    if (err) {
                        return next(err);
                    }

                    if(user.epGeneragted){
                        User.find({_id: user._id}, function (err, u) {
                            if(err){
                                return next(err);
                            }
                            u.email = undefined;
                            u.password = undefined;
                            u.epGeneragted = false;
                            u.save(function (err) {
                                if(err){
                                    return next(err);
                                }

                            })
                        })
                    }
                    var response = errors.noError();

                    var info = getUserData(user);

                    info.type = user.type;

                    _.extend(response, info);

                    return res.send(response);
                });
            }

        })(req, res, next)
    });



    router.get('/signout', function (req, res) {
        req.logout();
        res.redirect('/');
    });

    router.post('/signout', function (req, res) {
        req.logout();
        res.send(errors.noError());
    });
    router.get('/verify/:type/:number/:token', function (req, res) {
        User.findOne({
            contactNumber: req.params.number,
            verifyUserToken: req.params.token,
            checked: false,
            type: req.params.type
        }, function (err, user) {
            if (!user) {
                return res.send(errors.get(112));
            }

            user.verifyUserToken = undefined;
            user.checked = true;

            user.save(function (err) {
                if (err) {
                    return res.send(errors.get(106, [err]))
                }
                req.logIn(user, function (err) {
                    if (err) {
                        return next(err);
                    }

                    var response = errors.noError();

                    var info = getUserData(user);

                    info.type = user.type;

                    _.extend(response, info);

                    if (user.type === UserType.DRIVER) {
                        return OrderController.pendingJobs(user, function (err, orders) {
                            // be tolerance to orders bug
                            if (!err) {
                                response.data.pendingJobs = orders;
                            }

                            return res.send(response);
                        })
                    }

                    return res.send(response);
                });

            });
        });
    });

    router.get('/emailVerify/:id/:token', function (req, res) {
        User.findOne({
            _id: req.params.id,
            verifyEmailToken: req.params.token,
            emailChecked: false
        }, function (err, user) {
            if (!user) {
                return res.send(errors.get(112));
            }

            user.verifyEmailToken = undefined;
            user.emailChecked = true;
            user.blocked = false;

            user.save(function (err) {
                if (err) {
                    return res.send(errors.get(106, [err]))
                }

                res.render('emailVerified');
                //res.redirect('https://' + req.headers.host + '/emailVerified');
            });
        });
    });

    router.post('/user/update', check.isAuthenticated, function (req, res) {
        var user = req.user;
        var verify = false;
        if(req.body.email) {
            var email = validator.normalizeEmail(req.body.email);
            req.body.email = email;
            User.find({
                email: email,
                type: user.type
            }, function (err, users) {
                var mastupdate = false;
                if (users.length <= 1) {
                    if (users.length == 1) {
                        if (users[0].id != user.id) {
                            return res.send(errors.get(226));
                        }
                    }
                    if (!user) {
                        return res.send(errors.get(112));
                    }
                    if (user.email != email) {
                        verify = true;
                    }
                    setUserData(user, req.body);
                    if (verify)
                      {  
                         user.emailChecked = false;
                         user.verifyEmailToken = uuid.v4();
                      }
                        //lianna must be false
                    // save the user
                  

                    user.save(function (err) {
                        if (err) {
                            return res.send(errors.get(147, err.errors));
                        }
                        if (verify) {
                           // user.verifyEmailToken = uuid.v4();
                            beginVerify(user, req);
                           // user.emailChecked = false;////lianna must be false
                        }
                        return res.send(errors.noError());
                    });
                }
                else
                    return res.send(errors.get(226));

            });
        }
        else
        {
            if (!user) {
                return res.send(errors.get(112));
            }
            setUserData(user, req.body);

            // save the user
            user.save(function (err) {
                if (err) {
                    return res.send(errors.get(147, err.errors));
                }
                return res.send(errors.noError());
            });
        }

    });

    router.post('/user/info', check.isAuthenticated, function (req, res) {
        var user = req.user;

        res.send(errors.noError(getUserData(user)));
    });

    router.post('/forgot', function (req, res, next) {
        async.waterfall([
            function (done) {
                var token = uuid.v4();
                done(null, token);
            },
            function (token, done) {
                var email = validator.normalizeEmail(req.body.email);

                if (!email) {
                    return res.send(errors.get(107));
                }

                User.findOne({email: email, type: req.body.usertype}, function (err, user) {
                    if (!user) {
                        return res.send(errors.get(107));
                    }
                     if (!user.emailChecked) {
                        return res.send(errors.get(228));
                    }

                    user.resetPasswordToken = token;
                    user.resetPasswordExpires = Date.now() + 3600000; // 1 hour

                    user.save(function (err) {
                        if (err) {
                            res.send(errors.get(108, [err]));
                        }

                        done(err, token, user);
                    });
                });
            },
            function (token, user, done) {
                function makeResetUrl(token, username) {
                    var content = site.base + '/reset?token=' +
                        encodeURIComponent(token) +
                        '&email=' +
                        encodeURIComponent(username);
                    return content;
                }

                var url = makeResetUrl(token, user.email);

                if (!url) {
                    res.send(errors.get(117));

                    return done('Can\t create url');
                }

                var content = '<a href="' + url + '">Reset Password</a>';

                var mailOptions = {
                    to: user.email,
                    from: site.email,
                    subject: 'Locdel Password Reset',
                    html: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
                    'Please click on the following link\n\n' +
                    content + '\n\n' +
                    'If you did not request this, please ignore this email and your password will remain unchanged.\n'
                };

                mailer.create().sendMail(mailOptions, function (err) {
                    if (err) {
                        res.send(errors.get(110, [err]));
                    }
                    else {
                        res.send(errors.noError('You will receive email with reset password link.'));
                    }
                    done(err);
                });
            }
        ], function (err) {
            if (err) return next(err);
        });
    });

    router.get('/reset', function (req, res) {
        var email = validator.normalizeEmail(req.query.email);

        if (!email) {
            return res.send(errors.get(111));
        }

        User.findOne({
            resetPasswordToken: req.query.token,
            resetPasswordExpires: {$gt: Date.now()},
            email: email
        }, function (err, user) {
            if (!user) {
                return res.send(errors.get(111));
            }

            res.render('reset', {
                token: user.resetPasswordToken,
                username: user.email
            });
        });
    });

    router.post('/reset', function (req, res) {
        var email = validator.normalizeEmail(req.body.email);

        if (!email) {
            return res.send(errors.get(111));
        }

        User.findOne({
            resetPasswordToken: req.body.token,
            resetPasswordExpires: {$gt: Date.now()},
            email: email
        }, function (err, user) {
            if (!user) {
                return res.send(errors.get(111));
            }

            var isOk = user.createPasswordHash(req.body.password);

            if (!isOk) {
                return res.send(errors.get(103));
            }

            user.resetPasswordToken = undefined;
            user.resetPasswordExpires = undefined;

            user.save(function (err) {
                if (err) {
                    return res.send(errors.get(108, [err]));
                }

                res.send(errors.noError('Password successfully reset'))
            });
        });
    });
    router.post('/updateDataDuringRegistration', check.isAuthenticated, function (req, res) {
      
        
         var user = req.user;
        var verify = false;
        if(req.body.email && req.body.password) {
            var email = validator.normalizeEmail(req.body.email);
            req.body.email = email;
            User.find({
                email: email,
                type: user.type
            }, function (err, users) {
                var mastupdate = false;
                if (users.length <= 1) {
                    if (users.length == 1) {
                        if (users[0].id != user.id) {
                            return res.send(errors.get(226));
                        }
                    }
                    if (!user) {
                        return res.send(errors.get(112));
                    }
                    if (user.email != email) {
                        verify = true;
                    }
                    isOk = user.createPasswordHash(req.body.password);

                    if (!isOk) {
                     return res.send(errors.get(103));
                    }
                    
                    setUserData(user, req.body);
                    if (verify){
                        user.verifyEmailToken = uuid.v4();
                        user.emailChecked = false;//lianna must be false
                    }
                    // save the user
                  

                    user.save(function (err) {
                        if (err) {
                            return res.send(errors.get(147, err.errors));
                        }
                        if (verify) {
                            beginVerify(user, req);
                          //  user.emailChecked = false;////lianna must be false
                        }
                        return res.send(errors.noError());
                    });
                }
                else
                    return res.send(errors.get(226));

            });
        }
        else
        {
            
                return res.send(errors.get(226));
            
        }
    });
    router.post('/new-password', check.isAuthenticated, function (req, res) {
        var user = req.user;
       
        if (!user) {
            return res.send(errors.get(111));
        }

        var isOk = user.authenticate(req.body.old);

        if (!isOk) {
            return res.send(errors.get(173));
        }

        isOk = user.createPasswordHash(req.body.new);

        if (!isOk) {
            return res.send(errors.get(103));
        }

        user.save(function (err) {
            if (err) {
                return res.send(errors.get(174, [err]));
            }

            // after logoutAllSessions the cookie response flow will refresh the current session, so this need remove independent
            req.logout();
            // log out all other sessions
            session.logoutAllSessions(user._id, function () {
                // be tolerance for any kind of error
                res.send(errors.noError('Password successfully changed'))
            });

        });
    });



    router.post('/user/device', check.isAuthenticated, function (req, res) {
        var d = req.body.token,
            os = req.body.os || UserDeviceType.IOS,
            user = req.user;

        if (!d) {
            return res.send(errors.get(163));
        }
        var exits = false;


        User.find({
            'devices.token': d
        }, 'devices', function (err, users) {
            if (!err && users) {
                users.forEach(function (u) {
                    var c = false;
                    if (!u._id.equals(user._id)) {
                        u.devices.forEach(function (device) {
                            if (device.token === d && device.active) {
                                device.active = false;
                                c = true;
                            }
                        });
                        if (c) {
                            u.save();
                        }
                    }
                })
            }

            user.devices.forEach(function (device) {
                if (device.token === d) {
                    device.active = true;
                    exits = true;
                }
                else {
                    device.active = false;
                }
            });

            if (!exits) {
                user.devices.push({
                    active: true,
                    type: os,
                    token: d
                })
            }

            user.save(function (err) {
                if (err) {
                    return res.send(errors.get(164, [err]));
                }

                res.send(errors.noError());
            })

        });

    })
    router.post('/setUserProfileImage', imageUploader.single('photo'), check.isAuthenticated, function (req, res) {
        var user = req.user;
        var newName = uuid.v4().replace(/-/g, ''),
            file = req.file;

        if (!file) {
            return res.send(errors.get(172));
        }

        var filePath = file.path,
            newFullName = newName + path.extname(file.originalname);

        fs.rename(filePath, file.destination + '/' + newFullName, function (err) {
            var name = err ? file.filename : newFullName;
            if (fs.existsSync(req.user.profileImage)) {
                fs.unlink(req.user.profileImage);
            }
            user.profileImage = name;
            user.save(function (err) {
                if (err) {
                    return res.send(errors.get(147, err.errors));
                }

                return res.send(errors.noError(null,{
                    'url': 'https://' + req.headers.host + '/images/' + name
                }))
            });

        });
    });

    router.get('/getCustomerUrl', check.isAuthenticated, function (req, res) {
        return res.send(errors.noError(null,{
            'urliOS': 'https://itunes.apple.com/us/app/locdel/id1378044580',
            'urlAndroid': 'https://itunes.apple.com/am/app/facebook/id284882215?mt=8'
        }));
    });
    router.get('/getDriverUrl', check.isAuthenticated,  function (req, res) {
        return res.send(errors.noError(null,{
            'urliOS': 'https://itunes.apple.com/us/app/locdel-driver/id1438271849',
            'urlAndroid': 'https://itunes.apple.com/am/app/facebook/id284882215?mt=8'
        }));
    });

};