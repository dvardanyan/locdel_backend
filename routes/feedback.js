var User = require('../models/user/user.js'),
    UserType = require('../models/user/user-type.js'),
    check = require('../passport/check.js'),
    errors = require('../public/errors.js'),
    Order = require('../models/order/order.js'),
    mongoose = require('mongoose');

var Filter = require('bad-words'),
    filter = new Filter();

module.exports = function (router) {

    router.post('/feedback/set', check.isAuthenticated, function (req, res) {
        var orderId = req.body.orderId,
            mark = req.body.mark,
            comment = req.body.comment && filter.clean(req.body.comment),
            user = req.user;

        if (!orderId) {
            return res.send(errors.get(122));
        }


        if (!mark) {
            return res.send(errors.get(155));
        }

        var isDriver = user.type === UserType.DRIVER,
            userProp = isDriver ? 'user' : 'driver';

        Order
            .findOne({
                _id: orderId
            })
            .populate(userProp, 'feedback type')
            .exec(function (err, order) {
                if (err || !order) {
                    return res.send(errors.get(165));
                }

                var orderFeedback = {
                    mark: mark,
                    comment: comment
                };

                var target = order[userProp],
                    prop = isDriver ? 'driverFeedback' : 'customerFeedback';

                if (!target) {
                    return res.send(errors.get(166));
                }

                if (order[prop].mark) {
                    return res.send(errors.get(167));
                }

                var feedback = target.feedback;

                // set user average feedback
                feedback.average = feedback.average || 0;
                feedback.count = feedback.count || 0;
                feedback.average = (feedback.average * feedback.count + mark) / (feedback.count + 1);
                feedback.count = feedback.count + 1;

                // set user average from certain user
                var exists = feedback.list.some(function (f) {
                    if (user._id.equals(f.from)) {
                        var count = f.count || 0,
                            average = f.average || 0;
                        f.average = (count * average + mark) / (count + 1);
                        f.count = count + 1;

                        return true;
                    }
                });

                if (!exists) {
                    feedback.list.push({
                        count: 1,
                        average: mark,
                        from: user._id
                    })
                }

        order[prop] = orderFeedback

        // save order
        order.save(function (err) {
            if (err) {
                return res.send(errors.get(159, [err]));
            }
            // if no errors happened, then save user/driver(targer is either user or driver)
            target.feedback = feedback
            target.save(function (err) {
                if (err) {
                    return res.send(errors.get(159, [err]));
                }
            })


            res.send(errors.noError());
        })

    });

    });

    router.post('/feedback/average', check.isAuthenticated, function (req, res) {
        var driverId = req.body.driver;

        if (!driverId) {
            return res.send(errors.get(154));
        }

        User.findOne({
            _id: driverId
        }, 'feedback.average type', function (err, user) {
            if (err || !user) {
                return res.send(errors.get(155));
            }

            if (user.type !== UserType.DRIVER) {
                return res.send(156);
            }

            var r = errors.noError();

            r.feedback = user.feedback ? user.feedback.average : 0;
            res.send(r);
        })
    });


    router.post('/feedback/from', check.isAuthenticated, function (req, res) {
        var driverId = req.body.driver;

        if (!driverId) {
            return res.send(errors.get(154));
        }

        User.aggregate([
                {
                    $unwind: '$feedback.list'
                },
                {$project: {"from": "$feedback.list.from", "mark": "$feedback.list.mark"}},
                {
                    $group: {_id: '$from', average: {$avg: "$mark"}}
                }
            ],
            function (err, user) {
                if (err || !user) {
                    return res.send(errors.get(158));
                }

                var r = errors.noError();

                function average(list) {
                    return list.reduce(function (sum, e) {
                            e.mark += sum;
                        }, 0) / list.length;
                }

                r.feedback = user.feedback ? average(user.feedback.list) : 0;
                res.send(r);
            })
    })

};