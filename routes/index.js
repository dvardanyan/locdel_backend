var express = require('express');

module.exports = function (passport) {
    var apiRouter =  express.Router();
    var adminRouter =  express.Router();
    var siteRouter = express.Router();

    // admin
    require('./admin.js')(adminRouter, passport);
    require('./auth.js')(adminRouter, passport);

    // api
    require('./auth.js')(apiRouter, passport);
    require('./order.js')(apiRouter);
    require('./user.js')(apiRouter);
    require('./payment.js')(apiRouter);
    require('./feedback.js')(apiRouter);
    require('./schedule.js')(apiRouter);
    require('./notification.js')(apiRouter);

    // site
    require('./site.js')(siteRouter, passport);
    //require('./auth.js')(siteRouter, passport);

    return {
        api: apiRouter,
        admin: adminRouter,
        site: siteRouter
    };
};