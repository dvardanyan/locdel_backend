var check = require('../passport/check.js'),
    errors = require('../public/errors.js'),
    _ = require('underscore'),
    NotificationManager = require('../controllers/data-manager').NotificationManager,
    Notification = require('../models/order/notification.js'),
    NotificationStatus = require('../models/order/notification-status.js');


module.exports = function (router) {
    router.post('/notification/read', check.isAuthenticated,function (req, res) {
        var notId = req.body.notificationId;

        if (!notId) {
            return res.send(errors.get(207));
        }

        Notification.update({
            '_id': notId,
            user: req.user._id
        }, {$set: {status: NotificationStatus.READ}}, function (err, modified) {
            if (err || modified.nModified !== 1) {
                return res.send(errors.get(208, [err]));
            }

            res.send(errors.noError());
        });
    });

    router.post('/notification/unread', check.isAuthenticated,  function (req, res) {
        var notId = req.body.notificationId;

        if (!notId) {
            return res.send(errors.get(207));
        }

        Notification.update({
            '_id': notId,
            user: req.user._id
        }, {$set: {status: NotificationStatus.UNREAD}}, function (err, modified) {
            if (err || modified.nModified !== 1) {
                return res.send(errors.get(208, [err]));
            }

            res.send(errors.noError());
        });
    });

    router.post('/notification/delete', check.isAuthenticated, function (req, res) {
        var notId = req.body.notificationId;

        if (!notId) {
            return res.send(errors.get(207));
        }

        Notification.update({
            '_id': notId,
            user: req.user._id
        }, {$set: {status: NotificationStatus.DELETED}}, function (err, modified) {
            if (err || modified.nModified !== 1) {
                return res.send(errors.get(208, [err]));
            }

            res.send(errors.noError());
        });
    });

    router.post('/notifications', check.isAuthenticated, function (req, res) {
        var p = req.body;

        p = _.extend(p, req.query);
        p.user = req.user._id;
        var m = new NotificationManager(p);

        m.call(function (err, data) {
            if (err) {
                return res.send(errors.get(209, [err]));
            }

            res.send(errors.noError(data));
        });
    });
};
