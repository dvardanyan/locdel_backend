"use strict";

const UserType = require('../models/user/user-type.js'),
    OrderStatus = require('../models/order/order-status.js'),
    OrderCancelReason = require('../models/order/order-cancel-reason.js'),
    OrderFilters = require('../models/order/order-filters.js'),
    OrderType = require('../models/order/order-type.js'),
    OrderStepType = require('../models/order/order-step-type.js'),
    OrderController = require('../controllers/order.js'),
    check = require('../passport/check.js'),
    errors = require('../public/errors.js'),
    Order = require('../models/order/order.js'),
    Advertisement = require('../models/advertisement/advertisement.js'),
    Step = require('../models/order/step.js'),
    Tracking = require('../models/order/tracking.js'),
    moment = require('moment'),
    utils = require('../util/core-util.js'),
    geo = require('../controllers/geo.js'),
    geocoder = require('../controllers/geocoder.js'),
    direction = require('../controllers/direction.js'),
    geoUtils = require('../util/geo.js'),
    notification = require('../controllers/notification.js'),
    payment = require('../controllers/payment.js'),
    UserController = require('../controllers/user.js'),
    User = require('../models/user/user.js'),
    TransportType = require('../models/user/user-transport-type.js'),
    _ = require('underscore');


const parseOrder = OrderController.parse;
const pushHistory = OrderController.pushHistory;
const getPendingFilter = OrderController.pendingFilters;

function checkDate(date, filter) {
    if (!filter || (!filter.$gte && !filter.$le)) {
        return true;
    }

    if (date < filter.$gte) {
        return false;
    }

    if (date >= filter.$lt) {
        return false
    }

    return true;
}

function isToday(date) {
    const m = moment(date);
    const TODAY = moment().startOf('day');
    return m.isSame(TODAY, 'd');
}

module.exports = function (router) {
    router.post('/order/get', check.isAuthenticated, function (req, res) {
        var user = req.user,
            orderId = req.body.orderId;

        if (!orderId) {
            return res.send(errors.get(122));
        }

        Order
            .findOne({
                _id: orderId,
                $or: [{user: req.user._id}, {driver: req.user._id}]
            })
            .populate('user driver')
            .exec(function (err, order) {
                if (err || !order) {
                    return res.send(errors.get(165));
                }

                if (order.driver) {
                    var feedback = order.driver.feedback.average;
                    var photo = order.driver.car.images.driverPhoto;
                }

                var parsed = parseOrder(order.toObject());

                if (order.driver) {
                    parsed.driver.feedback = feedback;
                    parsed.driver.photo = photo;

                }

                return res.send(errors.noError(null, parsed));
            })
    });




    router.post('/order/create', check.isAuthenticated, function (req, res) {
        const body = req.body;

        if (!body.startPoint) {
            return res.send(errors.get(168));
        }

        const location = [body.startPoint.long, body.startPoint.lat];
        const source = req.body;

        // avoid payment hack
        delete source.status;

        OrderController.create(req.user, source, location, function (data) {
            res.send(data);
        },true);
    });

    router.post('/orders/pending', check.isAuthenticated, check.isDriver, function (req, res) {


        const user = req.user;
        const carType = user.car && user.car.type;

        if (!carType) {
            return res.send(errors.get(118));
        }

        const location = req.body.location;
        var number = 10;
        if(req.body.ordersCount)
        {
            number=req.body.ordersCount;
        }

        if (!location) {
            return res.send(errors.get(169));
        }

        const geoPoint = [location.long, location.lat];


        OrderController.nearestOrders(geoPoint, {
            /*filter: {
                'filters.transportType': {$in: [carType, OrderFilters.TransportType.All]},
                status: OrderStatus.PENDING
            }
            */
            filter: {
                'filters.transportType': {$bitsAllSet: carType},
                status: OrderStatus.PENDING
            },
            number: number
        }, function (err, result) {
            if (err) {
                return res.send(errors.get(120, [err]));
            }

            if(result.length == 0)
            {
                res.send({
                    status: 'OK',
                    orders: result
                });
            }

            var i = 0;
            result.forEach(function (item, index) {

                User.findOne({'_id': item.order.user}, function (err, customer) {

                    item.order.user = customer;

                    var o = parseOrder(item.order);

                    o.startDistance = item.startDistance;

                    result[index] = o;
                    i++;
                    if (i === result.length) {
                        res.send({
                            status: 'OK',
                            orders: result
                        });
                    }


                });


            });

        });





     /*
     // !!!!!!!!~~~~~~~~~Remove this temp code after Filtering issue solved~~~~~~~~~~~~~!!!!!!!!!!!!!!
      /*  var filter = {status: OrderStatus.PENDING};
        const count = 10
        Order
            .find(filter)
            .limit(count)
            .sort({ "created":-1})
            .exec(function (err, orders) {
                if (err) {
                    return res.send(errors.get(120, [err]));
                }


                var result = {},
                    sendOrders = req.body.orders;

                result.count = orders.length;
                if (sendOrders) {
                    result.orders = orders.map(function (order) {
                        return parseOrder(order.toObject());
                    });
                }

                return res.send(errors.noError(null, result));
            });
            */
        // !!!!!!!!~~~~~~~~~Remove this temp code after Filtering issue solved~~~~~~~~~~~~~!!!!!!!!!!!!!!




    });

    router.post('/order/pick', check.isAuthenticated, function (req, res) {
        var user = req.user,
            orderId = req.body.orderId;

        if (user.type !== UserType.DRIVER){
            return res.send(errors.get(119));
        }
        if (!orderId) {
            return res.send(errors.get(122));
        }


        function pickOrder(order) {
            var os = order.status;

            if(user.car.type === TransportType.Motorcycle && order.motoCost)
              {
               order.cost = order.motoCost;
              } else if (user.car.type === TransportType.Car && order.carCost)
              {
               order.cost = order.carCost;

              }else if (user.car.type === TransportType.PickUp && order.pickUpCost)
              {
               order.cost = order.pickUpCost;

              }else {
              return res.send(errors.get(227));
              }



            order.status = OrderStatus.PICKED;
            order.driver = user;

            pushHistory(order);
            order.save(function (err) {
                if (err) {
                    return res.send(errors.get(123, [err]));
                }


                if (os === OrderStatus.PENDING) {
                    notification.send(order.user, 1, {
                        order: order._id,
                        invoiceNumber: order.invoiceNumber,
                        user: {
                            firstName: user.firstName || "",
                            lastName: user.lastName || "",
                            id: user._id,
                            type: user.type
                        }
                    });
                }

                res.send({
                    'status': 'OK',
                    'message': 'Order successfully picked',
                    'order': parseOrder(order.toObject())
                });

            });
        }

        Order
            .findOne({
                _id: orderId
            })
            .populate('user driver')
            .exec(function (err, order) {
                if (err || !order) {
                    return res.send(errors.get(123, [err]));
                }
                switch (order.status) {
                    case OrderStatus.INITIAL:
                        return res.send(errors.get(175));
                    case OrderStatus.CANCELED:
                        return res.send(errors.get(125));
                    case OrderStatus.COMPLETED:
                        return res.send(errors.get(126));
                    case OrderStatus.PICKED:
                        return res.send(errors.get(124));
                    case OrderStatus.FAIL:
                        return res.send(errors.get(152));
                    case OrderStatus.PENDING:
                        pickOrder(order);
                        break;
                    case OrderStatus.HOLD:
                        pickOrder(order);
                        break;
                    default :
                        return res.send(errors.get(123));
                }
            })
    });


    router.post('/order/complete', check.isAuthenticated, function (req, res) {
        var user = req.user,
            orderId = req.body.orderId;

        if (!orderId) {
            return res.send(errors.get(122));
        }

        Order.findOne({
            _id: orderId
        }).populate('user').exec(function (err, order) {
            if (err || !order) {
                return res.send(errors.get(130, [err]));
            }

            if (!user._id.equals(order.driver)) {
                return res.send(errors.get(128));
            }

            function complete() {
                order.status = OrderStatus.COMPLETED;

                pushHistory(order);
                order.save(function (err) {
                    if (err) {
                        return res.send(errors.get(130, [err]));
                    }

                    Tracking.remove({
                        order: order._id
                    }, function (err) {
                        if (err) {
                            return res.send(errors.get(130, [err]));
                        }

                        notification.send(order.user, 6, {
                            order: order._id,
                            cost: order.calculateCost(),
                            invoiceNumber: order.invoiceNumber,
                            user: {
                                firstName: user.firstName || "",
                                lastName: user.lastName || "",
                                id: user._id,
                                type: UserType.DRIVER
                            }
                        });

                        res.send(errors.noError());
                    });
                });
            }

            switch (order.status) {
                case OrderStatus.INITIAL:
                    return res.send(errors.get(175));
                case OrderStatus.CANCELED:
                    return res.send(errors.get(125));
                case OrderStatus.COMPLETED:
                    return res.send(errors.get(126));
                case OrderStatus.PENDING:
                    return res.send(errors.get(129));
                case OrderStatus.HOLD:
                    complete();
                    break;
                case OrderStatus.FAIL:
                    return res.send(errors.get(152));
                case OrderStatus.PICKED:
                    complete();
                    break;
                default :
                    return res.send(errors.get(130));
            }
        })
    });

    router.post('/order/tip', check.isAuthenticated, check.isCustomer, function (req, res) {
        var tip = req.body.tip,
            orderId = req.body.orderId;

        if (!orderId) {
            return res.send(errors.get(122));
        }

        if (!tip || !(tip > 0)) {
            return res.send(errors.get(210));
        }

        Order.update({
            '_id': orderId,
            user: req.user._id,
            tip: {$exists: false},
            paymentProceed: {$ne: true}
        }, {$set: {tip: tip}}, function (err, modified) {
            if (err || modified.nModified !== 1) {
                return res.send(errors.get(211, [err]));
            }

            res.send(errors.noError());
        });
    });

    router.post('/order/hold', check.isAuthenticated, function (req, res) {
        const orderId = req.body.orderId;

        if (!orderId) {
            return res.send(errors.get(122));
        }

        Order
            .findOne({
                _id: orderId
            })
            .populate('user driver')
            .exec(function (err, order) {
            if (err || !order) {
                return res.send(errors.get(153, [err]));
            }
            function hold() {
                order.status = OrderStatus.HOLD;
                pushHistory(order);
                order.save(function (err) {
                    if (err) {
                        return res.send(errors.get(153, [err]));
                    }

                    // do not send push notification in case of hold
                    // notification.send(order.user, 5, {
                    //     order: order._id,
                    //     invoiceNumber: order.invoiceNumber
                    // });

                    res.send(errors.noError());

                });
            }

            switch (order.status) {
                case OrderStatus.INITIAL:
                    return res.send(errors.get(175));
                case OrderStatus.CANCELED:
                    return res.send(errors.get(125));
                case OrderStatus.COMPLETED:
                    return res.send(errors.get(126));
                case OrderStatus.HOLD:
                    return res.send(errors.get(151));
                case OrderStatus.FAIL:
                    return res.send(errors.get(152));
                case OrderStatus.PENDING:
                    return res.send(errors.get(129));
                    break;
                case OrderStatus.PICKED:
                    hold();
                    break;
                default :
                    return res.send(errors.get(153));
            }
        });
    });

    router.post('/order/cancel', check.isAuthenticated, function (req, res) {
        var user = req.user,
            orderId = req.body.orderId,
            reason = req.body.reason,
            comment = req.body.comment;

        if (!orderId) {
            return res.send(errors.get(122));
        }

        Order
            .findOne({
                _id: orderId
            })
            .populate('user driver')
            .exec(function (err, order) {
                if (err || !order) {
                    return res.send(errors.get(131, [err]));
                }

                if (!user._id.equals(order.user._id) && !user._id.equals(order.driver && order.driver._id)) {
                    return res.send(errors.get(128));
                }

                const preStatus = order.status;
                const isCustomer = user.type === UserType.CUSTOMER;
                const isDriver = user.type === UserType.DRIVER;

                if (isDriver && !utils.getEnumKey(OrderCancelReason, reason)) {
                    return res.send(errors.get(175));
                }


                function sendPush() {
                    if (isDriver) {
                        if (reason == OrderCancelReason.CRASH || reason == OrderCancelReason.VEHICLE_BROKE) {
                            notification.send(order.user, 2, {
                                order: order._id,
                                invoiceNumber: order.invoiceNumber,
                                user: {
                                    id: user._id,
                                    type: user.type,
                                    firstName: user.firstName || "",
                                    lastName: user.lastName || ""
                                },
                                reason: reason
                            });
                        } else {
                            // check for unexpected user configs
                            if (preStatus === OrderStatus.PICKED || preStatus === OrderStatus.HOLD) {
                                notification.send(order.user, 2, {
                                    order: order._id,
                                    invoiceNumber: order.invoiceNumber,
                                    user: {
                                        id: order.driver._id,
                                        type: order.driver.type,
                                        firstName: order.driver.firstName || "",
                                        lastName: order.driver.lastName || ""
                                    },
                                    reason: reason
                                });
                            }
                        }
                    }

                    if (isCustomer && (preStatus === OrderStatus.PICKED || preStatus === OrderStatus.HOLD)) {
                        notification.send(order.driver, 2, {
                            order: order._id,
                            invoiceNumber: order.invoiceNumber,
                            user: {
                                id: order.user._id,
                                type: order.user.type,
                                firstName: order.user.firstName || "",
                                lastName: order.user.lastName || ""
                            }
                        });
                    }
                }

                function cancel() {
                    if (isDriver && reason === OrderCancelReason.CRASH || reason === OrderCancelReason.VEHICLE_BROKE) {
                        return OrderController.crash(order, function (err) {
                            var response = err || errors.noError();

                            // send push to customer if new order created successfully
                            if (!err) {
                                sendPush();
                            }

                            res.send(response);
                        });
                    }

                    payment.cancel(user, order, reason, function (err) {
                        if (err) {
                            return res.send(err)
                        }

                        order.status = OrderStatus.CANCELED;
                        order.cancelStatus = isDriver ? OrderCancelReason.CUSTOMER : reason;
                        order.cancelComment = comment;

                        pushHistory(order);
                        order.save(function (err) {
                            if (err) {
                                return res.send(errors.get(131, [err]));
                            }


                            sendPush();

                            res.send(errors.noError('Order canceled'));

                        });

                    });
                }

                switch (order.status) {
                    case OrderStatus.INITIAL:
                        return res.send(errors.get(175));
                    case OrderStatus.CANCELED:
                        return res.send(errors.get(125));
                    case OrderStatus.COMPLETED:
                        return res.send(errors.get(126));
                    case OrderStatus.HOLD:
                        cancel();
                        break;
                    case OrderStatus.FAIL:
                        return res.send(errors.get(152));
                    case OrderStatus.PENDING:
                        cancel();
                        break;
                    case OrderStatus.PICKED:
                        cancel();
                        break;
                    default :
                        return res.send(errors.get(131));
                }
            })
    });

    router.post('/order/tracking/update', check.isAuthenticated, function (req, res) {
        var orderId = req.body.orderId;

        if (!orderId) {
            return res.send(errors.get(122));
        }

        Tracking.findOne({
            order: orderId
        }, function (err, order) {
            if (err) {
                return res.send(errors.get(142, [err]));
            }

            var lat = req.body.latitude,
                long = req.body.longitude;

            if (!(lat && long)) {
                return res.send(errors.get(143));
            }


            if (!order) {
                var tracking = new Tracking({
                    order: orderId,
                    latitude: lat,
                    longitude: long
                });

                return tracking.save(function (err) {
                    if (err) {
                        return res.send(errors.get(144, [err]));
                    }

                    return res.send(errors.noError());
                })
            }

            order.latitude = lat;
            order.longitude = long;

            order.save(function (err) {
                if (err) {
                    return res.send(errors.get(145, [err]));
                }

                return res.send(errors.noError());
            });
        });
    });

    router.post('/order/tracking/get', check.isAuthenticated, function (req, res) {
        var orderId = req.body.orderId;

        if (!orderId) {
            return res.send(errors.get(122));
        }

        Tracking.findOne({
            order: orderId
        }, function (err, tracking) {
            if (err) {
                return res.send(errors.get(142, [err]));
            }


            if (!tracking) {
                return res.send(errors.get(146));
            }


            return res.send(errors.noError(null, tracking));
        });
    });

    router.post('/orders/my', check.isAuthenticated, function (req, res) {
        var user = req.user;

        var filter = {};
        var dateFilter;

        if (user.type === UserType.CUSTOMER) {
            filter.user = user._id;
        }

        if (user.type === UserType.DRIVER) {
            filter.driver = user._id;
        }


        // filter by dates
        var startDate = req.body.startDate,
            endDate = req.body.endDate,
            count = req.body.count;

        if (startDate) {
            startDate = new Date(startDate);
            if (!utils.isValidDate(startDate)) {
                return res.send(errors.get(149))
            }
            dateFilter = {
                '$gte': startDate
            }
        }

        if (endDate) {
            endDate = new Date(endDate);
            if (!utils.isValidDate(endDate)) {
                return res.send(errors.get(149))
            }
            // adding one day to include this day orders
            endDate.setDate(endDate.getDate() + 1);

            dateFilter = dateFilter || {};

            dateFilter['$lt'] = endDate;
        }

        //filter by status
        var status = req.body.status || '';
        if (!Array.isArray(status)) {
            status = [status];
        }

        var stArray = [];

        status.forEach(function (st) {
            st = OrderStatus[st.toUpperCase()];

            if (st) {
                stArray.push(st);
            }
        });

        if (stArray.length > 0) {
            filter.status = {'$in': stArray};
        } else if (dateFilter) {
            // if no status was applied
            filter.created = dateFilter;
        }

        if (_.contains(stArray, OrderStatus.PENDING) && filter.driver) {
            filter = {
                $or: [filter, getPendingFilter(user)]
            }
        }

        Order
            .find(filter)
            .limit(count)
            .sort({ "created":-1})
            .populate('driver user')
            .exec(function (err, orders) {
                if (err) {
                    return res.send(errors.get(120, [err]));
                }


                var result = {},
                    sendOrders = req.body.orders;

                if (stArray.length > 0) {
                    orders = orders.filter(o => {
                        var status = o.history.find(h => h.status === o.status);
                        return status && checkDate(status.date, dateFilter);
                    });
                }

                result.count = orders.length;
                if (sendOrders) {
                    result.orders = orders.map(function (order) {

                       return  parseOrder(order.toObject());

                    });
                }

                if (stArray.indexOf(OrderStatus.COMPLETED) > -1) {
                    result.amount = orders
                        .filter(o => o.status === OrderStatus.COMPLETED)
                        .reduce(function (result, order) {
                            const status = order.history.find(h => h.status === order.status);
                            let cost = 0;

                            if (status && isToday(status.date, dateFilter)) {
                                cost = order.calculateCost();
                            }

                            return result + cost;
                        }, 0);
                }
                return res.send(errors.noError(null, result));

            });
    });

    router.post('/order/step/photo', check.isAuthenticated, function (req, res) {
        const stepId = req.body.stepId;
        const orderId = req.body.orderId;
        var photos = req.body.photos;

        if (!orderId) {
            return res.send(errors.get(122));
        }

        if (!stepId) {
            return res.send(errors.get(176));
        }

        if (!photos) {
            return res.send(errors.get(178));
        }

        if (!Array.isArray(photos)) {
            photos = [photos];
        }

        Order.findOne({
            '_id': orderId,
            'driver': req.user._id
        }).populate('user driver').exec(function (err, order) {
            if (err || !order) {
                return res.send(errors.get(165));
            }

            if (order.status !== OrderStatus.PICKED) {
                var error = errors.get(order.status === OrderStatus.CANCELED ? 125 : 152);

                return res.send(error);
            }

            var step = order.steps.filter(function (s) {
                return s._id.equals(stepId);
            })[0];

            step.completeDate = Date.now();

            if (step.type === OrderStepType.PICK) {
                notification.send(order.user, 9, {
                    order: order._id,
                    invoiceNumber: order.invoiceNumber,
                    user: {
                        id: order.driver._id,
                        type: order.driver.type,
                        firstName: order.driver.firstName || "",
                        lastName: order.driver.lastName || ""
                    },
                    photos: photos
                });
            }

            // add photos
            photos.every(function (p) {
                step.driverImages.push(p)
            });

            order.save(function (err) {
                if (err) {
                    return res.send(errors.get(179, [err]));
                }

                res.send(errors.noError());
            });
        })
    });

    router.post('/steps/find', check.isAuthenticated, function (req, res) {
        var search = req.body.search,
            reg = new RegExp(search, 'i');

        Step.find({
            $or: [{name: reg}, {contactNumber: reg}, {address: reg}],
            user: req.user._id,
            disabled: {$ne: true}
        }, function (err, steps) {
            if (err) {
                return res.send(errors.get(171, [err]));
            }
            res.send(errors.noError(null,steps));
        })
    });

    router.post('/steps/delete', check.isAuthenticated, function (req, res) {
        var stepId = req.body.stepId;

        if (!stepId) {
            res.send(errors.get(189));
        }

        Step.update({
            _id: stepId,
            user: req.user._id
        }, {$set: {disabled: true}}, function (err) {
            if (err) {
                return res.send(errors.get(188, [err]));
            }

            res.send(errors.noError());
        });
    });

    router.all('/make-all-pending', check.isAuthenticated, function (req, res) {
        var id = req.user._id;

        Order.update({user: id}, {$set: {status: OrderStatus.PENDING}}, {multi: true}, function (err) {
            if (err) {
                return res.send("Error, can\'t change order status")
            }

            return res.send(errors.noError());
        })
    });

    router.post('/order/step/add', check.isAuthenticated, check.isDriver, function (req, res) {
        const orderId = req.body.orderId;
        var steps = req.body.steps;

        if (!orderId) {
            return res.send(errors.get(122));
        }

        if (!steps) {
            return res.send(errors.get(180));
        }

        // normalize steps
        if (!Array.isArray(steps)) {
            steps = [steps];
        }

        Order.findOne({
            '_id': orderId,
            'driver': req.user._id,
            'type': OrderType.QUICK
        }, function (err, order) {
            if (err || !order) {
                return res.send(errors.get(165));
            }

            if (order.status !== OrderStatus.PICKED) {
                var error = errors.get(order.status === OrderStatus.CANCELED ? 125 : 152);

                return res.send(error);
            }

            // add photos
            steps.every(function (s) {
                order.steps.push(s);
            });

            var length = steps.length;

            OrderController.addCostData(order, function (err) {
                if (err) {
                    return res.send(err);
                }

                order.save(function (err) {
                    if (err) {
                        var error = errors.get(181, err.errors);

                        res.send(error);
                    }

                    res.send(errors.noError(null, {
                        stepIds: order.steps.slice(-1 * length).map((step) => step._id)
                    }));
                });
            });
        })
    });
    router.post('/getPrice', check.isAuthenticated, function (req, res) {
        direction.distance(req.body.points, function (err, distance, eta) {
            if (err) {
                return res.send(err);
            }

            var distance = geoUtils.metersToMiles(distance);
            var eta = eta / 60;

            var motoCost=  OrderController.cost(distance, eta, TransportType.Motorcycle, req.body.points);
            var carCost = OrderController.cost(distance, eta, TransportType.Car, req.body.points);
            var pickUpCost = OrderController.cost(distance, eta, TransportType.PickUp, req.body.points);

            return res.send(errors.noError(null, {carCost: carCost, motoCost: motoCost, pickUpCost: pickUpCost}));
        })
    });
    router.get('/GetAdvertisements', function (req, res) {
        Advertisement
            .find({isDeleted: false },
                function (err, advertisements) {
                    if (!advertisements) {
                        return res.send(errors.get(111));
                    }
                    advertisements.forEach(function (item) {
                        var str = item.file.url;
                        var url = str.replace('/www/locdel/', 'https://www.locdel.com/');
                        item.file.url = url;
                    });
                    res.send(errors.noError(advertisements));

                });
    });


};
