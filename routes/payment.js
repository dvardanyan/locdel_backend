var paypal = require('../controllers/paypal.js'),
    stripe = require('../controllers/stripe.js'),
    errors = require('../public/errors.js'),
    Order = require('../models/order/order.js'),
    OrderStatus = require('../models/order/order-status.js'),
    OrderController = require('../controllers/order.js'),
    check = require('../passport/check.js');

module.exports = function (router) {
    router.post('/payment/code', check.isAuthenticated, function (req, res) {
        var code = req.body.code,
            type = req.body.type;

        if (!code) {
            return res.send(errors.get(132));
        }

        if (type === 'paypal') {
            return paypal.generateToken(code, req.user, function (err) {
                if (err) {
                    return res.send(errors.get(133, [err]))
                }

                res.send(errors.noError());
            });
        }

        return res.send(errors.get(134));
    });

    router.post('/payment/pay', check.isAuthenticated, function (req, res) {
        var configs = req.body.configs || {},
            type = req.body.type,
            orderId = req.body.order;

        if (!orderId) {
            return res.send(errors.get(136));
        }

        var pay = function (sdk) {
            sdk.pay(req.user, orderId, configs || {}, function (err) {
                if (err) {
                    return OrderController.fail(orderId, function () {
                        res.send(errors.get(137, [err]));
                    })
                }

                var history = {
                    status: OrderStatus.PENDING,
                    date: Date.now()
                };

                Order.update({_id: orderId}, {
                    $set: {
                        status: OrderStatus.PENDING
                    },
                    $push: {history: history}
                }, function (err, numEffected) {
                    if (err) {
                        res.send(errors.get(161));
                    }

                    res.send(errors.noError());
                });

            });
        };

        switch (type) {
            case 'paypal':
                return pay(paypal);
            case 'stripe':
                return pay(stripe);
            default:
                return res.send(errors.get(134));
        }
    });

    router.post('/payment/stripe/customer', check.isAuthenticated, function (req, res) {
        const token = req.body.token;
        const cardInfo = req.body.cardInfo;

        stripe.customer(req.user, token, cardInfo, function (err) {
            if (err) {
                return res.send(errors.get(137, [err]));
            }

            res.send(errors.noError());
        });
    });

    router.post('/payment/can', check.isAuthenticated, function (req, res) {
        const type = req.body.type;

        switch (type) {
            case 'paypal':
                return res.send(errors.get(199));
            case 'stripe':
                if (stripe.check(req.user)) {
                    return res.send(errors.noError());
                }

                return res.send(errors.get(199));
            default:
                return res.send(errors.get(134));
        }
    });
};