var check = require('../passport/check.js'),
    errors = require('../public/errors.js'),
    Scheduler = require('../controllers/scheduler.js'),
    OrderController = require('../controllers/order.js'),
    direction = require('../controllers/direction.js'),
    OrderStatus = require('../models/order/order-status.js'),
    Schedule = require('../models/order/schedule.js'),
    moment = require('moment'),
    Order = require('../models/order/order.js');

function parseScheduleDate(date) {
    if(!date.dayOfWeek || !date.once){
        return date;
    }

    var clone = JSON.parse(JSON.stringify(date));
    var now = moment();

    clone.results = clone.results.filter(o=> {
        return o.date > now;
    }).map(o=>o.day);

    return clone;
}

module.exports = function (router) {

    router.post('/schedule/add', check.isAuthenticated, check.isCustomer, function (req, res) {
        var user = req.user,
            body = req.body;

        if (!body.date) {
            return res.send(errors.get(182));
        }

        if (!(body.orderId || body.order)) {
            return res.send(errors.get(136));
        }

        function response(err, schedule) {
            if (err) {
                return res.send(errors.get(187, [err]))
            }

            res.send(errors.noError('Thank You! Your order has been submitted successfully. We will contact you shortly. You can mange your orders in Pendings tab.', {
                scheduleId: schedule
            }));
        }

        if (body.orderId) {
            Scheduler.add(body.date, body.orderId, user._id, response);

            return;
        }

        var order = body.order;

        if (!order.startPoint) {
            return res.send(errors.get(168));
        }

        var location = [order.startPoint.long, order.startPoint.lat];

        order.status = OrderStatus.INACTIVE;

        // need create order
        OrderController.create(user, order, location, function (res) {
            var orderId = res.data.orderId;

            Scheduler.add(body.date, orderId, user._id, response);
        },false)
    });

    router.post('/schedule/disable', check.isAuthenticated, check.isCustomer, function (req, res) {
        var body = req.body;

        if (!body.scheduleId) {
            return res.send(errors.get(183));
        }

        Scheduler.disable(body.scheduleId, function (response) {
            res.send(response);
        })
    });

    router.post('/schedule/enable', check.isAuthenticated, check.isCustomer, function (req, res) {
        var body = req.body;

        if (!body.scheduleId) {
            return res.send(errors.get(183));
        }

        Scheduler.enable(body.scheduleId, function (response) {
            res.send(response);
        });
    });

    router.post('/schedule/reschedule', check.isAuthenticated, check.isCustomer, function (req, res) {
        var body = req.body;

        if (!body.scheduleId) {
            return res.send(errors.get(183));
        }

        if(!body.date) {
            return res.send(errors.get(182));
        }

        Scheduler.reschedule(body.scheduleId, body.date, function (err) {
            if(err){
                return res.send(errors.get(198, [err]));
            }

            res.send(errors.noError());
        });
    });

    router.post('/schedules', check.isAuthenticated, check.isCustomer, function (req, res) {
        Schedule
            .find({
                user: req.user._id,
                disabled: {$ne: true}
            })
            .populate("order")
            .exec(function (err, schedules) {
                if (err) {
                    return res.send(errors.get(186, [err]));
                }
                if(req.body.orders != false) {
                    schedules.forEach(function (schedule) {
                        schedule.order = OrderController.parse(schedule.order.toObject());
                        schedule.date = parseScheduleDate(schedule.date);
                    });
                }

                var result = errors.noError(schedules);

                result.count = schedules.length;

                res.send(result);
            })
    });

    router.post('/schedule', check.isAuthenticated, check.isCustomer, function (req, res) {
        var sId = req.body.scheduleId;

        if (!sId) {
            return res.send(errors.get(183));
        }

        Schedule
            .findById(sId)
            .populate("order")
            .exec(function (err, schedule) {
                if (err || !schedule) {
                    return res.send(errors.get(186, [err]));
                }
                schedule.order = OrderController.parse(schedule.order.toObject());
                schedule.date = parseScheduleDate(schedule.date);

                res.send(errors.noError(schedule));
            })
    });

    router.post('/schedule/orders', check.isAuthenticated, check.isCustomer, function (req, res) {
        var sId = req.body.scheduleId;

        if (!sId) {
            return res.send(errors.get(183));
        }

        Order
            .find({
                schedule: sId,
                user: req.user._id
            })
            .exec(function (err, orders) {
                if (err) {
                    return res.send(errors.get(120, [err]));
                }

                var parsed = orders.map(function (order) {
                    return OrderController.parse(order.toObject());
                });

                res.send(errors.noError(parsed));
            })
    });
};
