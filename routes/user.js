var UserController = require('../controllers/user.js'),
    errors = require('../public/errors.js'),
    check = require('../passport/check.js');

module.exports = function (router) {
    router.post('/user/driver', check.isAuthenticated, check.isCustomer, function (req, res) {
        var driverID = req.body.ID;


        if (!driverID) {
            return callback(errors.get(193));
        }


        UserController.driverInfo(driverID,
            function (err, driver) {
                if (err) {
                    return res.send(err);
                }


                var result = errors.noError(null, driver);

                res.send(result);
            })
    });
    router.post('/user/sendEmail', check.isAuthenticated, function (req, res) {
alert(11111);
        console.log(req.body);
        UserController.sendEmail(req.body);
        return res.send(errors.noError());

    });

};