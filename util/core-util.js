const path = require('path');
const os = require('os');

CoreUtil = {
    userIdGenerator: function (mail, type) {
        return mail + '@@' + type;
    },
    getEnumKey: function (enumObject, value) {
        return Object.keys(enumObject).filter(function (key) {
            return enumObject[key] === value
        })[0];
    },
    toCamelCase: function (s) {
        s = s.replace(/([^a-zA-Z0-9_\- ])|^[_0-9]+/g, "").trim().toLowerCase();
        s = s.replace(/([ -]+)([a-zA-Z0-9])/g, function (a, b, c) {
            return c.toUpperCase();
        });
        s = s.replace(/([0-9]+)([a-zA-Z])/g, function (a, b, c) {
            return b + c.toUpperCase();
        });
        return s;
    },
    toProperCase: function (s) {
        return s.replace(/\w\S*/g, function (txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        });
    },
    isValidDate: function (date) {
        return date.getTime() === date.getTime();
    },
    expandPath: function (filepath) {
        if (filepath[0] === '~') {
            return path.join(process.env.HOME, filepath.slice(1));
        }
        return filepath;
    }
};

module.exports = CoreUtil;