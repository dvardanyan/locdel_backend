function getMiles(i) {
    return i * 0.000621371192;
}
function getMeters(i) {
    return i * 1609.344;
}

module.exports = {
    metersToMiles: getMiles,
    milesToMeters: getMeters
};