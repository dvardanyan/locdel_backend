var regexps = {
        address: /^[a-zA-Z0-9\u00BF-\u1FFF\u2C00-\uD7FF\s,/'-]+$/,
        contactNumber: /^[+0-9]{12}$/,
        ssn: /^\d{3}-?\d{2}-?\d{4}$/
    },
    validators = {
        enum: function (enumObj, value) {
            var values = Object.keys(enumObj).map(function (key) {
                return enumObj[key];
            });

            return values.indexOf(value) !== -1;
        }
    };
module.exports = function validator(type) {
    var reg = regexps[type];

    if (!reg) {
        if (validators[type]) {
            return validators[type](arguments[1], arguments[2]);
        }
        else {
            return;
        }
    }

    return reg.test(arguments[1]);
};